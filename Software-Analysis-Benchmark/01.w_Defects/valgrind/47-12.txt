==42069== Memcheck, a memory error detector
==42069== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42069== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42069== Command: ./01_w_Defects 47012
==42069== 
==42069== 
==42069== HEAP SUMMARY:
==42069==     in use at exit: 0 bytes in 0 blocks
==42069==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42069== 
==42069== All heap blocks were freed -- no leaks are possible
==42069== 
==42069== For counts of detected and suppressed errors, rerun with: -v
==42069== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
