==42858== Memcheck, a memory error detector
==42858== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42858== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42858== Command: ./01_w_Defects 50019
==42858== 
==42858== 
==42858== HEAP SUMMARY:
==42858==     in use at exit: 0 bytes in 0 blocks
==42858==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42858== 
==42858== All heap blocks were freed -- no leaks are possible
==42858== 
==42858== For counts of detected and suppressed errors, rerun with: -v
==42858== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
