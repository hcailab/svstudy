==33886== Memcheck, a memory error detector
==33886== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33886== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33886== Command: ./01_w_Defects 7043
==33886== 
==33886== 
==33886== HEAP SUMMARY:
==33886==     in use at exit: 0 bytes in 0 blocks
==33886==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33886== 
==33886== All heap blocks were freed -- no leaks are possible
==33886== 
==33886== For counts of detected and suppressed errors, rerun with: -v
==33886== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
