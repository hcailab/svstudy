==34246== Memcheck, a memory error detector
==34246== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34246== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34246== Command: ./01_w_Defects 9020
==34246== 
==34246== 
==34246== HEAP SUMMARY:
==34246==     in use at exit: 0 bytes in 0 blocks
==34246==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34246== 
==34246== All heap blocks were freed -- no leaks are possible
==34246== 
==34246== For counts of detected and suppressed errors, rerun with: -v
==34246== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
