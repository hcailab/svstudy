==36584== Memcheck, a memory error detector
==36584== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36584== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36584== Command: ./01_w_Defects 20043
==36584== 
==36584== 
==36584== HEAP SUMMARY:
==36584==     in use at exit: 0 bytes in 0 blocks
==36584==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36584== 
==36584== All heap blocks were freed -- no leaks are possible
==36584== 
==36584== For counts of detected and suppressed errors, rerun with: -v
==36584== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
