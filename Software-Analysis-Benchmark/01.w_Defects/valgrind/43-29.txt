==41209== Memcheck, a memory error detector
==41209== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41209== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41209== Command: ./01_w_Defects 43029
==41209== 
==41209== 
==41209== HEAP SUMMARY:
==41209==     in use at exit: 0 bytes in 0 blocks
==41209==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41209== 
==41209== All heap blocks were freed -- no leaks are possible
==41209== 
==41209== For counts of detected and suppressed errors, rerun with: -v
==41209== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
