==38625== Memcheck, a memory error detector
==38625== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38625== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38625== Command: ./01_w_Defects 30021
==38625== 
==38625== 
==38625== HEAP SUMMARY:
==38625==     in use at exit: 0 bytes in 0 blocks
==38625==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38625== 
==38625== All heap blocks were freed -- no leaks are possible
==38625== 
==38625== For counts of detected and suppressed errors, rerun with: -v
==38625== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
