==39652== Memcheck, a memory error detector
==39652== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39652== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39652== Command: ./01_w_Defects 34040
==39652== 
==39652== 
==39652== HEAP SUMMARY:
==39652==     in use at exit: 0 bytes in 0 blocks
==39652==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39652== 
==39652== All heap blocks were freed -- no leaks are possible
==39652== 
==39652== For counts of detected and suppressed errors, rerun with: -v
==39652== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
