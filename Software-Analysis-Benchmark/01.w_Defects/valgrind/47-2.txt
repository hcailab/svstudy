==42036== Memcheck, a memory error detector
==42036== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42036== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42036== Command: ./01_w_Defects 47002
==42036== 
==42036== 
==42036== HEAP SUMMARY:
==42036==     in use at exit: 0 bytes in 0 blocks
==42036==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42036== 
==42036== All heap blocks were freed -- no leaks are possible
==42036== 
==42036== For counts of detected and suppressed errors, rerun with: -v
==42036== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
