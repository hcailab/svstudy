==35247== Memcheck, a memory error detector
==35247== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35247== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35247== Command: ./01_w_Defects 14049
==35247== 
==35247== 
==35247== HEAP SUMMARY:
==35247==     in use at exit: 0 bytes in 0 blocks
==35247==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35247== 
==35247== All heap blocks were freed -- no leaks are possible
==35247== 
==35247== For counts of detected and suppressed errors, rerun with: -v
==35247== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
