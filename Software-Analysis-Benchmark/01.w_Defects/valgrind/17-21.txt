==35823== Memcheck, a memory error detector
==35823== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35823== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35823== Command: ./01_w_Defects 17021
==35823== 
==35823== 
==35823== HEAP SUMMARY:
==35823==     in use at exit: 0 bytes in 0 blocks
==35823==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35823== 
==35823== All heap blocks were freed -- no leaks are possible
==35823== 
==35823== For counts of detected and suppressed errors, rerun with: -v
==35823== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
