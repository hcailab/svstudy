==40309== Memcheck, a memory error detector
==40309== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40309== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40309== Command: ./01_w_Defects 38032
==40309== 
==40309== 
==40309== HEAP SUMMARY:
==40309==     in use at exit: 0 bytes in 0 blocks
==40309==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40309== 
==40309== All heap blocks were freed -- no leaks are possible
==40309== 
==40309== For counts of detected and suppressed errors, rerun with: -v
==40309== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
