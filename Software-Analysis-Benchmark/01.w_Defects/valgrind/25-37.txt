==37448== Memcheck, a memory error detector
==37448== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37448== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37448== Command: ./01_w_Defects 25037
==37448== 
==37448== 
==37448== HEAP SUMMARY:
==37448==     in use at exit: 0 bytes in 0 blocks
==37448==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37448== 
==37448== All heap blocks were freed -- no leaks are possible
==37448== 
==37448== For counts of detected and suppressed errors, rerun with: -v
==37448== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
