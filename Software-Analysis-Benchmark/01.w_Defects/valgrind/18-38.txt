==36078== Memcheck, a memory error detector
==36078== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36078== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36078== Command: ./01_w_Defects 18038
==36078== 
==36078== 
==36078== HEAP SUMMARY:
==36078==     in use at exit: 0 bytes in 0 blocks
==36078==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36078== 
==36078== All heap blocks were freed -- no leaks are possible
==36078== 
==36078== For counts of detected and suppressed errors, rerun with: -v
==36078== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
