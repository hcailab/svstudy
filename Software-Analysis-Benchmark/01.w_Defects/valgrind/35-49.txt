==39885== Memcheck, a memory error detector
==39885== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39885== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39885== Command: ./01_w_Defects 35049
==39885== 
==39885== 
==39885== HEAP SUMMARY:
==39885==     in use at exit: 0 bytes in 0 blocks
==39885==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39885== 
==39885== All heap blocks were freed -- no leaks are possible
==39885== 
==39885== For counts of detected and suppressed errors, rerun with: -v
==39885== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
