==39906== Memcheck, a memory error detector
==39906== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39906== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39906== Command: ./01_w_Defects 37005
==39906== 
==39906== 
==39906== HEAP SUMMARY:
==39906==     in use at exit: 0 bytes in 0 blocks
==39906==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39906== 
==39906== All heap blocks were freed -- no leaks are possible
==39906== 
==39906== For counts of detected and suppressed errors, rerun with: -v
==39906== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
