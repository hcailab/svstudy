==40764== Memcheck, a memory error detector
==40764== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40764== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40764== Command: ./01_w_Defects 41022
==40764== 
==40764== 
==40764== HEAP SUMMARY:
==40764==     in use at exit: 0 bytes in 0 blocks
==40764==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40764== 
==40764== All heap blocks were freed -- no leaks are possible
==40764== 
==40764== For counts of detected and suppressed errors, rerun with: -v
==40764== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
