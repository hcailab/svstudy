==40366== Memcheck, a memory error detector
==40366== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40366== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40366== Command: ./01_w_Defects 38048
==40366== 
==40366== 
==40366== HEAP SUMMARY:
==40366==     in use at exit: 0 bytes in 0 blocks
==40366==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40366== 
==40366== All heap blocks were freed -- no leaks are possible
==40366== 
==40366== For counts of detected and suppressed errors, rerun with: -v
==40366== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
