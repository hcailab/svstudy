==42376== Memcheck, a memory error detector
==42376== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42376== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42376== Command: ./01_w_Defects 48015
==42376== 
==42376== 
==42376== HEAP SUMMARY:
==42376==     in use at exit: 0 bytes in 0 blocks
==42376==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42376== 
==42376== All heap blocks were freed -- no leaks are possible
==42376== 
==42376== For counts of detected and suppressed errors, rerun with: -v
==42376== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
