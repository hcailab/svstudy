==34727== Memcheck, a memory error detector
==34727== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34727== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34727== Command: ./01_w_Defects 11028
==34727== 
==34727== 
==34727== HEAP SUMMARY:
==34727==     in use at exit: 0 bytes in 0 blocks
==34727==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34727== 
==34727== All heap blocks were freed -- no leaks are possible
==34727== 
==34727== For counts of detected and suppressed errors, rerun with: -v
==34727== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
