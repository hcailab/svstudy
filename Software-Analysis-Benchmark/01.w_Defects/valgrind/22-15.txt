==36674== Memcheck, a memory error detector
==36674== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36674== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36674== Command: ./01_w_Defects 22015
==36674== 
==36674== 
==36674== HEAP SUMMARY:
==36674==     in use at exit: 0 bytes in 0 blocks
==36674==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36674== 
==36674== All heap blocks were freed -- no leaks are possible
==36674== 
==36674== For counts of detected and suppressed errors, rerun with: -v
==36674== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
