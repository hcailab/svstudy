==36365== Memcheck, a memory error detector
==36365== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36365== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36365== Command: ./01_w_Defects 19045
==36365== 
==36365== 
==36365== HEAP SUMMARY:
==36365==     in use at exit: 0 bytes in 0 blocks
==36365==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36365== 
==36365== All heap blocks were freed -- no leaks are possible
==36365== 
==36365== For counts of detected and suppressed errors, rerun with: -v
==36365== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
