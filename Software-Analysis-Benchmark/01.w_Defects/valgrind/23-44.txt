==37010== Memcheck, a memory error detector
==37010== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37010== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37010== Command: ./01_w_Defects 23044
==37010== 
==37010== 
==37010== HEAP SUMMARY:
==37010==     in use at exit: 0 bytes in 0 blocks
==37010==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37010== 
==37010== All heap blocks were freed -- no leaks are possible
==37010== 
==37010== For counts of detected and suppressed errors, rerun with: -v
==37010== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
