==36553== Memcheck, a memory error detector
==36553== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36553== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36553== Command: ./01_w_Defects 20034
==36553== 
==36553== 
==36553== HEAP SUMMARY:
==36553==     in use at exit: 0 bytes in 0 blocks
==36553==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36553== 
==36553== All heap blocks were freed -- no leaks are possible
==36553== 
==36553== For counts of detected and suppressed errors, rerun with: -v
==36553== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
