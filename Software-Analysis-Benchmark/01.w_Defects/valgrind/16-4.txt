==70572== Memcheck, a memory error detector
==70572== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==70572== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==70572== Command: ./01_w_Defects 16004
==70572== 
==70572== Invalid free() / delete / delete[] / realloc()
==70572==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==70572==    by 0x41030C: free_nondynamic_allocated_memory_004 (free_nondynamic_allocated_memory.c:62)
==70572==    by 0x410758: free_nondynamic_allocated_memory_main (free_nondynamic_allocated_memory.c:296)
==70572==    by 0x4019AD: main (main.c:116)
==70572==  Address 0x1ffefffe08 is on thread 1's stack
==70572==  in frame #1, created by free_nondynamic_allocated_memory_004 (free_nondynamic_allocated_memory.c:58)
==70572== 
==70572== 
==70572== HEAP SUMMARY:
==70572==     in use at exit: 0 bytes in 0 blocks
==70572==   total heap usage: 1 allocs, 2 frees, 1,024 bytes allocated
==70572== 
==70572== All heap blocks were freed -- no leaks are possible
==70572== 
==70572== For counts of detected and suppressed errors, rerun with: -v
==70572== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
