==39700== Memcheck, a memory error detector
==39700== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39700== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39700== Command: ./01_w_Defects 35002
==39700== 
==39700== 
==39700== HEAP SUMMARY:
==39700==     in use at exit: 0 bytes in 0 blocks
==39700==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39700== 
==39700== All heap blocks were freed -- no leaks are possible
==39700== 
==39700== For counts of detected and suppressed errors, rerun with: -v
==39700== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
