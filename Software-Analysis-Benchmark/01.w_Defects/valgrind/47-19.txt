==42111== Memcheck, a memory error detector
==42111== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42111== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42111== Command: ./01_w_Defects 47019
==42111== 
==42111== 
==42111== HEAP SUMMARY:
==42111==     in use at exit: 0 bytes in 0 blocks
==42111==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42111== 
==42111== All heap blocks were freed -- no leaks are possible
==42111== 
==42111== For counts of detected and suppressed errors, rerun with: -v
==42111== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
