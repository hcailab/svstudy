==72205== Memcheck, a memory error detector
==72205== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==72205== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==72205== Command: ./01_w_Defects 3019
==72205== 
==72205== Invalid write of size 4
==72205==    at 0x404C00: dynamic_buffer_underrun_019 (buffer_underrun_dynamic.c:354)
==72205==    by 0x40585E: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:884)
==72205==    by 0x4017F3: main (main.c:38)
==72205==  Address 0x57ea46c is 20 bytes before a block of size 20 alloc'd
==72205==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==72205==    by 0x404BD3: dynamic_buffer_underrun_019 (buffer_underrun_dynamic.c:348)
==72205==    by 0x40585E: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:884)
==72205==    by 0x4017F3: main (main.c:38)
==72205== 
==72205== 
==72205== HEAP SUMMARY:
==72205==     in use at exit: 0 bytes in 0 blocks
==72205==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==72205== 
==72205== All heap blocks were freed -- no leaks are possible
==72205== 
==72205== For counts of detected and suppressed errors, rerun with: -v
==72205== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
