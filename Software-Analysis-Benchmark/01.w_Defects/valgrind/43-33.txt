==41225== Memcheck, a memory error detector
==41225== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41225== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41225== Command: ./01_w_Defects 43033
==41225== 
==41225== 
==41225== HEAP SUMMARY:
==41225==     in use at exit: 0 bytes in 0 blocks
==41225==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41225== 
==41225== All heap blocks were freed -- no leaks are possible
==41225== 
==41225== For counts of detected and suppressed errors, rerun with: -v
==41225== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
