==33047== Memcheck, a memory error detector
==33047== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33047== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33047== Command: ./01_w_Defects 3041
==33047== 
==33047== 
==33047== HEAP SUMMARY:
==33047==     in use at exit: 0 bytes in 0 blocks
==33047==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33047== 
==33047== All heap blocks were freed -- no leaks are possible
==33047== 
==33047== For counts of detected and suppressed errors, rerun with: -v
==33047== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
