==33467== Memcheck, a memory error detector
==33467== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33467== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33467== Command: ./01_w_Defects 5040
==33467== 
==33467== 
==33467== HEAP SUMMARY:
==33467==     in use at exit: 0 bytes in 0 blocks
==33467==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33467== 
==33467== All heap blocks were freed -- no leaks are possible
==33467== 
==33467== For counts of detected and suppressed errors, rerun with: -v
==33467== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
