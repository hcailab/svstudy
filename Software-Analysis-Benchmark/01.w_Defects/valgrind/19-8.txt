==36211== Memcheck, a memory error detector
==36211== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36211== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36211== Command: ./01_w_Defects 19008
==36211== 
==36211== 
==36211== HEAP SUMMARY:
==36211==     in use at exit: 0 bytes in 0 blocks
==36211==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36211== 
==36211== All heap blocks were freed -- no leaks are possible
==36211== 
==36211== For counts of detected and suppressed errors, rerun with: -v
==36211== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
