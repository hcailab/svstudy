==40246== Memcheck, a memory error detector
==40246== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40246== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40246== Command: ./01_w_Defects 38015
==40246== 
==40246== 
==40246== HEAP SUMMARY:
==40246==     in use at exit: 0 bytes in 0 blocks
==40246==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40246== 
==40246== All heap blocks were freed -- no leaks are possible
==40246== 
==40246== For counts of detected and suppressed errors, rerun with: -v
==40246== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
