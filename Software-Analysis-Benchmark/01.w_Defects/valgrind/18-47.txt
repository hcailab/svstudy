==36168== Memcheck, a memory error detector
==36168== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36168== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36168== Command: ./01_w_Defects 18047
==36168== 
==36168== 
==36168== HEAP SUMMARY:
==36168==     in use at exit: 0 bytes in 0 blocks
==36168==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36168== 
==36168== All heap blocks were freed -- no leaks are possible
==36168== 
==36168== For counts of detected and suppressed errors, rerun with: -v
==36168== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
