==69026== Memcheck, a memory error detector
==69026== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==69026== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==69026== Command: ./01_w_Defects 24003
==69026== 
==69026== 
==69026== HEAP SUMMARY:
==69026==     in use at exit: 0 bytes in 0 blocks
==69026==   total heap usage: 10 allocs, 10 frees, 1,249 bytes allocated
==69026== 
==69026== All heap blocks were freed -- no leaks are possible
==69026== 
==69026== For counts of detected and suppressed errors, rerun with: -v
==69026== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
