==38991== Memcheck, a memory error detector
==38991== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38991== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38991== Command: ./01_w_Defects 31045
==38991== 
==38991== 
==38991== HEAP SUMMARY:
==38991==     in use at exit: 0 bytes in 0 blocks
==38991==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38991== 
==38991== All heap blocks were freed -- no leaks are possible
==38991== 
==38991== For counts of detected and suppressed errors, rerun with: -v
==38991== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
