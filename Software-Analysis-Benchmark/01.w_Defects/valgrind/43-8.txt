==41136== Memcheck, a memory error detector
==41136== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41136== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41136== Command: ./01_w_Defects 43008
==41136== 
==41136== 
==41136== HEAP SUMMARY:
==41136==     in use at exit: 0 bytes in 0 blocks
==41136==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41136== 
==41136== All heap blocks were freed -- no leaks are possible
==41136== 
==41136== For counts of detected and suppressed errors, rerun with: -v
==41136== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
