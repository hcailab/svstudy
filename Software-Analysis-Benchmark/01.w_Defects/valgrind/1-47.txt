==32603== Memcheck, a memory error detector
==32603== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32603== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32603== Command: ./01_w_Defects 1047
==32603== 
==32603== 
==32603== HEAP SUMMARY:
==32603==     in use at exit: 0 bytes in 0 blocks
==32603==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32603== 
==32603== All heap blocks were freed -- no leaks are possible
==32603== 
==32603== For counts of detected and suppressed errors, rerun with: -v
==32603== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
