==38153== Memcheck, a memory error detector
==38153== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38153== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38153== Command: ./01_w_Defects 28019
==38153== 
==38153== 
==38153== HEAP SUMMARY:
==38153==     in use at exit: 0 bytes in 0 blocks
==38153==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38153== 
==38153== All heap blocks were freed -- no leaks are possible
==38153== 
==38153== For counts of detected and suppressed errors, rerun with: -v
==38153== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
