==37269== Memcheck, a memory error detector
==37269== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37269== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37269== Command: ./01_w_Defects 24050
==37269== 
==37269== 
==37269== HEAP SUMMARY:
==37269==     in use at exit: 0 bytes in 0 blocks
==37269==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37269== 
==37269== All heap blocks were freed -- no leaks are possible
==37269== 
==37269== For counts of detected and suppressed errors, rerun with: -v
==37269== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
