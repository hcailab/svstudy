==34398== Memcheck, a memory error detector
==34398== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34398== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34398== Command: ./01_w_Defects 9047
==34398== 
==34398== 
==34398== HEAP SUMMARY:
==34398==     in use at exit: 0 bytes in 0 blocks
==34398==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34398== 
==34398== All heap blocks were freed -- no leaks are possible
==34398== 
==34398== For counts of detected and suppressed errors, rerun with: -v
==34398== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
