==34988== Memcheck, a memory error detector
==34988== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34988== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34988== Command: ./01_w_Defects 12045
==34988== 
==34988== 
==34988== HEAP SUMMARY:
==34988==     in use at exit: 0 bytes in 0 blocks
==34988==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34988== 
==34988== All heap blocks were freed -- no leaks are possible
==34988== 
==34988== For counts of detected and suppressed errors, rerun with: -v
==34988== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
