==65058== Memcheck, a memory error detector
==65058== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==65058== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==65058== Command: ./01_w_Defects 32027
==65058== 
==65058== 
==65058== HEAP SUMMARY:
==65058==     in use at exit: 0 bytes in 0 blocks
==65058==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==65058== 
==65058== All heap blocks were freed -- no leaks are possible
==65058== 
==65058== For counts of detected and suppressed errors, rerun with: -v
==65058== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
