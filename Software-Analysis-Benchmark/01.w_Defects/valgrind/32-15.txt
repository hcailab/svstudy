==65002== Memcheck, a memory error detector
==65002== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==65002== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==65002== Command: ./01_w_Defects 32015
==65002== 
==65002== 
==65002== HEAP SUMMARY:
==65002==     in use at exit: 0 bytes in 0 blocks
==65002==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==65002== 
==65002== All heap blocks were freed -- no leaks are possible
==65002== 
==65002== For counts of detected and suppressed errors, rerun with: -v
==65002== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
