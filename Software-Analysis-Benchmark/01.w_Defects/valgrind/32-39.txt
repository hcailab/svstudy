==65110== Memcheck, a memory error detector
==65110== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==65110== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==65110== Command: ./01_w_Defects 32039
==65110== 
==65110== 
==65110== HEAP SUMMARY:
==65110==     in use at exit: 0 bytes in 0 blocks
==65110==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==65110== 
==65110== All heap blocks were freed -- no leaks are possible
==65110== 
==65110== For counts of detected and suppressed errors, rerun with: -v
==65110== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
