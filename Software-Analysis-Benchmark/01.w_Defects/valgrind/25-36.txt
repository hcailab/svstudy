==37446== Memcheck, a memory error detector
==37446== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37446== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37446== Command: ./01_w_Defects 25036
==37446== 
==37446== 
==37446== HEAP SUMMARY:
==37446==     in use at exit: 0 bytes in 0 blocks
==37446==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37446== 
==37446== All heap blocks were freed -- no leaks are possible
==37446== 
==37446== For counts of detected and suppressed errors, rerun with: -v
==37446== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
