==39274== Memcheck, a memory error detector
==39274== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39274== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39274== Command: ./01_w_Defects 33008
==39274== 
==39274== 
==39274== HEAP SUMMARY:
==39274==     in use at exit: 0 bytes in 0 blocks
==39274==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39274== 
==39274== All heap blocks were freed -- no leaks are possible
==39274== 
==39274== For counts of detected and suppressed errors, rerun with: -v
==39274== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
