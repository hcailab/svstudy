==34712== Memcheck, a memory error detector
==34712== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34712== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34712== Command: ./01_w_Defects 11023
==34712== 
==34712== 
==34712== HEAP SUMMARY:
==34712==     in use at exit: 0 bytes in 0 blocks
==34712==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34712== 
==34712== All heap blocks were freed -- no leaks are possible
==34712== 
==34712== For counts of detected and suppressed errors, rerun with: -v
==34712== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
