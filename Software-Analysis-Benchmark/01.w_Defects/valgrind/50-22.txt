==42865== Memcheck, a memory error detector
==42865== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42865== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42865== Command: ./01_w_Defects 50022
==42865== 
==42865== 
==42865== HEAP SUMMARY:
==42865==     in use at exit: 0 bytes in 0 blocks
==42865==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42865== 
==42865== All heap blocks were freed -- no leaks are possible
==42865== 
==42865== For counts of detected and suppressed errors, rerun with: -v
==42865== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
