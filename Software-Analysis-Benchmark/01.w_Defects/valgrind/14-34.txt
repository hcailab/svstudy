==35152== Memcheck, a memory error detector
==35152== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35152== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35152== Command: ./01_w_Defects 14034
==35152== 
==35152== 
==35152== HEAP SUMMARY:
==35152==     in use at exit: 0 bytes in 0 blocks
==35152==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35152== 
==35152== All heap blocks were freed -- no leaks are possible
==35152== 
==35152== For counts of detected and suppressed errors, rerun with: -v
==35152== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
