==36614== Memcheck, a memory error detector
==36614== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36614== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36614== Command: ./01_w_Defects 20050
==36614== 
==36614== 
==36614== HEAP SUMMARY:
==36614==     in use at exit: 0 bytes in 0 blocks
==36614==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36614== 
==36614== All heap blocks were freed -- no leaks are possible
==36614== 
==36614== For counts of detected and suppressed errors, rerun with: -v
==36614== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
