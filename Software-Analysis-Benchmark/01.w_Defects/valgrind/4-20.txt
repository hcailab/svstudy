==33169== Memcheck, a memory error detector
==33169== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33169== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33169== Command: ./01_w_Defects 4020
==33169== 
==33169== 
==33169== HEAP SUMMARY:
==33169==     in use at exit: 0 bytes in 0 blocks
==33169==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33169== 
==33169== All heap blocks were freed -- no leaks are possible
==33169== 
==33169== For counts of detected and suppressed errors, rerun with: -v
==33169== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
