==34065== Memcheck, a memory error detector
==34065== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34065== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34065== Command: ./01_w_Defects 8037
==34065== 
==34065== 
==34065== HEAP SUMMARY:
==34065==     in use at exit: 0 bytes in 0 blocks
==34065==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34065== 
==34065== All heap blocks were freed -- no leaks are possible
==34065== 
==34065== For counts of detected and suppressed errors, rerun with: -v
==34065== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
