==37371== Memcheck, a memory error detector
==37371== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37371== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37371== Command: ./01_w_Defects 25017
==37371== 
==37371== 
==37371== HEAP SUMMARY:
==37371==     in use at exit: 0 bytes in 0 blocks
==37371==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37371== 
==37371== All heap blocks were freed -- no leaks are possible
==37371== 
==37371== For counts of detected and suppressed errors, rerun with: -v
==37371== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
