==34279== Memcheck, a memory error detector
==34279== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34279== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34279== Command: ./01_w_Defects 9027
==34279== 
==34279== 
==34279== HEAP SUMMARY:
==34279==     in use at exit: 0 bytes in 0 blocks
==34279==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34279== 
==34279== All heap blocks were freed -- no leaks are possible
==34279== 
==34279== For counts of detected and suppressed errors, rerun with: -v
==34279== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
