==34133== Memcheck, a memory error detector
==34133== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34133== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34133== Command: ./01_w_Defects 8045
==34133== 
==34133== 
==34133== HEAP SUMMARY:
==34133==     in use at exit: 0 bytes in 0 blocks
==34133==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34133== 
==34133== All heap blocks were freed -- no leaks are possible
==34133== 
==34133== For counts of detected and suppressed errors, rerun with: -v
==34133== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
