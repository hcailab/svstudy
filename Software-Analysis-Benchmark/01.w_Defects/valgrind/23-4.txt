==36846== Memcheck, a memory error detector
==36846== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36846== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36846== Command: ./01_w_Defects 23004
==36846== 
==36846== 
==36846== HEAP SUMMARY:
==36846==     in use at exit: 0 bytes in 0 blocks
==36846==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36846== 
==36846== All heap blocks were freed -- no leaks are possible
==36846== 
==36846== For counts of detected and suppressed errors, rerun with: -v
==36846== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
