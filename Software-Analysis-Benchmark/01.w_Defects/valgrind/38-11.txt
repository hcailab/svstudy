==40233== Memcheck, a memory error detector
==40233== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40233== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40233== Command: ./01_w_Defects 38011
==40233== 
==40233== 
==40233== HEAP SUMMARY:
==40233==     in use at exit: 0 bytes in 0 blocks
==40233==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40233== 
==40233== All heap blocks were freed -- no leaks are possible
==40233== 
==40233== For counts of detected and suppressed errors, rerun with: -v
==40233== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
