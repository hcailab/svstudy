==33645== Memcheck, a memory error detector
==33645== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33645== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33645== Command: ./01_w_Defects 6035
==33645== 
==33645== 
==33645== HEAP SUMMARY:
==33645==     in use at exit: 0 bytes in 0 blocks
==33645==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33645== 
==33645== All heap blocks were freed -- no leaks are possible
==33645== 
==33645== For counts of detected and suppressed errors, rerun with: -v
==33645== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
