==32485== Memcheck, a memory error detector
==32485== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32485== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32485== Command: ./01_w_Defects 1018
==32485== 
==32485== 
==32485== HEAP SUMMARY:
==32485==     in use at exit: 0 bytes in 0 blocks
==32485==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32485== 
==32485== All heap blocks were freed -- no leaks are possible
==32485== 
==32485== For counts of detected and suppressed errors, rerun with: -v
==32485== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
