==36992== Memcheck, a memory error detector
==36992== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36992== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36992== Command: ./01_w_Defects 23038
==36992== 
==36992== 
==36992== HEAP SUMMARY:
==36992==     in use at exit: 0 bytes in 0 blocks
==36992==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36992== 
==36992== All heap blocks were freed -- no leaks are possible
==36992== 
==36992== For counts of detected and suppressed errors, rerun with: -v
==36992== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
