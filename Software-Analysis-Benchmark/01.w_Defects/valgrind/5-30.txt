==33430== Memcheck, a memory error detector
==33430== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33430== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33430== Command: ./01_w_Defects 5030
==33430== 
==33430== 
==33430== HEAP SUMMARY:
==33430==     in use at exit: 0 bytes in 0 blocks
==33430==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33430== 
==33430== All heap blocks were freed -- no leaks are possible
==33430== 
==33430== For counts of detected and suppressed errors, rerun with: -v
==33430== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
