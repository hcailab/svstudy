==33111== Memcheck, a memory error detector
==33111== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33111== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33111== Command: ./01_w_Defects 4006
==33111== 
==33111== 
==33111== HEAP SUMMARY:
==33111==     in use at exit: 0 bytes in 0 blocks
==33111==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33111== 
==33111== All heap blocks were freed -- no leaks are possible
==33111== 
==33111== For counts of detected and suppressed errors, rerun with: -v
==33111== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
