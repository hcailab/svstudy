==35120== Memcheck, a memory error detector
==35120== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35120== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35120== Command: ./01_w_Defects 14023
==35120== 
==35120== 
==35120== HEAP SUMMARY:
==35120==     in use at exit: 0 bytes in 0 blocks
==35120==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35120== 
==35120== All heap blocks were freed -- no leaks are possible
==35120== 
==35120== For counts of detected and suppressed errors, rerun with: -v
==35120== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
