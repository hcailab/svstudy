==36191== Memcheck, a memory error detector
==36191== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36191== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36191== Command: ./01_w_Defects 19003
==36191== 
==36191== 
==36191== HEAP SUMMARY:
==36191==     in use at exit: 0 bytes in 0 blocks
==36191==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36191== 
==36191== All heap blocks were freed -- no leaks are possible
==36191== 
==36191== For counts of detected and suppressed errors, rerun with: -v
==36191== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
