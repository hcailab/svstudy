==43227== Memcheck, a memory error detector
==43227== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43227== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43227== Command: ./01_w_Defects 51050
==43227== 
==43227== 
==43227== HEAP SUMMARY:
==43227==     in use at exit: 0 bytes in 0 blocks
==43227==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43227== 
==43227== All heap blocks were freed -- no leaks are possible
==43227== 
==43227== For counts of detected and suppressed errors, rerun with: -v
==43227== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
