==39847== Memcheck, a memory error detector
==39847== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39847== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39847== Command: ./01_w_Defects 35042
==39847== 
==39847== 
==39847== HEAP SUMMARY:
==39847==     in use at exit: 0 bytes in 0 blocks
==39847==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39847== 
==39847== All heap blocks were freed -- no leaks are possible
==39847== 
==39847== For counts of detected and suppressed errors, rerun with: -v
==39847== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
