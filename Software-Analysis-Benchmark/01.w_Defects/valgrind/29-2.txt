==38310== Memcheck, a memory error detector
==38310== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38310== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38310== Command: ./01_w_Defects 29002
==38310== 
==38310== 
==38310== HEAP SUMMARY:
==38310==     in use at exit: 0 bytes in 0 blocks
==38310==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38310== 
==38310== All heap blocks were freed -- no leaks are possible
==38310== 
==38310== For counts of detected and suppressed errors, rerun with: -v
==38310== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
