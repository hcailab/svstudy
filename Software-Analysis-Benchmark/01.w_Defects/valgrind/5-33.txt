==33441== Memcheck, a memory error detector
==33441== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33441== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33441== Command: ./01_w_Defects 5033
==33441== 
==33441== 
==33441== HEAP SUMMARY:
==33441==     in use at exit: 0 bytes in 0 blocks
==33441==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33441== 
==33441== All heap blocks were freed -- no leaks are possible
==33441== 
==33441== For counts of detected and suppressed errors, rerun with: -v
==33441== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
