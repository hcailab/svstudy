==38829== Memcheck, a memory error detector
==38829== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38829== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38829== Command: ./01_w_Defects 31010
==38829== 
==38829== Invalid write of size 4
==38829==    at 0x10DAF0: null_pointer_010 (null_pointer.c:159)
==38829==    by 0x10DAF0: null_pointer_main (null_pointer.c:403)
==38829==    by 0x109BDC: main (main.c:206)
==38829==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
==38829== 
==38829== 
==38829== Process terminating with default action of signal 11 (SIGSEGV)
==38829==  Access not within mapped region at address 0x0
==38829==    at 0x10DAF0: null_pointer_010 (null_pointer.c:159)
==38829==    by 0x10DAF0: null_pointer_main (null_pointer.c:403)
==38829==    by 0x109BDC: main (main.c:206)
==38829==  If you believe this happened as a result of a stack
==38829==  overflow in your program's main thread (unlikely but
==38829==  possible), you can try to increase the size of the
==38829==  main thread stack using the --main-stacksize= flag.
==38829==  The main thread stack size used in this run was 8388608.
==38829== 
==38829== HEAP SUMMARY:
==38829==     in use at exit: 0 bytes in 0 blocks
==38829==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38829== 
==38829== All heap blocks were freed -- no leaks are possible
==38829== 
==38829== For counts of detected and suppressed errors, rerun with: -v
==38829== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
