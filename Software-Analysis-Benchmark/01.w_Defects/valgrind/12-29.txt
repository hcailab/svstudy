==34928== Memcheck, a memory error detector
==34928== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34928== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34928== Command: ./01_w_Defects 12029
==34928== 
==34928== 
==34928== HEAP SUMMARY:
==34928==     in use at exit: 0 bytes in 0 blocks
==34928==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34928== 
==34928== All heap blocks were freed -- no leaks are possible
==34928== 
==34928== For counts of detected and suppressed errors, rerun with: -v
==34928== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
