==37356== Memcheck, a memory error detector
==37356== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37356== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37356== Command: ./01_w_Defects 25012
==37356== 
==37356== 
==37356== HEAP SUMMARY:
==37356==     in use at exit: 0 bytes in 0 blocks
==37356==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37356== 
==37356== All heap blocks were freed -- no leaks are possible
==37356== 
==37356== For counts of detected and suppressed errors, rerun with: -v
==37356== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
