==36582== Memcheck, a memory error detector
==36582== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36582== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36582== Command: ./01_w_Defects 20042
==36582== 
==36582== 
==36582== HEAP SUMMARY:
==36582==     in use at exit: 0 bytes in 0 blocks
==36582==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36582== 
==36582== All heap blocks were freed -- no leaks are possible
==36582== 
==36582== For counts of detected and suppressed errors, rerun with: -v
==36582== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
