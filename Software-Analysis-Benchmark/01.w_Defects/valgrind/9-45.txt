==34378== Memcheck, a memory error detector
==34378== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34378== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34378== Command: ./01_w_Defects 9045
==34378== 
==34378== 
==34378== HEAP SUMMARY:
==34378==     in use at exit: 0 bytes in 0 blocks
==34378==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34378== 
==34378== All heap blocks were freed -- no leaks are possible
==34378== 
==34378== For counts of detected and suppressed errors, rerun with: -v
==34378== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
