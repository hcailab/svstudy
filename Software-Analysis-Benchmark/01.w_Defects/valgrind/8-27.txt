==34016== Memcheck, a memory error detector
==34016== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34016== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34016== Command: ./01_w_Defects 8027
==34016== 
==34016== 
==34016== HEAP SUMMARY:
==34016==     in use at exit: 0 bytes in 0 blocks
==34016==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34016== 
==34016== All heap blocks were freed -- no leaks are possible
==34016== 
==34016== For counts of detected and suppressed errors, rerun with: -v
==34016== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
