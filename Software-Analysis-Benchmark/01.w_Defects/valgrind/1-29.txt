==32527== Memcheck, a memory error detector
==32527== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32527== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32527== Command: ./01_w_Defects 1029
==32527== 
==32527== 
==32527== HEAP SUMMARY:
==32527==     in use at exit: 0 bytes in 0 blocks
==32527==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32527== 
==32527== All heap blocks were freed -- no leaks are possible
==32527== 
==32527== For counts of detected and suppressed errors, rerun with: -v
==32527== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
