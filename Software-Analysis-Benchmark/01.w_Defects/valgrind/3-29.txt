==72245== Memcheck, a memory error detector
==72245== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==72245== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==72245== Command: ./01_w_Defects 3029
==72245== 
==72245== Invalid write of size 1
==72245==    at 0x404F61: dynamic_buffer_underrun_029 (buffer_underrun_dynamic.c:531)
==72245==    by 0x4059B2: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:934)
==72245==    by 0x4017F3: main (main.c:38)
==72245==  Address 0x57ea47f is 1 bytes before a block of size 1 alloc'd
==72245==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==72245==    by 0x404F4D: dynamic_buffer_underrun_029 (buffer_underrun_dynamic.c:528)
==72245==    by 0x4059B2: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:934)
==72245==    by 0x4017F3: main (main.c:38)
==72245== 
==72245== 
==72245== HEAP SUMMARY:
==72245==     in use at exit: 0 bytes in 0 blocks
==72245==   total heap usage: 2 allocs, 2 frees, 1,025 bytes allocated
==72245== 
==72245== All heap blocks were freed -- no leaks are possible
==72245== 
==72245== For counts of detected and suppressed errors, rerun with: -v
==72245== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
