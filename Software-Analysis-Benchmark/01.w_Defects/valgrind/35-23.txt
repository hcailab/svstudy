==39769== Memcheck, a memory error detector
==39769== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39769== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39769== Command: ./01_w_Defects 35023
==39769== 
==39769== 
==39769== HEAP SUMMARY:
==39769==     in use at exit: 0 bytes in 0 blocks
==39769==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39769== 
==39769== All heap blocks were freed -- no leaks are possible
==39769== 
==39769== For counts of detected and suppressed errors, rerun with: -v
==39769== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
