==42753== Memcheck, a memory error detector
==42753== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42753== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42753== Command: ./01_w_Defects 49050
==42753== 
==42753== 
==42753== HEAP SUMMARY:
==42753==     in use at exit: 0 bytes in 0 blocks
==42753==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42753== 
==42753== All heap blocks were freed -- no leaks are possible
==42753== 
==42753== For counts of detected and suppressed errors, rerun with: -v
==42753== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
