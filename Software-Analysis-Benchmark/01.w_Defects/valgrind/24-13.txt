==69152== Memcheck, a memory error detector
==69152== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==69152== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==69152== Command: ./01_w_Defects 24013
==69152== 
==69152== Invalid read of size 4
==69152==    at 0x41800F: invalid_memory_access_013_func_002 (invalid_memory_access.c:432)
==69152==    by 0x418070: invalid_memory_access_013 (invalid_memory_access.c:454)
==69152==    by 0x418737: invalid_memory_access_main (invalid_memory_access.c:725)
==69152==    by 0x401AAC: main (main.c:164)
==69152==  Address 0x57ea480 is 0 bytes inside a block of size 12 free'd
==69152==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69152==    by 0x417FAF: invalid_memory_access_013_func_002 (invalid_memory_access.c:411)
==69152==    by 0x418070: invalid_memory_access_013 (invalid_memory_access.c:454)
==69152==    by 0x418737: invalid_memory_access_main (invalid_memory_access.c:725)
==69152==    by 0x401AAC: main (main.c:164)
==69152==  Block was alloc'd at
==69152==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69152==    by 0x417F2D: invalid_memory_access_013_func_001 (invalid_memory_access.c:395)
==69152==    by 0x418066: invalid_memory_access_013 (invalid_memory_access.c:453)
==69152==    by 0x418737: invalid_memory_access_main (invalid_memory_access.c:725)
==69152==    by 0x401AAC: main (main.c:164)
==69152== 
==69152== 
==69152== HEAP SUMMARY:
==69152==     in use at exit: 0 bytes in 0 blocks
==69152==   total heap usage: 2 allocs, 2 frees, 1,036 bytes allocated
==69152== 
==69152== All heap blocks were freed -- no leaks are possible
==69152== 
==69152== For counts of detected and suppressed errors, rerun with: -v
==69152== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
