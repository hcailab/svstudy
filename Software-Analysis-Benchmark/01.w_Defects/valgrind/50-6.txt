==42776== Memcheck, a memory error detector
==42776== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42776== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42776== Command: ./01_w_Defects 50006
==42776== 
==42776== 
==42776== HEAP SUMMARY:
==42776==     in use at exit: 0 bytes in 0 blocks
==42776==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42776== 
==42776== All heap blocks were freed -- no leaks are possible
==42776== 
==42776== For counts of detected and suppressed errors, rerun with: -v
==42776== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
