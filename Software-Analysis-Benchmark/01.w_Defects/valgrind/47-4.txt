==42041== Memcheck, a memory error detector
==42041== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42041== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42041== Command: ./01_w_Defects 47004
==42041== 
==42041== 
==42041== HEAP SUMMARY:
==42041==     in use at exit: 0 bytes in 0 blocks
==42041==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42041== 
==42041== All heap blocks were freed -- no leaks are possible
==42041== 
==42041== For counts of detected and suppressed errors, rerun with: -v
==42041== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
