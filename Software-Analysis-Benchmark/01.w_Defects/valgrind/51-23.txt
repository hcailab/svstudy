==43115== Memcheck, a memory error detector
==43115== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43115== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43115== Command: ./01_w_Defects 51023
==43115== 
==43115== 
==43115== HEAP SUMMARY:
==43115==     in use at exit: 0 bytes in 0 blocks
==43115==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43115== 
==43115== All heap blocks were freed -- no leaks are possible
==43115== 
==43115== For counts of detected and suppressed errors, rerun with: -v
==43115== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
