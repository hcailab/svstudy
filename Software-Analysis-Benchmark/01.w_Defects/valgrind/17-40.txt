==35893== Memcheck, a memory error detector
==35893== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35893== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35893== Command: ./01_w_Defects 17040
==35893== 
==35893== 
==35893== HEAP SUMMARY:
==35893==     in use at exit: 0 bytes in 0 blocks
==35893==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35893== 
==35893== All heap blocks were freed -- no leaks are possible
==35893== 
==35893== For counts of detected and suppressed errors, rerun with: -v
==35893== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
