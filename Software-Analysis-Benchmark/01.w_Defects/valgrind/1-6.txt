==32431== Memcheck, a memory error detector
==32431== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32431== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32431== Command: ./01_w_Defects 1006
==32431== 
==32431== 
==32431== HEAP SUMMARY:
==32431==     in use at exit: 0 bytes in 0 blocks
==32431==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32431== 
==32431== All heap blocks were freed -- no leaks are possible
==32431== 
==32431== For counts of detected and suppressed errors, rerun with: -v
==32431== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
