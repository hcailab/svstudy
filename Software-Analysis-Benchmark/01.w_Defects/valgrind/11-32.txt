==34740== Memcheck, a memory error detector
==34740== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34740== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34740== Command: ./01_w_Defects 11032
==34740== 
==34740== 
==34740== HEAP SUMMARY:
==34740==     in use at exit: 0 bytes in 0 blocks
==34740==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34740== 
==34740== All heap blocks were freed -- no leaks are possible
==34740== 
==34740== For counts of detected and suppressed errors, rerun with: -v
==34740== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
