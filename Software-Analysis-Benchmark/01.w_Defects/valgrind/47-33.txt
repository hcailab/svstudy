==42203== Memcheck, a memory error detector
==42203== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42203== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42203== Command: ./01_w_Defects 47033
==42203== 
==42203== 
==42203== HEAP SUMMARY:
==42203==     in use at exit: 0 bytes in 0 blocks
==42203==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42203== 
==42203== All heap blocks were freed -- no leaks are possible
==42203== 
==42203== For counts of detected and suppressed errors, rerun with: -v
==42203== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
