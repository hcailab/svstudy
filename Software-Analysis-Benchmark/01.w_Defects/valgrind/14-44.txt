==35190== Memcheck, a memory error detector
==35190== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35190== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35190== Command: ./01_w_Defects 14044
==35190== 
==35190== 
==35190== HEAP SUMMARY:
==35190==     in use at exit: 0 bytes in 0 blocks
==35190==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35190== 
==35190== All heap blocks were freed -- no leaks are possible
==35190== 
==35190== For counts of detected and suppressed errors, rerun with: -v
==35190== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
