==32422== Memcheck, a memory error detector
==32422== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32422== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32422== Command: ./01_w_Defects 1004
==32422== 
==32422== 
==32422== HEAP SUMMARY:
==32422==     in use at exit: 0 bytes in 0 blocks
==32422==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32422== 
==32422== All heap blocks were freed -- no leaks are possible
==32422== 
==32422== For counts of detected and suppressed errors, rerun with: -v
==32422== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
