==34457== Memcheck, a memory error detector
==34457== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34457== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34457== Command: ./01_w_Defects 10008
==34457== 
==34457== 
==34457== HEAP SUMMARY:
==34457==     in use at exit: 0 bytes in 0 blocks
==34457==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34457== 
==34457== All heap blocks were freed -- no leaks are possible
==34457== 
==34457== For counts of detected and suppressed errors, rerun with: -v
==34457== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
