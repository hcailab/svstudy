==39650== Memcheck, a memory error detector
==39650== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39650== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39650== Command: ./01_w_Defects 34039
==39650== 
==39650== 
==39650== HEAP SUMMARY:
==39650==     in use at exit: 0 bytes in 0 blocks
==39650==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39650== 
==39650== All heap blocks were freed -- no leaks are possible
==39650== 
==39650== For counts of detected and suppressed errors, rerun with: -v
==39650== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
