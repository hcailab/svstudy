==41178== Memcheck, a memory error detector
==41178== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41178== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41178== Command: ./01_w_Defects 43021
==41178== 
==41178== 
==41178== HEAP SUMMARY:
==41178==     in use at exit: 0 bytes in 0 blocks
==41178==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41178== 
==41178== All heap blocks were freed -- no leaks are possible
==41178== 
==41178== For counts of detected and suppressed errors, rerun with: -v
==41178== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
