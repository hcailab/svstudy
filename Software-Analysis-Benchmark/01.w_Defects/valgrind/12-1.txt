==69353== Memcheck, a memory error detector
==69353== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==69353== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==69353== Command: ./01_w_Defects 12001
==69353== 
==69353== Invalid free() / delete / delete[] / realloc()
==69353==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69353==    by 0x40D56C: double_free_001 (double_free.c:22)
==69353==    by 0x40D8D3: double_free_main (double_free.c:235)
==69353==    by 0x401925: main (main.c:92)
==69353==  Address 0x57ea480 is 0 bytes inside a block of size 1 free'd
==69353==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69353==    by 0x40D560: double_free_001 (double_free.c:20)
==69353==    by 0x40D8D3: double_free_main (double_free.c:235)
==69353==    by 0x401925: main (main.c:92)
==69353==  Block was alloc'd at
==69353==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69353==    by 0x40D550: double_free_001 (double_free.c:19)
==69353==    by 0x40D8D3: double_free_main (double_free.c:235)
==69353==    by 0x401925: main (main.c:92)
==69353== 
==69353== 
==69353== HEAP SUMMARY:
==69353==     in use at exit: 0 bytes in 0 blocks
==69353==   total heap usage: 2 allocs, 3 frees, 1,025 bytes allocated
==69353== 
==69353== All heap blocks were freed -- no leaks are possible
==69353== 
==69353== For counts of detected and suppressed errors, rerun with: -v
==69353== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
