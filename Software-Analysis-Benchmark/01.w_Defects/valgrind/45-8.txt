==41609== Memcheck, a memory error detector
==41609== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41609== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41609== Command: ./01_w_Defects 45008
==41609== 
==41609== 
==41609== HEAP SUMMARY:
==41609==     in use at exit: 0 bytes in 0 blocks
==41609==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41609== 
==41609== All heap blocks were freed -- no leaks are possible
==41609== 
==41609== For counts of detected and suppressed errors, rerun with: -v
==41609== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
