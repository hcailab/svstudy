==38516== Memcheck, a memory error detector
==38516== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38516== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38516== Command: ./01_w_Defects 29040
==38516== 
==38516== 
==38516== HEAP SUMMARY:
==38516==     in use at exit: 0 bytes in 0 blocks
==38516==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38516== 
==38516== All heap blocks were freed -- no leaks are possible
==38516== 
==38516== For counts of detected and suppressed errors, rerun with: -v
==38516== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
