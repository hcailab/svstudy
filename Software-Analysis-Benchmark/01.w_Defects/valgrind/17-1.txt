==70999== Memcheck, a memory error detector
==70999== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==70999== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==70999== Command: ./01_w_Defects 17001
==70999== 
==70999== 
==70999== HEAP SUMMARY:
==70999==     in use at exit: 0 bytes in 0 blocks
==70999==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==70999== 
==70999== All heap blocks were freed -- no leaks are possible
==70999== 
==70999== For counts of detected and suppressed errors, rerun with: -v
==70999== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
