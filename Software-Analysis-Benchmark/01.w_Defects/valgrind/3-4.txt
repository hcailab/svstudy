==72138== Memcheck, a memory error detector
==72138== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==72138== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==72138== Command: ./01_w_Defects 3004
==72138== 
==72138== Invalid write of size 4
==72138==    at 0x404539: dynamic_buffer_underrun_004 (buffer_underrun_dynamic.c:79)
==72138==    by 0x405660: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:809)
==72138==    by 0x4017F3: main (main.c:38)
==72138==  Address 0x57ea46c is 20 bytes before a block of size 20 alloc'd
==72138==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==72138==    by 0x404525: dynamic_buffer_underrun_004 (buffer_underrun_dynamic.c:76)
==72138==    by 0x405660: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:809)
==72138==    by 0x4017F3: main (main.c:38)
==72138== 
==72138== 
==72138== HEAP SUMMARY:
==72138==     in use at exit: 0 bytes in 0 blocks
==72138==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==72138== 
==72138== All heap blocks were freed -- no leaks are possible
==72138== 
==72138== For counts of detected and suppressed errors, rerun with: -v
==72138== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
