==40329== Memcheck, a memory error detector
==40329== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40329== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40329== Command: ./01_w_Defects 38037
==40329== 
==40329== 
==40329== HEAP SUMMARY:
==40329==     in use at exit: 0 bytes in 0 blocks
==40329==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40329== 
==40329== All heap blocks were freed -- no leaks are possible
==40329== 
==40329== For counts of detected and suppressed errors, rerun with: -v
==40329== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
