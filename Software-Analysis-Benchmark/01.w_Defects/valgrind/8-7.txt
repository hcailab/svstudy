==71527== Memcheck, a memory error detector
==71527== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==71527== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==71527== Command: ./01_w_Defects 8007
==71527== 
==71527== 
==71527== HEAP SUMMARY:
==71527==     in use at exit: 0 bytes in 0 blocks
==71527==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==71527== 
==71527== All heap blocks were freed -- no leaks are possible
==71527== 
==71527== For counts of detected and suppressed errors, rerun with: -v
==71527== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
