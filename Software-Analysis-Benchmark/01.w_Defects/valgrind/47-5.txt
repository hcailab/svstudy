==42043== Memcheck, a memory error detector
==42043== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42043== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42043== Command: ./01_w_Defects 47005
==42043== 
==42043== 
==42043== HEAP SUMMARY:
==42043==     in use at exit: 0 bytes in 0 blocks
==42043==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42043== 
==42043== All heap blocks were freed -- no leaks are possible
==42043== 
==42043== For counts of detected and suppressed errors, rerun with: -v
==42043== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
