==40813== Memcheck, a memory error detector
==40813== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40813== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40813== Command: ./01_w_Defects 41036
==40813== 
==40813== 
==40813== HEAP SUMMARY:
==40813==     in use at exit: 0 bytes in 0 blocks
==40813==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40813== 
==40813== All heap blocks were freed -- no leaks are possible
==40813== 
==40813== For counts of detected and suppressed errors, rerun with: -v
==40813== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
