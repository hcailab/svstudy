==35867== Memcheck, a memory error detector
==35867== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35867== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35867== Command: ./01_w_Defects 17033
==35867== 
==35867== 
==35867== HEAP SUMMARY:
==35867==     in use at exit: 0 bytes in 0 blocks
==35867==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35867== 
==35867== All heap blocks were freed -- no leaks are possible
==35867== 
==35867== For counts of detected and suppressed errors, rerun with: -v
==35867== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
