==34659== Memcheck, a memory error detector
==34659== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34659== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34659== Command: ./01_w_Defects 11012
==34659== 
==34659== 
==34659== HEAP SUMMARY:
==34659==     in use at exit: 0 bytes in 0 blocks
==34659==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34659== 
==34659== All heap blocks were freed -- no leaks are possible
==34659== 
==34659== For counts of detected and suppressed errors, rerun with: -v
==34659== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
