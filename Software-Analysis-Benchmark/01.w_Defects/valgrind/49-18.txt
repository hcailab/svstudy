==42622== Memcheck, a memory error detector
==42622== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42622== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42622== Command: ./01_w_Defects 49018
==42622== 
==42622== 
==42622== HEAP SUMMARY:
==42622==     in use at exit: 0 bytes in 0 blocks
==42622==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42622== 
==42622== All heap blocks were freed -- no leaks are possible
==42622== 
==42622== For counts of detected and suppressed errors, rerun with: -v
==42622== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
