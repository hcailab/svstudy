==43190== Memcheck, a memory error detector
==43190== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43190== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43190== Command: ./01_w_Defects 51042
==43190== 
==43190== 
==43190== HEAP SUMMARY:
==43190==     in use at exit: 0 bytes in 0 blocks
==43190==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43190== 
==43190== All heap blocks were freed -- no leaks are possible
==43190== 
==43190== For counts of detected and suppressed errors, rerun with: -v
==43190== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
