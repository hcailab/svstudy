==39607== Memcheck, a memory error detector
==39607== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39607== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39607== Command: ./01_w_Defects 34029
==39607== 
==39607== 
==39607== HEAP SUMMARY:
==39607==     in use at exit: 0 bytes in 0 blocks
==39607==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39607== 
==39607== All heap blocks were freed -- no leaks are possible
==39607== 
==39607== For counts of detected and suppressed errors, rerun with: -v
==39607== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
