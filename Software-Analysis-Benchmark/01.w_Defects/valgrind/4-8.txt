==33125== Memcheck, a memory error detector
==33125== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33125== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33125== Command: ./01_w_Defects 4008
==33125== 
==33125== 
==33125== HEAP SUMMARY:
==33125==     in use at exit: 0 bytes in 0 blocks
==33125==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33125== 
==33125== All heap blocks were freed -- no leaks are possible
==33125== 
==33125== For counts of detected and suppressed errors, rerun with: -v
==33125== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
