==40340== Memcheck, a memory error detector
==40340== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40340== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40340== Command: ./01_w_Defects 38040
==40340== 
==40340== 
==40340== HEAP SUMMARY:
==40340==     in use at exit: 0 bytes in 0 blocks
==40340==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40340== 
==40340== All heap blocks were freed -- no leaks are possible
==40340== 
==40340== For counts of detected and suppressed errors, rerun with: -v
==40340== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
