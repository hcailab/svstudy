==35154== Memcheck, a memory error detector
==35154== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35154== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35154== Command: ./01_w_Defects 14035
==35154== 
==35154== 
==35154== HEAP SUMMARY:
==35154==     in use at exit: 0 bytes in 0 blocks
==35154==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35154== 
==35154== All heap blocks were freed -- no leaks are possible
==35154== 
==35154== For counts of detected and suppressed errors, rerun with: -v
==35154== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
