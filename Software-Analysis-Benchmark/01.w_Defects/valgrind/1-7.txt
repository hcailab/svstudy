==32440== Memcheck, a memory error detector
==32440== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32440== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32440== Command: ./01_w_Defects 1007
==32440== 
==32440== 
==32440== HEAP SUMMARY:
==32440==     in use at exit: 0 bytes in 0 blocks
==32440==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32440== 
==32440== All heap blocks were freed -- no leaks are possible
==32440== 
==32440== For counts of detected and suppressed errors, rerun with: -v
==32440== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
