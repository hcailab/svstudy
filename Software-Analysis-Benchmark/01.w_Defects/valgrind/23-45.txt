==37012== Memcheck, a memory error detector
==37012== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37012== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37012== Command: ./01_w_Defects 23045
==37012== 
==37012== 
==37012== HEAP SUMMARY:
==37012==     in use at exit: 0 bytes in 0 blocks
==37012==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37012== 
==37012== All heap blocks were freed -- no leaks are possible
==37012== 
==37012== For counts of detected and suppressed errors, rerun with: -v
==37012== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
