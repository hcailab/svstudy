==37155== Memcheck, a memory error detector
==37155== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37155== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37155== Command: ./01_w_Defects 24026
==37155== 
==37155== 
==37155== HEAP SUMMARY:
==37155==     in use at exit: 0 bytes in 0 blocks
==37155==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37155== 
==37155== All heap blocks were freed -- no leaks are possible
==37155== 
==37155== For counts of detected and suppressed errors, rerun with: -v
==37155== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
