==35856== Memcheck, a memory error detector
==35856== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35856== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35856== Command: ./01_w_Defects 17030
==35856== 
==35856== 
==35856== HEAP SUMMARY:
==35856==     in use at exit: 0 bytes in 0 blocks
==35856==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35856== 
==35856== All heap blocks were freed -- no leaks are possible
==35856== 
==35856== For counts of detected and suppressed errors, rerun with: -v
==35856== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
