==37380== Memcheck, a memory error detector
==37380== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37380== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37380== Command: ./01_w_Defects 25019
==37380== 
==37380== 
==37380== HEAP SUMMARY:
==37380==     in use at exit: 0 bytes in 0 blocks
==37380==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37380== 
==37380== All heap blocks were freed -- no leaks are possible
==37380== 
==37380== For counts of detected and suppressed errors, rerun with: -v
==37380== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
