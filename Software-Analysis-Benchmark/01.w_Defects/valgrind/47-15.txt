==42085== Memcheck, a memory error detector
==42085== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42085== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42085== Command: ./01_w_Defects 47015
==42085== 
==42085== 
==42085== HEAP SUMMARY:
==42085==     in use at exit: 0 bytes in 0 blocks
==42085==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42085== 
==42085== All heap blocks were freed -- no leaks are possible
==42085== 
==42085== For counts of detected and suppressed errors, rerun with: -v
==42085== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
