==40832== Memcheck, a memory error detector
==40832== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40832== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40832== Command: ./01_w_Defects 41042
==40832== 
==40832== 
==40832== HEAP SUMMARY:
==40832==     in use at exit: 0 bytes in 0 blocks
==40832==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40832== 
==40832== All heap blocks were freed -- no leaks are possible
==40832== 
==40832== For counts of detected and suppressed errors, rerun with: -v
==40832== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
