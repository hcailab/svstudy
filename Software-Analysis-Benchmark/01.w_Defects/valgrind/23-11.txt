==36881== Memcheck, a memory error detector
==36881== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36881== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36881== Command: ./01_w_Defects 23011
==36881== 
==36881== 
==36881== HEAP SUMMARY:
==36881==     in use at exit: 0 bytes in 0 blocks
==36881==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36881== 
==36881== All heap blocks were freed -- no leaks are possible
==36881== 
==36881== For counts of detected and suppressed errors, rerun with: -v
==36881== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
