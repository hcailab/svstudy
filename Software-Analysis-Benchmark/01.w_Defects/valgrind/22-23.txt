==36711== Memcheck, a memory error detector
==36711== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36711== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36711== Command: ./01_w_Defects 22023
==36711== 
==36711== 
==36711== HEAP SUMMARY:
==36711==     in use at exit: 0 bytes in 0 blocks
==36711==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36711== 
==36711== All heap blocks were freed -- no leaks are possible
==36711== 
==36711== For counts of detected and suppressed errors, rerun with: -v
==36711== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
