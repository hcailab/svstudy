==35916== Memcheck, a memory error detector
==35916== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35916== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35916== Command: ./01_w_Defects 17046
==35916== 
==35916== 
==35916== HEAP SUMMARY:
==35916==     in use at exit: 0 bytes in 0 blocks
==35916==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35916== 
==35916== All heap blocks were freed -- no leaks are possible
==35916== 
==35916== For counts of detected and suppressed errors, rerun with: -v
==35916== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
