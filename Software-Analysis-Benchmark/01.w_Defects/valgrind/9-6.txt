==34182== Memcheck, a memory error detector
==34182== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34182== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34182== Command: ./01_w_Defects 9006
==34182== 
==34182== 
==34182== HEAP SUMMARY:
==34182==     in use at exit: 0 bytes in 0 blocks
==34182==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34182== 
==34182== All heap blocks were freed -- no leaks are possible
==34182== 
==34182== For counts of detected and suppressed errors, rerun with: -v
==34182== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
