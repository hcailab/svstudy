==40370== Memcheck, a memory error detector
==40370== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40370== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40370== Command: ./01_w_Defects 38049
==40370== 
==40370== 
==40370== HEAP SUMMARY:
==40370==     in use at exit: 0 bytes in 0 blocks
==40370==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40370== 
==40370== All heap blocks were freed -- no leaks are possible
==40370== 
==40370== For counts of detected and suppressed errors, rerun with: -v
==40370== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
