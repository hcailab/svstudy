==41731== Memcheck, a memory error detector
==41731== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41731== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41731== Command: ./01_w_Defects 45038
==41731== 
==41731== 
==41731== HEAP SUMMARY:
==41731==     in use at exit: 0 bytes in 0 blocks
==41731==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41731== 
==41731== All heap blocks were freed -- no leaks are possible
==41731== 
==41731== For counts of detected and suppressed errors, rerun with: -v
==41731== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
