==40292== Memcheck, a memory error detector
==40292== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40292== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40292== Command: ./01_w_Defects 38028
==40292== 
==40292== 
==40292== HEAP SUMMARY:
==40292==     in use at exit: 0 bytes in 0 blocks
==40292==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40292== 
==40292== All heap blocks were freed -- no leaks are possible
==40292== 
==40292== For counts of detected and suppressed errors, rerun with: -v
==40292== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
