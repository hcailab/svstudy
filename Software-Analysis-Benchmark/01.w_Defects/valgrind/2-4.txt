==71716== Memcheck, a memory error detector
==71716== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==71716== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==71716== Command: ./01_w_Defects 2004
==71716== 
==71716== Invalid write of size 4
==71716==    at 0x401FBB: dynamic_buffer_overrun_004 (buffer_overrun_dynamic.c:77)
==71716==    by 0x402C42: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:636)
==71716==    by 0x4017D1: main (main.c:32)
==71716==  Address 0x57ea494 is 0 bytes after a block of size 20 alloc'd
==71716==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==71716==    by 0x401FA7: dynamic_buffer_overrun_004 (buffer_overrun_dynamic.c:74)
==71716==    by 0x402C42: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:636)
==71716==    by 0x4017D1: main (main.c:32)
==71716== 
==71716== 
==71716== HEAP SUMMARY:
==71716==     in use at exit: 0 bytes in 0 blocks
==71716==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==71716== 
==71716== All heap blocks were freed -- no leaks are possible
==71716== 
==71716== For counts of detected and suppressed errors, rerun with: -v
==71716== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
