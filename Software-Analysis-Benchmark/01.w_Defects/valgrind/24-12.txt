==69150== Memcheck, a memory error detector
==69150== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==69150== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==69150== Command: ./01_w_Defects 24012
==69150== 
==69150== Invalid read of size 4
==69150==    at 0x417ED9: invalid_memory_access_012_func_001 (invalid_memory_access.c:371)
==69150==    by 0x417EF3: invalid_memory_access_012 (invalid_memory_access.c:377)
==69150==    by 0x418715: invalid_memory_access_main (invalid_memory_access.c:720)
==69150==    by 0x401AAC: main (main.c:164)
==69150==  Address 0x57ea480 is 0 bytes inside a block of size 12 free'd
==69150==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69150==    by 0x417E82: invalid_memory_access_012_func_001 (invalid_memory_access.c:349)
==69150==    by 0x417EF3: invalid_memory_access_012 (invalid_memory_access.c:377)
==69150==    by 0x418715: invalid_memory_access_main (invalid_memory_access.c:720)
==69150==    by 0x401AAC: main (main.c:164)
==69150==  Block was alloc'd at
==69150==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69150==    by 0x417E25: invalid_memory_access_012_func_001 (invalid_memory_access.c:337)
==69150==    by 0x417EF3: invalid_memory_access_012 (invalid_memory_access.c:377)
==69150==    by 0x418715: invalid_memory_access_main (invalid_memory_access.c:720)
==69150==    by 0x401AAC: main (main.c:164)
==69150== 
==69150== 
==69150== HEAP SUMMARY:
==69150==     in use at exit: 0 bytes in 0 blocks
==69150==   total heap usage: 2 allocs, 2 frees, 1,036 bytes allocated
==69150== 
==69150== All heap blocks were freed -- no leaks are possible
==69150== 
==69150== For counts of detected and suppressed errors, rerun with: -v
==69150== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
