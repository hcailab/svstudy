==41801== Memcheck, a memory error detector
==41801== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41801== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41801== Command: ./01_w_Defects 46007
==41801== 
==41801== 
==41801== HEAP SUMMARY:
==41801==     in use at exit: 0 bytes in 0 blocks
==41801==   total heap usage: 5 allocs, 5 frees, 1,056 bytes allocated
==41801== 
==41801== All heap blocks were freed -- no leaks are possible
==41801== 
==41801== For counts of detected and suppressed errors, rerun with: -v
==41801== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
