==39729== Memcheck, a memory error detector
==39729== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39729== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39729== Command: ./01_w_Defects 35011
==39729== 
==39729== 
==39729== HEAP SUMMARY:
==39729==     in use at exit: 0 bytes in 0 blocks
==39729==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39729== 
==39729== All heap blocks were freed -- no leaks are possible
==39729== 
==39729== For counts of detected and suppressed errors, rerun with: -v
==39729== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
