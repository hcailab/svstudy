==40116== Memcheck, a memory error detector
==40116== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40116== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40116== Command: ./01_w_Defects 37047
==40116== 
==40116== 
==40116== HEAP SUMMARY:
==40116==     in use at exit: 0 bytes in 0 blocks
==40116==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40116== 
==40116== All heap blocks were freed -- no leaks are possible
==40116== 
==40116== For counts of detected and suppressed errors, rerun with: -v
==40116== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
