==41470== Memcheck, a memory error detector
==41470== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41470== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41470== Command: ./01_w_Defects 44022
==41470== 
==41470== 
==41470== HEAP SUMMARY:
==41470==     in use at exit: 0 bytes in 0 blocks
==41470==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41470== 
==41470== All heap blocks were freed -- no leaks are possible
==41470== 
==41470== For counts of detected and suppressed errors, rerun with: -v
==41470== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
