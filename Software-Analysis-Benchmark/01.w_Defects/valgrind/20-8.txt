==36414== Memcheck, a memory error detector
==36414== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36414== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36414== Command: ./01_w_Defects 20008
==36414== 
==36414== 
==36414== HEAP SUMMARY:
==36414==     in use at exit: 0 bytes in 0 blocks
==36414==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36414== 
==36414== All heap blocks were freed -- no leaks are possible
==36414== 
==36414== For counts of detected and suppressed errors, rerun with: -v
==36414== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
