==43146== Memcheck, a memory error detector
==43146== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43146== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43146== Command: ./01_w_Defects 51031
==43146== 
==43146== 
==43146== HEAP SUMMARY:
==43146==     in use at exit: 0 bytes in 0 blocks
==43146==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43146== 
==43146== All heap blocks were freed -- no leaks are possible
==43146== 
==43146== For counts of detected and suppressed errors, rerun with: -v
==43146== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
