==33077== Memcheck, a memory error detector
==33077== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33077== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33077== Command: ./01_w_Defects 3049
==33077== 
==33077== 
==33077== HEAP SUMMARY:
==33077==     in use at exit: 0 bytes in 0 blocks
==33077==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33077== 
==33077== All heap blocks were freed -- no leaks are possible
==33077== 
==33077== For counts of detected and suppressed errors, rerun with: -v
==33077== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
