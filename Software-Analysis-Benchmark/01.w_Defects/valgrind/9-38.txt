==34322== Memcheck, a memory error detector
==34322== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34322== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34322== Command: ./01_w_Defects 9038
==34322== 
==34322== 
==34322== HEAP SUMMARY:
==34322==     in use at exit: 0 bytes in 0 blocks
==34322==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34322== 
==34322== All heap blocks were freed -- no leaks are possible
==34322== 
==34322== For counts of detected and suppressed errors, rerun with: -v
==34322== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
