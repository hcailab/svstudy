==41931== Memcheck, a memory error detector
==41931== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41931== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41931== Command: ./01_w_Defects 46027
==41931== 
==41931== 
==41931== HEAP SUMMARY:
==41931==     in use at exit: 0 bytes in 0 blocks
==41931==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41931== 
==41931== All heap blocks were freed -- no leaks are possible
==41931== 
==41931== For counts of detected and suppressed errors, rerun with: -v
==41931== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
