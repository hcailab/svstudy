==33983== Memcheck, a memory error detector
==33983== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33983== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33983== Command: ./01_w_Defects 8019
==33983== 
==33983== 
==33983== HEAP SUMMARY:
==33983==     in use at exit: 0 bytes in 0 blocks
==33983==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33983== 
==33983== All heap blocks were freed -- no leaks are possible
==33983== 
==33983== For counts of detected and suppressed errors, rerun with: -v
==33983== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
