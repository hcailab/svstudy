==33665== Memcheck, a memory error detector
==33665== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33665== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33665== Command: ./01_w_Defects 6039
==33665== 
==33665== 
==33665== HEAP SUMMARY:
==33665==     in use at exit: 0 bytes in 0 blocks
==33665==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33665== 
==33665== All heap blocks were freed -- no leaks are possible
==33665== 
==33665== For counts of detected and suppressed errors, rerun with: -v
==33665== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
