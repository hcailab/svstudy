==40090== Memcheck, a memory error detector
==40090== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40090== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40090== Command: ./01_w_Defects 37040
==40090== 
==40090== 
==40090== HEAP SUMMARY:
==40090==     in use at exit: 0 bytes in 0 blocks
==40090==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40090== 
==40090== All heap blocks were freed -- no leaks are possible
==40090== 
==40090== For counts of detected and suppressed errors, rerun with: -v
==40090== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
