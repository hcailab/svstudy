==38441== Memcheck, a memory error detector
==38441== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38441== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38441== Command: ./01_w_Defects 29026
==38441== 
==38441== 
==38441== HEAP SUMMARY:
==38441==     in use at exit: 0 bytes in 0 blocks
==38441==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38441== 
==38441== All heap blocks were freed -- no leaks are possible
==38441== 
==38441== For counts of detected and suppressed errors, rerun with: -v
==38441== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
