==33434== Memcheck, a memory error detector
==33434== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33434== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33434== Command: ./01_w_Defects 5032
==33434== 
==33434== 
==33434== HEAP SUMMARY:
==33434==     in use at exit: 0 bytes in 0 blocks
==33434==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33434== 
==33434== All heap blocks were freed -- no leaks are possible
==33434== 
==33434== For counts of detected and suppressed errors, rerun with: -v
==33434== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
