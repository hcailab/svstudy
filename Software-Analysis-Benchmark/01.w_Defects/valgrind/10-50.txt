==34619== Memcheck, a memory error detector
==34619== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34619== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34619== Command: ./01_w_Defects 10050
==34619== 
==34619== 
==34619== HEAP SUMMARY:
==34619==     in use at exit: 0 bytes in 0 blocks
==34619==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34619== 
==34619== All heap blocks were freed -- no leaks are possible
==34619== 
==34619== For counts of detected and suppressed errors, rerun with: -v
==34619== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
