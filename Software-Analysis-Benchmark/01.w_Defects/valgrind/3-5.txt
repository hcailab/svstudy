==72140== Memcheck, a memory error detector
==72140== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==72140== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==72140== Command: ./01_w_Defects 3005
==72140== 
==72140== Invalid write of size 8
==72140==    at 0x40458D: dynamic_buffer_underrun_005 (buffer_underrun_dynamic.c:96)
==72140==    by 0x405682: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:814)
==72140==    by 0x4017F3: main (main.c:38)
==72140==  Address 0x57ea478 is 8 bytes before a block of size 40 alloc'd
==72140==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==72140==    by 0x404564: dynamic_buffer_underrun_005 (buffer_underrun_dynamic.c:90)
==72140==    by 0x405682: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:814)
==72140==    by 0x4017F3: main (main.c:38)
==72140== 
==72140== 
==72140== HEAP SUMMARY:
==72140==     in use at exit: 0 bytes in 0 blocks
==72140==   total heap usage: 2 allocs, 2 frees, 1,064 bytes allocated
==72140== 
==72140== All heap blocks were freed -- no leaks are possible
==72140== 
==72140== For counts of detected and suppressed errors, rerun with: -v
==72140== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
