==36345== Memcheck, a memory error detector
==36345== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36345== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36345== Command: ./01_w_Defects 19041
==36345== 
==36345== 
==36345== HEAP SUMMARY:
==36345==     in use at exit: 0 bytes in 0 blocks
==36345==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36345== 
==36345== All heap blocks were freed -- no leaks are possible
==36345== 
==36345== For counts of detected and suppressed errors, rerun with: -v
==36345== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
