==35902== Memcheck, a memory error detector
==35902== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35902== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35902== Command: ./01_w_Defects 17042
==35902== 
==35902== 
==35902== HEAP SUMMARY:
==35902==     in use at exit: 0 bytes in 0 blocks
==35902==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35902== 
==35902== All heap blocks were freed -- no leaks are possible
==35902== 
==35902== For counts of detected and suppressed errors, rerun with: -v
==35902== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
