==36182== Memcheck, a memory error detector
==36182== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36182== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36182== Command: ./01_w_Defects 19001
==36182== 
==36182== 
==36182== HEAP SUMMARY:
==36182==     in use at exit: 0 bytes in 0 blocks
==36182==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36182== 
==36182== All heap blocks were freed -- no leaks are possible
==36182== 
==36182== For counts of detected and suppressed errors, rerun with: -v
==36182== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
