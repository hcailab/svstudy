==42907== Memcheck, a memory error detector
==42907== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42907== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42907== Command: ./01_w_Defects 50034
==42907== 
==42907== 
==42907== HEAP SUMMARY:
==42907==     in use at exit: 0 bytes in 0 blocks
==42907==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42907== 
==42907== All heap blocks were freed -- no leaks are possible
==42907== 
==42907== For counts of detected and suppressed errors, rerun with: -v
==42907== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
