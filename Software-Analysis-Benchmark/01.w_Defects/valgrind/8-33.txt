==34047== Memcheck, a memory error detector
==34047== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34047== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34047== Command: ./01_w_Defects 8033
==34047== 
==34047== 
==34047== HEAP SUMMARY:
==34047==     in use at exit: 0 bytes in 0 blocks
==34047==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34047== 
==34047== All heap blocks were freed -- no leaks are possible
==34047== 
==34047== For counts of detected and suppressed errors, rerun with: -v
==34047== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
