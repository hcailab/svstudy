==40984== Memcheck, a memory error detector
==40984== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40984== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40984== Command: ./01_w_Defects 42030
==40984== 
==40984== 
==40984== HEAP SUMMARY:
==40984==     in use at exit: 0 bytes in 0 blocks
==40984==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40984== 
==40984== All heap blocks were freed -- no leaks are possible
==40984== 
==40984== For counts of detected and suppressed errors, rerun with: -v
==40984== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
