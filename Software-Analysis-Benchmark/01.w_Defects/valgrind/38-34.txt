==40313== Memcheck, a memory error detector
==40313== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40313== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40313== Command: ./01_w_Defects 38034
==40313== 
==40313== 
==40313== HEAP SUMMARY:
==40313==     in use at exit: 0 bytes in 0 blocks
==40313==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40313== 
==40313== All heap blocks were freed -- no leaks are possible
==40313== 
==40313== For counts of detected and suppressed errors, rerun with: -v
==40313== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
