==38409== Memcheck, a memory error detector
==38409== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38409== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38409== Command: ./01_w_Defects 29017
==38409== 
==38409== 
==38409== HEAP SUMMARY:
==38409==     in use at exit: 880 bytes in 11 blocks
==38409==   total heap usage: 12 allocs, 1 frees, 1,904 bytes allocated
==38409== 
==38409== LEAK SUMMARY:
==38409==    definitely lost: 0 bytes in 0 blocks
==38409==    indirectly lost: 0 bytes in 0 blocks
==38409==      possibly lost: 0 bytes in 0 blocks
==38409==    still reachable: 880 bytes in 11 blocks
==38409==         suppressed: 0 bytes in 0 blocks
==38409== Reachable blocks (those to which a pointer was found) are not shown.
==38409== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==38409== 
==38409== For counts of detected and suppressed errors, rerun with: -v
==38409== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
