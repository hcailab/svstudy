==42463== Memcheck, a memory error detector
==42463== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42463== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42463== Command: ./01_w_Defects 48037
==42463== 
==42463== 
==42463== HEAP SUMMARY:
==42463==     in use at exit: 0 bytes in 0 blocks
==42463==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42463== 
==42463== All heap blocks were freed -- no leaks are possible
==42463== 
==42463== For counts of detected and suppressed errors, rerun with: -v
==42463== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
