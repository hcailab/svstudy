==38658== Memcheck, a memory error detector
==38658== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38658== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38658== Command: ./01_w_Defects 30031
==38658== 
==38658== 
==38658== HEAP SUMMARY:
==38658==     in use at exit: 0 bytes in 0 blocks
==38658==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38658== 
==38658== All heap blocks were freed -- no leaks are possible
==38658== 
==38658== For counts of detected and suppressed errors, rerun with: -v
==38658== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
