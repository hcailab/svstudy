==38586== Memcheck, a memory error detector
==38586== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38586== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38586== Command: ./01_w_Defects 30009
==38586== 
==38586== 
==38586== HEAP SUMMARY:
==38586==     in use at exit: 0 bytes in 0 blocks
==38586==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38586== 
==38586== All heap blocks were freed -- no leaks are possible
==38586== 
==38586== For counts of detected and suppressed errors, rerun with: -v
==38586== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
