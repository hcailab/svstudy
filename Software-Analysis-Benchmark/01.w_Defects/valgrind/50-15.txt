==42841== Memcheck, a memory error detector
==42841== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42841== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42841== Command: ./01_w_Defects 50015
==42841== 
==42841== Invalid read of size 1
==42841==    at 0x54AF540: __strcpy_sse2_unaligned (strcpy-sse2-unaligned.S:47)
==42841==    by 0x11B641: strcpy (string_fortified.h:90)
==42841==    by 0x11B641: wrong_arguments_func_pointer_015_func_002 (wrong_arguments_func_pointer.c:438)
==42841==    by 0x11B68F: wrong_arguments_func_pointer_015 (wrong_arguments_func_pointer.c:456)
==42841==    by 0x11BB72: wrong_arguments_func_pointer_main (wrong_arguments_func_pointer.c:681)
==42841==    by 0x109AF8: main (main.c:320)
==42841==  Address 0x33 is not stack'd, malloc'd or (recently) free'd
==42841== 
==42841== 
==42841== Process terminating with default action of signal 11 (SIGSEGV)
==42841==  Access not within mapped region at address 0x33
==42841==    at 0x54AF540: __strcpy_sse2_unaligned (strcpy-sse2-unaligned.S:47)
==42841==    by 0x11B641: strcpy (string_fortified.h:90)
==42841==    by 0x11B641: wrong_arguments_func_pointer_015_func_002 (wrong_arguments_func_pointer.c:438)
==42841==    by 0x11B68F: wrong_arguments_func_pointer_015 (wrong_arguments_func_pointer.c:456)
==42841==    by 0x11BB72: wrong_arguments_func_pointer_main (wrong_arguments_func_pointer.c:681)
==42841==    by 0x109AF8: main (main.c:320)
==42841==  If you believe this happened as a result of a stack
==42841==  overflow in your program's main thread (unlikely but
==42841==  possible), you can try to increase the size of the
==42841==  main thread stack using the --main-stacksize= flag.
==42841==  The main thread stack size used in this run was 8388608.
==42841== 
==42841== HEAP SUMMARY:
==42841==     in use at exit: 115 bytes in 6 blocks
==42841==   total heap usage: 7 allocs, 1 frees, 1,139 bytes allocated
==42841== 
==42841== LEAK SUMMARY:
==42841==    definitely lost: 0 bytes in 0 blocks
==42841==    indirectly lost: 0 bytes in 0 blocks
==42841==      possibly lost: 0 bytes in 0 blocks
==42841==    still reachable: 115 bytes in 6 blocks
==42841==         suppressed: 0 bytes in 0 blocks
==42841== Reachable blocks (those to which a pointer was found) are not shown.
==42841== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==42841== 
==42841== For counts of detected and suppressed errors, rerun with: -v
==42841== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
