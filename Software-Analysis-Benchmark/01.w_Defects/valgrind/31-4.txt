==38783== Memcheck, a memory error detector
==38783== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38783== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38783== Command: ./01_w_Defects 31004
==38783== 
==38783== Invalid write of size 4
==38783==    at 0x10DB40: null_pointer_004 (null_pointer.c:63)
==38783==    by 0x10DB40: null_pointer_main (null_pointer.c:373)
==38783==    by 0x109BDC: main (main.c:206)
==38783==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
==38783== 
==38783== 
==38783== Process terminating with default action of signal 11 (SIGSEGV)
==38783==  Access not within mapped region at address 0x0
==38783==    at 0x10DB40: null_pointer_004 (null_pointer.c:63)
==38783==    by 0x10DB40: null_pointer_main (null_pointer.c:373)
==38783==    by 0x109BDC: main (main.c:206)
==38783==  If you believe this happened as a result of a stack
==38783==  overflow in your program's main thread (unlikely but
==38783==  possible), you can try to increase the size of the
==38783==  main thread stack using the --main-stacksize= flag.
==38783==  The main thread stack size used in this run was 8388608.
==38783== 
==38783== HEAP SUMMARY:
==38783==     in use at exit: 0 bytes in 0 blocks
==38783==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38783== 
==38783== All heap blocks were freed -- no leaks are possible
==38783== 
==38783== For counts of detected and suppressed errors, rerun with: -v
==38783== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
