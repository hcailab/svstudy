==34108== Memcheck, a memory error detector
==34108== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34108== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34108== Command: ./01_w_Defects 8041
==34108== 
==34108== 
==34108== HEAP SUMMARY:
==34108==     in use at exit: 0 bytes in 0 blocks
==34108==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34108== 
==34108== All heap blocks were freed -- no leaks are possible
==34108== 
==34108== For counts of detected and suppressed errors, rerun with: -v
==34108== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
