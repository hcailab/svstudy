==38709== Memcheck, a memory error detector
==38709== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38709== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38709== Command: ./01_w_Defects 30041
==38709== 
==38709== 
==38709== HEAP SUMMARY:
==38709==     in use at exit: 0 bytes in 0 blocks
==38709==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38709== 
==38709== All heap blocks were freed -- no leaks are possible
==38709== 
==38709== For counts of detected and suppressed errors, rerun with: -v
==38709== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
