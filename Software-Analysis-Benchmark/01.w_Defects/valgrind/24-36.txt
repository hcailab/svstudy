==37190== Memcheck, a memory error detector
==37190== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37190== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37190== Command: ./01_w_Defects 24036
==37190== 
==37190== 
==37190== HEAP SUMMARY:
==37190==     in use at exit: 0 bytes in 0 blocks
==37190==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37190== 
==37190== All heap blocks were freed -- no leaks are possible
==37190== 
==37190== For counts of detected and suppressed errors, rerun with: -v
==37190== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
