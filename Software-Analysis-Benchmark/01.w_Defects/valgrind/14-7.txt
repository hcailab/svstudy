==35044== Memcheck, a memory error detector
==35044== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35044== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35044== Command: ./01_w_Defects 14007
==35044== 
==35044== 
==35044== HEAP SUMMARY:
==35044==     in use at exit: 0 bytes in 0 blocks
==35044==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35044== 
==35044== All heap blocks were freed -- no leaks are possible
==35044== 
==35044== For counts of detected and suppressed errors, rerun with: -v
==35044== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
