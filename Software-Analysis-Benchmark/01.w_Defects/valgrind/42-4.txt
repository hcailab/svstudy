==67732== Memcheck, a memory error detector
==67732== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==67732== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==67732== Command: ./01_w_Defects 42004
==67732== 
==67732== 
==67732== Process terminating with default action of signal 11 (SIGSEGV)
==67732==  Access not within mapped region at address 0x1FFEEBEE08
==67732==    at 0x410B7B: st_overflow_004 (st_overflow.c:124)
==67732==  If you believe this happened as a result of a stack
==67732==  overflow in your program's main thread (unlikely but
==67732==  possible), you can try to increase the size of the
==67732==  main thread stack using the --main-stacksize= flag.
==67732==  The main thread stack size used in this run was 1048576.
==67732== 
==67732== Process terminating with default action of signal 11 (SIGSEGV)
==67732==  Access not within mapped region at address 0x1FFEEBEE00
==67732==    at 0x4A2A650: _vgnU_freeres (in /usr/lib/valgrind/vgpreload_core-amd64-linux.so)
==67732==  If you believe this happened as a result of a stack
==67732==  overflow in your program's main thread (unlikely but
==67732==  possible), you can try to increase the size of the
==67732==  main thread stack using the --main-stacksize= flag.
==67732==  The main thread stack size used in this run was 1048576.
==67732== 
==67732== HEAP SUMMARY:
==67732==     in use at exit: 1,024 bytes in 1 blocks
==67732==   total heap usage: 1 allocs, 0 frees, 1,024 bytes allocated
==67732== 
==67732== LEAK SUMMARY:
==67732==    definitely lost: 0 bytes in 0 blocks
==67732==    indirectly lost: 0 bytes in 0 blocks
==67732==      possibly lost: 0 bytes in 0 blocks
==67732==    still reachable: 1,024 bytes in 1 blocks
==67732==         suppressed: 0 bytes in 0 blocks
==67732== Rerun with --leak-check=full to see details of leaked memory
==67732== 
==67732== For counts of detected and suppressed errors, rerun with: -v
==67732== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
