==34568== Memcheck, a memory error detector
==34568== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34568== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34568== Command: ./01_w_Defects 10036
==34568== 
==34568== 
==34568== HEAP SUMMARY:
==34568==     in use at exit: 0 bytes in 0 blocks
==34568==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34568== 
==34568== All heap blocks were freed -- no leaks are possible
==34568== 
==34568== For counts of detected and suppressed errors, rerun with: -v
==34568== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
