==36768== Memcheck, a memory error detector
==36768== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36768== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36768== Command: ./01_w_Defects 22038
==36768== 
==36768== 
==36768== HEAP SUMMARY:
==36768==     in use at exit: 0 bytes in 0 blocks
==36768==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36768== 
==36768== All heap blocks were freed -- no leaks are possible
==36768== 
==36768== For counts of detected and suppressed errors, rerun with: -v
==36768== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
