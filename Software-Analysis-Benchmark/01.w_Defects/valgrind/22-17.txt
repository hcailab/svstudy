==36683== Memcheck, a memory error detector
==36683== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36683== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36683== Command: ./01_w_Defects 22017
==36683== 
==36683== 
==36683== HEAP SUMMARY:
==36683==     in use at exit: 0 bytes in 0 blocks
==36683==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36683== 
==36683== All heap blocks were freed -- no leaks are possible
==36683== 
==36683== For counts of detected and suppressed errors, rerun with: -v
==36683== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
