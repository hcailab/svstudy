==42258== Memcheck, a memory error detector
==42258== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42258== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42258== Command: ./01_w_Defects 47040
==42258== 
==42258== 
==42258== HEAP SUMMARY:
==42258==     in use at exit: 0 bytes in 0 blocks
==42258==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42258== 
==42258== All heap blocks were freed -- no leaks are possible
==42258== 
==42258== For counts of detected and suppressed errors, rerun with: -v
==42258== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
