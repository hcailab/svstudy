==71707== Memcheck, a memory error detector
==71707== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==71707== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==71707== Command: ./01_w_Defects 2002
==71707== 
==71707== Invalid write of size 2
==71707==    at 0x401F01: dynamic_buffer_overrun_002 (buffer_overrun_dynamic.c:42)
==71707==    by 0x402BFE: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:626)
==71707==    by 0x4017D1: main (main.c:32)
==71707==  Address 0x57ea48a is 0 bytes after a block of size 10 alloc'd
==71707==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==71707==    by 0x401EED: dynamic_buffer_overrun_002 (buffer_overrun_dynamic.c:39)
==71707==    by 0x402BFE: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:626)
==71707==    by 0x4017D1: main (main.c:32)
==71707== 
==71707== 
==71707== HEAP SUMMARY:
==71707==     in use at exit: 0 bytes in 0 blocks
==71707==   total heap usage: 2 allocs, 2 frees, 1,034 bytes allocated
==71707== 
==71707== All heap blocks were freed -- no leaks are possible
==71707== 
==71707== For counts of detected and suppressed errors, rerun with: -v
==71707== ERROR SUMMARY: 2 errors from 1 contexts (suppressed: 0 from 0)
