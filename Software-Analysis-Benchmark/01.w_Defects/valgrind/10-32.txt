==34549== Memcheck, a memory error detector
==34549== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34549== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34549== Command: ./01_w_Defects 10032
==34549== 
==34549== 
==34549== HEAP SUMMARY:
==34549==     in use at exit: 0 bytes in 0 blocks
==34549==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34549== 
==34549== All heap blocks were freed -- no leaks are possible
==34549== 
==34549== For counts of detected and suppressed errors, rerun with: -v
==34549== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
