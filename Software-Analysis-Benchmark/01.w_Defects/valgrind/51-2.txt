==42989== Memcheck, a memory error detector
==42989== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42989== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42989== Command: ./01_w_Defects 51002
==42989== 
==42989== 
==42989== HEAP SUMMARY:
==42989==     in use at exit: 0 bytes in 0 blocks
==42989==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42989== 
==42989== All heap blocks were freed -- no leaks are possible
==42989== 
==42989== For counts of detected and suppressed errors, rerun with: -v
==42989== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
