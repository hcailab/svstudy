==38225== Memcheck, a memory error detector
==38225== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38225== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38225== Command: ./01_w_Defects 28036
==38225== 
==38225== 
==38225== HEAP SUMMARY:
==38225==     in use at exit: 0 bytes in 0 blocks
==38225==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38225== 
==38225== All heap blocks were freed -- no leaks are possible
==38225== 
==38225== For counts of detected and suppressed errors, rerun with: -v
==38225== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
