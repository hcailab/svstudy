==39665== Memcheck, a memory error detector
==39665== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39665== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39665== Command: ./01_w_Defects 34044
==39665== 
==39665== 
==39665== HEAP SUMMARY:
==39665==     in use at exit: 0 bytes in 0 blocks
==39665==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39665== 
==39665== All heap blocks were freed -- no leaks are possible
==39665== 
==39665== For counts of detected and suppressed errors, rerun with: -v
==39665== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
