==36155== Memcheck, a memory error detector
==36155== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36155== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36155== Command: ./01_w_Defects 18044
==36155== 
==36155== 
==36155== HEAP SUMMARY:
==36155==     in use at exit: 0 bytes in 0 blocks
==36155==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36155== 
==36155== All heap blocks were freed -- no leaks are possible
==36155== 
==36155== For counts of detected and suppressed errors, rerun with: -v
==36155== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
