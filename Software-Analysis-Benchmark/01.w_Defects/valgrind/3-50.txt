==33079== Memcheck, a memory error detector
==33079== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33079== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33079== Command: ./01_w_Defects 3050
==33079== 
==33079== 
==33079== HEAP SUMMARY:
==33079==     in use at exit: 0 bytes in 0 blocks
==33079==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33079== 
==33079== All heap blocks were freed -- no leaks are possible
==33079== 
==33079== For counts of detected and suppressed errors, rerun with: -v
==33079== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
