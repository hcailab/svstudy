==41596== Memcheck, a memory error detector
==41596== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41596== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41596== Command: ./01_w_Defects 45004
==41596== 
==41596== 
==41596== HEAP SUMMARY:
==41596==     in use at exit: 0 bytes in 0 blocks
==41596==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==41596== 
==41596== All heap blocks were freed -- no leaks are possible
==41596== 
==41596== For counts of detected and suppressed errors, rerun with: -v
==41596== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
