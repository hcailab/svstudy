==38677== Memcheck, a memory error detector
==38677== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38677== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38677== Command: ./01_w_Defects 30034
==38677== 
==38677== 
==38677== HEAP SUMMARY:
==38677==     in use at exit: 0 bytes in 0 blocks
==38677==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38677== 
==38677== All heap blocks were freed -- no leaks are possible
==38677== 
==38677== For counts of detected and suppressed errors, rerun with: -v
==38677== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
