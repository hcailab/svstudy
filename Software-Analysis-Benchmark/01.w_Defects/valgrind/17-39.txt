==35891== Memcheck, a memory error detector
==35891== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35891== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35891== Command: ./01_w_Defects 17039
==35891== 
==35891== 
==35891== HEAP SUMMARY:
==35891==     in use at exit: 0 bytes in 0 blocks
==35891==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35891== 
==35891== All heap blocks were freed -- no leaks are possible
==35891== 
==35891== For counts of detected and suppressed errors, rerun with: -v
==35891== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
