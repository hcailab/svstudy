==39349== Memcheck, a memory error detector
==39349== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39349== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39349== Command: ./01_w_Defects 33023
==39349== 
==39349== 
==39349== HEAP SUMMARY:
==39349==     in use at exit: 0 bytes in 0 blocks
==39349==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39349== 
==39349== All heap blocks were freed -- no leaks are possible
==39349== 
==39349== For counts of detected and suppressed errors, rerun with: -v
==39349== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
