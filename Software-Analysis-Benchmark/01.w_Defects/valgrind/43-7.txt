==68604== Memcheck, a memory error detector
==68604== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==68604== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==68604== Command: ./01_w_Defects 43007
==68604== 
==68604== Conditional jump or move depends on uninitialised value(s)
==68604==    at 0x4C32D48: __strlen_sse2 (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==68604==    by 0x41323F: st_underrun_007_func_001 (st_underrun.c:225)
==68604==    by 0x4132FD: st_underrun_007 (st_underrun.c:251)
==68604==    by 0x413406: st_underrun_main (st_underrun.c:295)
==68604==    by 0x401D32: main (main.c:278)
==68604== 
==68604== Conditional jump or move depends on uninitialised value(s)
==68604==    at 0x41326B: st_underrun_007_func_001 (st_underrun.c:227)
==68604==    by 0x4132FD: st_underrun_007 (st_underrun.c:251)
==68604==    by 0x413406: st_underrun_main (st_underrun.c:295)
==68604==    by 0x401D32: main (main.c:278)
==68604== 
==68604== Invalid read of size 1
==68604==    at 0x413265: st_underrun_007_func_001 (st_underrun.c:227)
==68604==    by 0x4132FD: st_underrun_007 (st_underrun.c:251)
==68604==    by 0x413406: st_underrun_main (st_underrun.c:295)
==68604==    by 0x401D32: main (main.c:278)
==68604==  Address 0x1ffefffd0f is on thread 1's stack
==68604==  129 bytes below stack pointer
==68604== 
==68604== Invalid read of size 1
==68604==    at 0x413251: st_underrun_007_func_001 (st_underrun.c:229)
==68604==    by 0x4132FD: st_underrun_007 (st_underrun.c:251)
==68604==    by 0x413406: st_underrun_main (st_underrun.c:295)
==68604==    by 0x401D32: main (main.c:278)
==68604==  Address 0x1ffefffd0f is on thread 1's stack
==68604==  129 bytes below stack pointer
==68604== 
==68604== 
==68604== HEAP SUMMARY:
==68604==     in use at exit: 0 bytes in 0 blocks
==68604==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==68604== 
==68604== All heap blocks were freed -- no leaks are possible
==68604== 
==68604== For counts of detected and suppressed errors, rerun with: -v
==68604== Use --track-origins=yes to see where uninitialised values come from
==68604== ERROR SUMMARY: 2423 errors from 4 contexts (suppressed: 0 from 0)
