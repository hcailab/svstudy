==72223== Memcheck, a memory error detector
==72223== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==72223== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==72223== Command: ./01_w_Defects 3023
==72223== 
==72223== Invalid write of size 4
==72223==    at 0x404D50: dynamic_buffer_underrun_023 (buffer_underrun_dynamic.c:426)
==72223==    by 0x4058E6: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:904)
==72223==    by 0x4017F3: main (main.c:38)
==72223==  Address 0x57ea47c is 4 bytes before a block of size 20 alloc'd
==72223==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==72223==    by 0x404D1F: dynamic_buffer_underrun_023 (buffer_underrun_dynamic.c:418)
==72223==    by 0x4058E6: dynamic_buffer_underrun_main (buffer_underrun_dynamic.c:904)
==72223==    by 0x4017F3: main (main.c:38)
==72223== 
==72223== 
==72223== HEAP SUMMARY:
==72223==     in use at exit: 0 bytes in 0 blocks
==72223==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==72223== 
==72223== All heap blocks were freed -- no leaks are possible
==72223== 
==72223== For counts of detected and suppressed errors, rerun with: -v
==72223== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
