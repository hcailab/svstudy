==38376== Memcheck, a memory error detector
==38376== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38376== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38376== Command: ./01_w_Defects 29008
==38376== 
==38376== 
==38376== HEAP SUMMARY:
==38376==     in use at exit: 0 bytes in 0 blocks
==38376==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==38376== 
==38376== All heap blocks were freed -- no leaks are possible
==38376== 
==38376== For counts of detected and suppressed errors, rerun with: -v
==38376== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
