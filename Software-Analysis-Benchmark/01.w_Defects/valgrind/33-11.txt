==39285== Memcheck, a memory error detector
==39285== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39285== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39285== Command: ./01_w_Defects 33011
==39285== 
==39285== 
==39285== HEAP SUMMARY:
==39285==     in use at exit: 0 bytes in 0 blocks
==39285==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39285== 
==39285== All heap blocks were freed -- no leaks are possible
==39285== 
==39285== For counts of detected and suppressed errors, rerun with: -v
==39285== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
