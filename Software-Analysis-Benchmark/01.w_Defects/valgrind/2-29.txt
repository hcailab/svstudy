==71806== Memcheck, a memory error detector
==71806== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==71806== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==71806== Command: ./01_w_Defects 2029
==71806== 
==71806== Invalid write of size 1
==71806==    at 0x4029C6: dynamic_buffer_overrun_029 (buffer_overrun_dynamic.c:532)
==71806==    by 0x402F94: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:761)
==71806==    by 0x4017D1: main (main.c:32)
==71806==  Address 0x57ea482 is 1 bytes after a block of size 1 alloc'd
==71806==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==71806==    by 0x4029AA: dynamic_buffer_overrun_029 (buffer_overrun_dynamic.c:529)
==71806==    by 0x402F94: dynamic_buffer_overrun_main (buffer_overrun_dynamic.c:761)
==71806==    by 0x4017D1: main (main.c:32)
==71806== 
==71806== 
==71806== HEAP SUMMARY:
==71806==     in use at exit: 0 bytes in 0 blocks
==71806==   total heap usage: 2 allocs, 2 frees, 1,025 bytes allocated
==71806== 
==71806== All heap blocks were freed -- no leaks are possible
==71806== 
==71806== For counts of detected and suppressed errors, rerun with: -v
==71806== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
