==42285== Memcheck, a memory error detector
==42285== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42285== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42285== Command: ./01_w_Defects 47046
==42285== 
==42285== 
==42285== HEAP SUMMARY:
==42285==     in use at exit: 0 bytes in 0 blocks
==42285==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42285== 
==42285== All heap blocks were freed -- no leaks are possible
==42285== 
==42285== For counts of detected and suppressed errors, rerun with: -v
==42285== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
