==42019== Memcheck, a memory error detector
==42019== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42019== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42019== Command: ./01_w_Defects 46049
==42019== 
==42019== 
==42019== HEAP SUMMARY:
==42019==     in use at exit: 0 bytes in 0 blocks
==42019==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42019== 
==42019== All heap blocks were freed -- no leaks are possible
==42019== 
==42019== For counts of detected and suppressed errors, rerun with: -v
==42019== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
