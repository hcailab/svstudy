==36461== Memcheck, a memory error detector
==36461== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36461== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36461== Command: ./01_w_Defects 20014
==36461== 
==36461== 
==36461== HEAP SUMMARY:
==36461==     in use at exit: 0 bytes in 0 blocks
==36461==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36461== 
==36461== All heap blocks were freed -- no leaks are possible
==36461== 
==36461== For counts of detected and suppressed errors, rerun with: -v
==36461== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
