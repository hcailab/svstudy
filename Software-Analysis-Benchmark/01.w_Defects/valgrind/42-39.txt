==41017== Memcheck, a memory error detector
==41017== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41017== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41017== Command: ./01_w_Defects 42039
==41017== 
==41017== 
==41017== HEAP SUMMARY:
==41017==     in use at exit: 0 bytes in 0 blocks
==41017==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41017== 
==41017== All heap blocks were freed -- no leaks are possible
==41017== 
==41017== For counts of detected and suppressed errors, rerun with: -v
==41017== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
