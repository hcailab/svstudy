==42787== Memcheck, a memory error detector
==42787== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42787== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42787== Command: ./01_w_Defects 50009
==42787== 
==42787== Invalid read of size 1
==42787==    at 0x4C32D32: __strlen_sse2 (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==42787==    by 0x11B321: wrong_arguments_func_pointer_009_func_001 (wrong_arguments_func_pointer.c:211)
==42787==    by 0x11BAE3: wrong_arguments_func_pointer_main (wrong_arguments_func_pointer.c:651)
==42787==    by 0x109AF8: main (main.c:320)
==42787==  Address 0x53 is not stack'd, malloc'd or (recently) free'd
==42787== 
==42787== 
==42787== Process terminating with default action of signal 11 (SIGSEGV)
==42787==  Access not within mapped region at address 0x53
==42787==    at 0x4C32D32: __strlen_sse2 (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==42787==    by 0x11B321: wrong_arguments_func_pointer_009_func_001 (wrong_arguments_func_pointer.c:211)
==42787==    by 0x11BAE3: wrong_arguments_func_pointer_main (wrong_arguments_func_pointer.c:651)
==42787==    by 0x109AF8: main (main.c:320)
==42787==  If you believe this happened as a result of a stack
==42787==  overflow in your program's main thread (unlikely but
==42787==  possible), you can try to increase the size of the
==42787==  main thread stack using the --main-stacksize= flag.
==42787==  The main thread stack size used in this run was 8388608.
==42787== 
==42787== HEAP SUMMARY:
==42787==     in use at exit: 80 bytes in 1 blocks
==42787==   total heap usage: 2 allocs, 1 frees, 1,104 bytes allocated
==42787== 
==42787== LEAK SUMMARY:
==42787==    definitely lost: 0 bytes in 0 blocks
==42787==    indirectly lost: 0 bytes in 0 blocks
==42787==      possibly lost: 0 bytes in 0 blocks
==42787==    still reachable: 80 bytes in 1 blocks
==42787==         suppressed: 0 bytes in 0 blocks
==42787== Reachable blocks (those to which a pointer was found) are not shown.
==42787== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==42787== 
==42787== For counts of detected and suppressed errors, rerun with: -v
==42787== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
