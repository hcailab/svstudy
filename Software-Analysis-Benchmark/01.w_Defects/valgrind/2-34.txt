==32774== Memcheck, a memory error detector
==32774== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32774== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32774== Command: ./01_w_Defects 2034
==32774== 
==32774== 
==32774== HEAP SUMMARY:
==32774==     in use at exit: 0 bytes in 0 blocks
==32774==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32774== 
==32774== All heap blocks were freed -- no leaks are possible
==32774== 
==32774== For counts of detected and suppressed errors, rerun with: -v
==32774== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
