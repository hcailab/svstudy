==39272== Memcheck, a memory error detector
==39272== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39272== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39272== Command: ./01_w_Defects 33007
==39272== 
==39272== 
==39272== HEAP SUMMARY:
==39272==     in use at exit: 0 bytes in 0 blocks
==39272==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39272== 
==39272== All heap blocks were freed -- no leaks are possible
==39272== 
==39272== For counts of detected and suppressed errors, rerun with: -v
==39272== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
