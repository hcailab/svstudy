==42549== Memcheck, a memory error detector
==42549== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42549== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42549== Command: ./01_w_Defects 49007
==42549== 
==42549== 
==42549== HEAP SUMMARY:
==42549==     in use at exit: 0 bytes in 0 blocks
==42549==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42549== 
==42549== All heap blocks were freed -- no leaks are possible
==42549== 
==42549== For counts of detected and suppressed errors, rerun with: -v
==42549== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
