==42406== Memcheck, a memory error detector
==42406== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42406== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42406== Command: ./01_w_Defects 48022
==42406== 
==42406== 
==42406== HEAP SUMMARY:
==42406==     in use at exit: 0 bytes in 0 blocks
==42406==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42406== 
==42406== All heap blocks were freed -- no leaks are possible
==42406== 
==42406== For counts of detected and suppressed errors, rerun with: -v
==42406== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
