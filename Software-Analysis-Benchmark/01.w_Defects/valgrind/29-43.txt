==38527== Memcheck, a memory error detector
==38527== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38527== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38527== Command: ./01_w_Defects 29043
==38527== 
==38527== 
==38527== HEAP SUMMARY:
==38527==     in use at exit: 0 bytes in 0 blocks
==38527==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38527== 
==38527== All heap blocks were freed -- no leaks are possible
==38527== 
==38527== For counts of detected and suppressed errors, rerun with: -v
==38527== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
