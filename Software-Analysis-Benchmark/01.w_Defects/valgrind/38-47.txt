==40364== Memcheck, a memory error detector
==40364== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40364== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40364== Command: ./01_w_Defects 38047
==40364== 
==40364== 
==40364== HEAP SUMMARY:
==40364==     in use at exit: 0 bytes in 0 blocks
==40364==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40364== 
==40364== All heap blocks were freed -- no leaks are possible
==40364== 
==40364== For counts of detected and suppressed errors, rerun with: -v
==40364== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
