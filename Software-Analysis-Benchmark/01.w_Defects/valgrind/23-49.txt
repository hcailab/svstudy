==37037== Memcheck, a memory error detector
==37037== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37037== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37037== Command: ./01_w_Defects 23049
==37037== 
==37037== 
==37037== HEAP SUMMARY:
==37037==     in use at exit: 0 bytes in 0 blocks
==37037==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37037== 
==37037== All heap blocks were freed -- no leaks are possible
==37037== 
==37037== For counts of detected and suppressed errors, rerun with: -v
==37037== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
