==33487== Memcheck, a memory error detector
==33487== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33487== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33487== Command: ./01_w_Defects 5047
==33487== 
==33487== 
==33487== HEAP SUMMARY:
==33487==     in use at exit: 0 bytes in 0 blocks
==33487==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33487== 
==33487== All heap blocks were freed -- no leaks are possible
==33487== 
==33487== For counts of detected and suppressed errors, rerun with: -v
==33487== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
