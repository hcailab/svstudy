==65154== Memcheck, a memory error detector
==65154== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==65154== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==65154== Command: ./01_w_Defects 32049
==65154== 
==65154== 
==65154== HEAP SUMMARY:
==65154==     in use at exit: 0 bytes in 0 blocks
==65154==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==65154== 
==65154== All heap blocks were freed -- no leaks are possible
==65154== 
==65154== For counts of detected and suppressed errors, rerun with: -v
==65154== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
