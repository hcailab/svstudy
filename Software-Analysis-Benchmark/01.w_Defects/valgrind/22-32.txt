==36744== Memcheck, a memory error detector
==36744== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36744== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36744== Command: ./01_w_Defects 22032
==36744== 
==36744== 
==36744== HEAP SUMMARY:
==36744==     in use at exit: 0 bytes in 0 blocks
==36744==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36744== 
==36744== All heap blocks were freed -- no leaks are possible
==36744== 
==36744== For counts of detected and suppressed errors, rerun with: -v
==36744== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
