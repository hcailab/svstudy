==69379== Memcheck, a memory error detector
==69379== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==69379== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==69379== Command: ./01_w_Defects 12009
==69379== 
==69379== Invalid free() / delete / delete[] / realloc()
==69379==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69379==    by 0x40D7D9: double_free_009 (double_free.c:168)
==69379==    by 0x40D9E3: double_free_main (double_free.c:275)
==69379==    by 0x401925: main (main.c:92)
==69379==  Address 0x57ea480 is 0 bytes inside a block of size 1 free'd
==69379==    at 0x4C30D3B: free (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69379==    by 0x40D7C3: double_free_009 (double_free.c:165)
==69379==    by 0x40D9E3: double_free_main (double_free.c:275)
==69379==    by 0x401925: main (main.c:92)
==69379==  Block was alloc'd at
==69379==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==69379==    by 0x40D7AA: double_free_009 (double_free.c:160)
==69379==    by 0x40D9E3: double_free_main (double_free.c:275)
==69379==    by 0x401925: main (main.c:92)
==69379== 
==69379== 
==69379== HEAP SUMMARY:
==69379==     in use at exit: 0 bytes in 0 blocks
==69379==   total heap usage: 2 allocs, 3 frees, 1,025 bytes allocated
==69379== 
==69379== All heap blocks were freed -- no leaks are possible
==69379== 
==69379== For counts of detected and suppressed errors, rerun with: -v
==69379== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
