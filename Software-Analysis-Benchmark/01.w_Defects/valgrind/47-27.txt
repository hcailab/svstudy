==42180== Memcheck, a memory error detector
==42180== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42180== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42180== Command: ./01_w_Defects 47027
==42180== 
==42180== 
==42180== HEAP SUMMARY:
==42180==     in use at exit: 0 bytes in 0 blocks
==42180==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42180== 
==42180== All heap blocks were freed -- no leaks are possible
==42180== 
==42180== For counts of detected and suppressed errors, rerun with: -v
==42180== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
