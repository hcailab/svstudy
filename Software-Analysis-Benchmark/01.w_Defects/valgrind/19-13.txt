==36238== Memcheck, a memory error detector
==36238== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36238== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36238== Command: ./01_w_Defects 19013
==36238== 
==36238== 
==36238== HEAP SUMMARY:
==36238==     in use at exit: 0 bytes in 0 blocks
==36238==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36238== 
==36238== All heap blocks were freed -- no leaks are possible
==36238== 
==36238== For counts of detected and suppressed errors, rerun with: -v
==36238== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
