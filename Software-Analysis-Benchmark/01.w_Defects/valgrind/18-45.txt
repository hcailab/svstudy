==36157== Memcheck, a memory error detector
==36157== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36157== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36157== Command: ./01_w_Defects 18045
==36157== 
==36157== 
==36157== HEAP SUMMARY:
==36157==     in use at exit: 0 bytes in 0 blocks
==36157==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36157== 
==36157== All heap blocks were freed -- no leaks are possible
==36157== 
==36157== For counts of detected and suppressed errors, rerun with: -v
==36157== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
