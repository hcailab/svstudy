==39364== Memcheck, a memory error detector
==39364== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39364== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39364== Command: ./01_w_Defects 33028
==39364== 
==39364== 
==39364== HEAP SUMMARY:
==39364==     in use at exit: 0 bytes in 0 blocks
==39364==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39364== 
==39364== All heap blocks were freed -- no leaks are possible
==39364== 
==39364== For counts of detected and suppressed errors, rerun with: -v
==39364== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
