/*
 * HeaderFile.h
 *
 */

#ifndef HEADERFILE_H_
#define HEADERFILE_H_

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<string.h>
#include<pthread.h>
#include<ctype.h>
#include<unistd.h>
int idx, sink;
double dsink;
void *psink;

#define PRINT_DEBUG 1
#define UINT_MAX 429496725
#endif /* HEADERFILE_H_ */



void bit_shift_main();
void dynamic_buffer_overrun_main ();
void dynamic_buffer_underrun_main();
void cmp_funcadr_main();
void conflicting_cond_main();
void data_lost_main();
void data_overflow_main();
void data_underflow_main();
void dead_code_main();
void dead_lock_main();
void deletion_of_data_structure_sentinel_main ();
void double_free_main ();
void double_lock_main();
void double_release_main();
void endless_loop_main();
void free_nondynamic_allocated_memory_main ();
void free_null_pointer_main();
void func_pointer_main();
void function_return_value_unchecked_main();
void improper_termination_of_block_main ();
void insign_code_main();
void invalid_extern_main();
void invalid_memory_access_main();
void littlemem_st_main();
void livelock_main();
void lock_never_unlock_main();
void memory_allocation_failure_main();
void memory_leak_main();
void not_return_main();
void null_pointer_main();
void overrun_st_main();
void ow_memcpy_main();
void pow_related_errors_main ();
void ptr_subtraction_main();
void race_condition_main ();
void redundant_cond_main();
void return_local_main();
void sign_conv_main();
void sleep_lock_main();
void st_cross_thread_access_main();
void st_overflow_main();
void st_underrun_main();
void underrun_st_main();
void uninit_memory_access_main();
void uninit_pointer_main();
void uninit_var_main();
void unlock_without_lock_main();
void unused_var_main();
void wrong_arguments_func_pointer_main();
void zero_division_main();




























