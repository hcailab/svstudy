=================================================================
==64394==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7ffc2f1cc3f4 at pc 0x000000511361 bp 0x7ffc2f1cc3b0 sp 0x7ffc2f1cc3a8
WRITE of size 4 at 0x7ffc2f1cc3f4 thread T0
    #0 0x511360 in overrun_st_017 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:223:33
    #1 0x518524 in overrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:867:3
    #2 0x4f931a in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:212:3
    #3 0x7fac7b181b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffc2f1cc3f4 is located in stack of thread T0 at offset 52 in frame
    #0 0x51123f in overrun_st_017 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:221

  This frame has 1 object(s):
    [32, 52) 'buf' (line 222) <== Memory access at offset 52 overflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:223:33 in overrun_st_017
Shadow bytes around the buggy address:
  0x100005e31820: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e31830: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e31840: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e31850: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e31860: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x100005e31870: 00 00 00 00 00 00 00 00 f1 f1 f1 f1 00 00[04]f3
  0x100005e31880: f3 f3 f3 f3 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e31890: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e318a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e318b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100005e318c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64394==ABORTING
