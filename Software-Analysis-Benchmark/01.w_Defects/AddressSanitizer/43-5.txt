=================================================================
==13166==ERROR: AddressSanitizer: stack-buffer-underflow on address 0x7ffcd4c9337f at pc 0x00000053c5d1 bp 0x7ffcd4c93350 sp 0x7ffcd4c93348
WRITE of size 1 at 0x7ffcd4c9337f thread T0
    #0 0x53c5d0 in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:160:15
    #1 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #2 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #3 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #4 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #5 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #6 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #7 0x53c57f in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:155:4
    #8 0x53c8ec in st_underrun_005 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:170:2
    #9 0x53d6b8 in st_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:285:3
    #10 0x4f9c83 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:278:3
    #11 0x7f32acdf6b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #12 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffcd4c9337f is located in stack of thread T0 at offset 31 in frame
    #0 0x53c0cf in st_underrun_005_func_001 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:149

  This frame has 3 object(s):
    [32, 42) 's' <== Memory access at offset 31 underflows this variable
    [64, 80) 'coerce'
    [96, 112) 's.coerce'
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-underflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/st_underrun.c:160:15 in st_underrun_005_func_001
Shadow bytes around the buggy address:
  0x10001a98a610: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a620: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a630: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a640: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a650: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x10001a98a660: 00 00 00 00 00 00 00 00 00 00 00 00 f1 f1 f1[f1]
  0x10001a98a670: 00 02 f2 f2 00 00 f2 f2 00 00 f3 f3 00 00 00 00
  0x10001a98a680: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a690: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10001a98a6a0: 00 00 00 00 f1 f1 f1 f1 00 02 f2 f2 00 00 f2 f2
  0x10001a98a6b0: 00 00 f3 f3 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==13166==ABORTING
