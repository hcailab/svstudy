=================================================================
==14307==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x603000000024 at pc 0x0000004fbb65 bp 0x7ffce58c6b80 sp 0x7ffce58c6b78
WRITE of size 4 at 0x603000000024 thread T0
    #0 0x4fbb64 in dynamic_buffer_overrun_022 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_overrun_dynamic.c:403:13
    #1 0x4fd765 in dynamic_buffer_overrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_overrun_dynamic.c:726:3
    #2 0x4f7a4a in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:32:3
    #3 0x7fdc84f6eb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

0x603000000024 is located 0 bytes to the right of 20-byte region [0x603000000010,0x603000000024)
allocated by thread T0 here:
    #0 0x4c68fa in calloc /tmp/final/llvm.src/projects/compiler-rt/lib/asan/asan_malloc_linux.cc:155:3
    #1 0x4fbb06 in dynamic_buffer_overrun_022 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_overrun_dynamic.c:398:18
    #2 0x4fd765 in dynamic_buffer_overrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_overrun_dynamic.c:726:3
    #3 0x4f7a4a in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:32:3
    #4 0x7fdc84f6eb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_overrun_dynamic.c:403:13 in dynamic_buffer_overrun_022
Shadow bytes around the buggy address:
  0x0c067fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c067fff8000: fa fa 00 00[04]fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8010: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8020: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==14307==ABORTING
