=================================================================
==14484==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x60200000000f at pc 0x000000503b91 bp 0x7ffd6e9ff6c0 sp 0x7ffd6e9ff6b8
WRITE of size 1 at 0x60200000000f thread T0
    #0 0x503b90 in dynamic_buffer_underrun_030 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:558:20
    #1 0x5062bd in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:939:3
    #2 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #3 0x7f8c3d2ceb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

0x60200000000f is located 1 bytes to the left of 10-byte region [0x602000000010,0x60200000001a)
allocated by thread T0 here:
    #0 0x4c68fa in calloc /tmp/final/llvm.src/projects/compiler-rt/lib/asan/asan_malloc_linux.cc:155:3
    #1 0x503aa5 in dynamic_buffer_underrun_030 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:550:16
    #2 0x5062bd in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:939:3
    #3 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #4 0x7f8c3d2ceb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:558:20 in dynamic_buffer_underrun_030
Shadow bytes around the buggy address:
  0x0c047fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c047fff8000: fa[fa]00 02 fa fa 00 02 fa fa 00 02 fa fa 00 02
  0x0c047fff8010: fa fa 00 02 fa fa 00 02 fa fa 00 02 fa fa 00 02
  0x0c047fff8020: fa fa 00 02 fa fa 00 02 fa fa fa fa fa fa fa fa
  0x0c047fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==14484==ABORTING
