=================================================================
==64462==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7ffe6ac62614 at pc 0x000000514a98 bp 0x7ffe6ac625d0 sp 0x7ffe6ac625c8
WRITE of size 4 at 0x7ffe6ac62614 thread T0
    #0 0x514a97 in overrun_st_038 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:523:24
    #1 0x5195f5 in overrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:972:3
    #2 0x4f931a in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:212:3
    #3 0x7ff7fb562b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffe6ac62614 is located in stack of thread T0 at offset 52 in frame
    #0 0x5148bf in overrun_st_038 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:517

  This frame has 2 object(s):
    [32, 52) 'buf' (line 518) <== Memory access at offset 52 overflows this variable
    [96, 112) 'indexes' (line 520)
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:523:24 in overrun_st_038
Shadow bytes around the buggy address:
  0x10004d584470: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d584480: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d584490: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d5844a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d5844b0: 00 00 00 00 00 00 00 00 00 00 00 00 f1 f1 f1 f1
=>0x10004d5844c0: 00 00[04]f2 f2 f2 f2 f2 00 00 f3 f3 00 00 00 00
  0x10004d5844d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d5844e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d5844f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d584500: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10004d584510: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64462==ABORTING
