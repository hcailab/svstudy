=================================================================
==64474==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7ffc36d14758 at pc 0x000000515658 bp 0x7ffc36d146b0 sp 0x7ffc36d146a8
WRITE of size 4 at 0x7ffc36d14758 thread T0
    #0 0x515657 in overrun_st_042 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:589:14
    #1 0x519929 in overrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:992:3
    #2 0x4f931a in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:212:3
    #3 0x7f50d3859b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffc36d14758 is located in stack of thread T0 at offset 152 in frame
    #0 0x5154cf in overrun_st_042 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:581

  This frame has 1 object(s):
    [32, 152) 'buf' (line 582) <== Memory access at offset 152 overflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:589:14 in overrun_st_042
Shadow bytes around the buggy address:
  0x100006d9a890: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a8a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a8b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a8c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a8d0: 00 00 00 00 00 00 00 00 f1 f1 f1 f1 00 00 00 00
=>0x100006d9a8e0: 00 00 00 00 00 00 00 00 00 00 00[f3]f3 f3 f3 f3
  0x100006d9a8f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a900: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a910: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a920: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x100006d9a930: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64474==ABORTING
