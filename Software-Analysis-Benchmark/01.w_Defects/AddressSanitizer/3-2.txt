=================================================================
==14394==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x602000000006 at pc 0x000000501e61 bp 0x7fffb747ceb0 sp 0x7fffb747cea8
WRITE of size 2 at 0x602000000006 thread T0
    #0 0x501e60 in dynamic_buffer_underrun_002 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:44:14
    #1 0x504c51 in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:799:3
    #2 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #3 0x7fbc82efab96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

0x602000000006 is located 10 bytes to the left of 10-byte region [0x602000000010,0x60200000001a)
allocated by thread T0 here:
    #0 0x4c68fa in calloc /tmp/final/llvm.src/projects/compiler-rt/lib/asan/asan_malloc_linux.cc:155:3
    #1 0x501e0a in dynamic_buffer_underrun_002 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:41:22
    #2 0x504c51 in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:799:3
    #3 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #4 0x7fbc82efab96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:44:14 in dynamic_buffer_underrun_002
Shadow bytes around the buggy address:
  0x0c047fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c047fff8000:[fa]fa 00 02 fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8010: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8020: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==14394==ABORTING
