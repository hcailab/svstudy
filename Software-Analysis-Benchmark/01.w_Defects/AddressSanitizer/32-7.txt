=================================================================
==64364==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7fff39a105e8 at pc 0x00000050f8ec bp 0x7fff39a10590 sp 0x7fff39a10588
WRITE of size 8 at 0x7fff39a105e8 thread T0
    #0 0x50f8eb in overrun_st_007 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:89:9
    #1 0x517d22 in overrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:817:3
    #2 0x4f931a in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:212:3
    #3 0x7f475c044b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7fff39a105e8 is located in stack of thread T0 at offset 72 in frame
    #0 0x50f7ef in overrun_st_007 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:87

  This frame has 1 object(s):
    [32, 72) 'buf' (line 88) <== Memory access at offset 72 overflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:89:9 in overrun_st_007
Shadow bytes around the buggy address:
  0x10006733a060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a080: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a090: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a0a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x10006733a0b0: 00 00 00 00 f1 f1 f1 f1 00 00 00 00 00[f3]f3 f3
  0x10006733a0c0: f3 f3 f3 f3 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a0d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a0e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a0f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10006733a100: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64364==ABORTING
