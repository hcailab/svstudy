=================================================================
==14412==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x604000000048 at pc 0x000000502320 bp 0x7ffc37770030 sp 0x7ffc37770028
READ of size 8 at 0x604000000048 thread T0
    #0 0x50231f in dynamic_buffer_underrun_008 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:154:6
    #1 0x50511f in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:829:3
    #2 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #3 0x7fc69ed26b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

0x604000000048 is located 8 bytes to the left of 40-byte region [0x604000000050,0x604000000078)
allocated by thread T0 here:
    #0 0x4c68fa in calloc /tmp/final/llvm.src/projects/compiler-rt/lib/asan/asan_malloc_linux.cc:155:3
    #1 0x502256 in dynamic_buffer_underrun_008 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:144:22
    #2 0x50511f in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:829:3
    #3 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #4 0x7fc69ed26b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:154:6 in dynamic_buffer_underrun_008
Shadow bytes around the buggy address:
  0x0c087fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c087fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c087fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c087fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c087fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c087fff8000: fa fa fd fd fd fd fd fd fa[fa]00 00 00 00 00 fa
  0x0c087fff8010: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c087fff8020: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c087fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c087fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c087fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==14412==ABORTING
