=================================================================
==64403==ERROR: AddressSanitizer: stack-buffer-overflow on address 0x7ffedd4d5014 at pc 0x000000511a27 bp 0x7ffedd4d4fd0 sp 0x7ffedd4d4fc8
WRITE of size 4 at 0x7ffedd4d5014 thread T0
    #0 0x511a26 in overrun_st_020 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:265:14
    #1 0x51878b in overrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:882:3
    #2 0x4f931a in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:212:3
    #3 0x7ff681ff9b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffedd4d5014 is located in stack of thread T0 at offset 52 in frame
    #0 0x5118ef in overrun_st_020 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:260

  This frame has 1 object(s):
    [32, 52) 'buf' (line 261) <== Memory access at offset 52 overflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-overflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/overrun_st.c:265:14 in overrun_st_020
Shadow bytes around the buggy address:
  0x10005ba929b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba929c0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba929d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba929e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba929f0: 00 00 00 00 00 00 00 00 00 00 00 00 f1 f1 f1 f1
=>0x10005ba92a00: 00 00[04]f3 f3 f3 f3 f3 00 00 00 00 00 00 00 00
  0x10005ba92a10: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba92a20: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba92a30: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba92a40: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10005ba92a50: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64403==ABORTING
