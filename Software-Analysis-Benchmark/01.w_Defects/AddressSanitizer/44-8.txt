=================================================================
==64298==ERROR: AddressSanitizer: stack-buffer-underflow on address 0x7ffd71dd35fc at pc 0x000000540b54 bp 0x7ffd71dd35d0 sp 0x7ffd71dd35c8
WRITE of size 4 at 0x7ffd71dd35fc thread T0
    #0 0x540b53 in underrun_st_008 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/underrun_st.c:109:6
    #1 0x54156f in underrun_st_main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/underrun_st.c:239:3
    #2 0x4f9d5e in main /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/main.c:284:3
    #3 0x7f4343decb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

Address 0x7ffd71dd35fc is located in stack of thread T0 at offset 28 in frame
    #0 0x540a2f in underrun_st_008 /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/underrun_st.c:102

  This frame has 1 object(s):
    [32, 52) 'buf' (line 103) <== Memory access at offset 28 underflows this variable
HINT: this may be a false positive if your program uses some custom stack unwind mechanism, swapcontext or vfork
      (longjmp and C++ exceptions *are* supported)
SUMMARY: AddressSanitizer: stack-buffer-underflow /home/ynong/Download/SV2/Software-Analysis-Benchmark/01.w_Defects/underrun_st.c:109:6 in underrun_st_008
Shadow bytes around the buggy address:
  0x10002e3b2660: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b2670: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b2680: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b2690: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b26a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x10002e3b26b0: 00 00 00 00 00 00 00 00 00 00 00 00 f1 f1 f1[f1]
  0x10002e3b26c0: 00 00 04 f3 f3 f3 f3 f3 00 00 00 00 00 00 00 00
  0x10002e3b26d0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b26e0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b26f0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x10002e3b2700: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==64298==ABORTING
