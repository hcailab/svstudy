=================================================================
==14487==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x60200000000f at pc 0x000000503dbc bp 0x7ffc39c9d550 sp 0x7ffc39c9d548
WRITE of size 1 at 0x60200000000f thread T0
    #0 0x503dbb in dynamic_buffer_underrun_031 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:579:17
    #1 0x50638a in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:944:3
    #2 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #3 0x7fe466db3b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x41ad39 in _start (/home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/01_w_Defects+0x41ad39)

0x60200000000f is located 1 bytes to the left of 12-byte region [0x602000000010,0x60200000001c)
allocated by thread T0 here:
    #0 0x4c68fa in calloc /tmp/final/llvm.src/projects/compiler-rt/lib/asan/asan_malloc_linux.cc:155:3
    #1 0x503ce8 in dynamic_buffer_underrun_031 /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:572:21
    #2 0x50638a in dynamic_buffer_underrun_main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:944:3
    #3 0x4f7b19 in main /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/main.c:38:3
    #4 0x7fe466db3b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/ynong/Download/Software-Analysis-Benchmark/01.w_Defects/buffer_underrun_dynamic.c:579:17 in dynamic_buffer_underrun_031
Shadow bytes around the buggy address:
  0x0c047fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c047fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c047fff8000: fa[fa]00 04 fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8010: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8020: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8030: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c047fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==14487==ABORTING
