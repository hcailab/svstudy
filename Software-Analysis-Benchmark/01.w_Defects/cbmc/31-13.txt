CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing null_pointer.c
Converting
Type-checking null_pointer
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 54 steps
simple slicing removed 4 assignments
Generated 7 VCC(s), 2 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
201 variables, 268 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
201 variables, 139 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00263246s

** Results:
null_pointer.c function null_pointer_001
[null_pointer_001.pointer_dereference.1] line 23 dereference failure: pointer NULL in *p: SUCCESS

null_pointer.c function null_pointer_002
[null_pointer_002.pointer_dereference.1] line 34 dereference failure: pointer NULL in *p: SUCCESS

null_pointer.c function null_pointer_003
[null_pointer_003.pointer_dereference.1] line 47 dereference failure: dead object in *pp: SUCCESS
[null_pointer_003.pointer_dereference.2] line 47 dereference failure: pointer outside object bounds in *pp: SUCCESS
[null_pointer_003.pointer_dereference.3] line 47 dereference failure: pointer NULL in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.4] line 47 dereference failure: pointer invalid in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.5] line 47 dereference failure: deallocated dynamic object in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.6] line 47 dereference failure: dead object in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.7] line 47 dereference failure: pointer outside dynamic object bounds in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.8] line 47 dereference failure: pointer outside object bounds in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.9] line 47 dereference failure: invalid integer address in *(*pp): SUCCESS

null_pointer.c function null_pointer_004
[null_pointer_004.pointer_dereference.1] line 63 dereference failure: pointer NULL in *((signed int *)(char *)((char *)p + (signed long int)0ul)): SUCCESS

null_pointer.c function null_pointer_005
[null_pointer_005.pointer_dereference.1] line 94 dereference failure: pointer NULL in *p: SUCCESS

null_pointer.c function null_pointer_006
[null_pointer_006.pointer_dereference.6] line 105 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_006.pointer_dereference.7] line 105 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_006.pointer_dereference.5] line 105 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_006.pointer_dereference.3] line 105 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_006.pointer_dereference.1] line 105 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_006.pointer_dereference.2] line 105 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_006.pointer_dereference.4] line 105 dereference failure: dead object in *p: SUCCESS

null_pointer.c function null_pointer_007
[null_pointer_007.pointer_dereference.7] line 117 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_007.pointer_dereference.6] line 117 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_007.pointer_dereference.5] line 117 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_007.pointer_dereference.4] line 117 dereference failure: dead object in *p: SUCCESS
[null_pointer_007.pointer_dereference.3] line 117 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_007.pointer_dereference.2] line 117 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_007.pointer_dereference.1] line 117 dereference failure: pointer NULL in *p: SUCCESS

null_pointer.c function null_pointer_008
[null_pointer_008.pointer_dereference.1] line 133 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_008.pointer_dereference.2] line 133 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_008.pointer_dereference.3] line 133 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_008.pointer_dereference.4] line 133 dereference failure: dead object in *p: SUCCESS
[null_pointer_008.pointer_dereference.5] line 133 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_008.pointer_dereference.6] line 133 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_008.pointer_dereference.7] line 133 dereference failure: invalid integer address in *p: SUCCESS

null_pointer.c function null_pointer_009_func_001
[null_pointer_009_func_001.pointer_dereference.5] line 142 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.7] line 142 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.6] line 142 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.4] line 142 dereference failure: dead object in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.1] line 142 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.2] line 142 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.3] line 142 dereference failure: deallocated dynamic object in *p: SUCCESS

null_pointer.c function null_pointer_010
[null_pointer_010.pointer_dereference.1] line 159 dereference failure: pointer NULL in *p1: SUCCESS

null_pointer.c function null_pointer_011
[null_pointer_011.pointer_dereference.1] line 173 dereference failure: pointer NULL in *p2: SUCCESS

null_pointer.c function null_pointer_012
[null_pointer_012.pointer_dereference.1] line 180 dereference failure: pointer NULL in p[(signed long int)3]: SUCCESS

null_pointer.c function null_pointer_013
[null_pointer_013.pointer_dereference.1] line 194 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_013.pointer_dereference.2] line 194 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_013.pointer_dereference.3] line 194 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_013.pointer_dereference.4] line 194 dereference failure: dead object in *p: SUCCESS
[null_pointer_013.pointer_dereference.5] line 194 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_013.pointer_dereference.6] line 194 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_013.pointer_dereference.7] line 194 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_013.pointer_dereference.13] line 196 dereference failure: pointer outside object bounds in *p: FAILURE
[null_pointer_013.pointer_dereference.14] line 196 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_013.pointer_dereference.12] line 196 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_013.pointer_dereference.10] line 196 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_013.pointer_dereference.9] line 196 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_013.pointer_dereference.8] line 196 dereference failure: pointer NULL in *p: FAILURE
[null_pointer_013.pointer_dereference.11] line 196 dereference failure: dead object in *p: SUCCESS

null_pointer.c function null_pointer_014
[null_pointer_014.pointer_dereference.6] line 213 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.7] line 213 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_014.pointer_dereference.5] line 213 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.4] line 213 dereference failure: dead object in *p: SUCCESS
[null_pointer_014.pointer_dereference.3] line 213 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_014.pointer_dereference.2] line 213 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_014.pointer_dereference.1] line 213 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_014.pointer_dereference.8] line 216 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_014.pointer_dereference.9] line 216 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_014.pointer_dereference.10] line 216 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_014.pointer_dereference.11] line 216 dereference failure: dead object in *p: SUCCESS
[null_pointer_014.pointer_dereference.12] line 216 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.13] line 216 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.14] line 216 dereference failure: invalid integer address in *p: SUCCESS

null_pointer.c function null_pointer_016_func_002
[null_pointer_016_func_002.pointer_dereference.5] line 264 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.6] line 264 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.7] line 264 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.3] line 264 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.2] line 264 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.1] line 264 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.4] line 264 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.17] line 270 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.21] line 270 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.20] line 270 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.19] line 270 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.18] line 270 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.16] line 270 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.15] line 270 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.14] line 270 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.13] line 270 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.12] line 270 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.11] line 270 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.10] line 270 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.9] line 270 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.8] line 270 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS

null_pointer.c function null_pointer_016
[null_pointer_016.pointer_dereference.1] line 288 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.14] line 288 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.13] line 288 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.12] line 288 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.11] line 288 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.10] line 288 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.9] line 288 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.8] line 288 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.7] line 288 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.6] line 288 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.5] line 288 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.4] line 288 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.3] line 288 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.2] line 288 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.21] line 290 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.20] line 290 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.19] line 290 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.17] line 290 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.16] line 290 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.15] line 290 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.18] line 290 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.22] line 291 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.23] line 291 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.24] line 291 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.25] line 291 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.26] line 291 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.27] line 291 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.28] line 291 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS

null_pointer.c function null_pointer_017_func_001
[null_pointer_017_func_001.pointer_dereference.1] line 321 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.7] line 321 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.6] line 321 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.5] line 321 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.4] line 321 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.3] line 321 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.2] line 321 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS

null_pointer.c function null_pointer_017
[null_pointer_017.pointer_dereference.7] line 334 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.2] line 334 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.6] line 334 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.5] line 334 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.3] line 334 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.1] line 334 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.4] line 334 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.13] line 340 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.14] line 340 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.12] line 340 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.11] line 340 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.10] line 340 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.9] line 340 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.8] line 340 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.15] line 341 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.16] line 341 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.17] line 341 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.18] line 341 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.19] line 341 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.20] line 341 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.21] line 341 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS

** 2 of 149 failed (2 iterations)
VERIFICATION FAILED
