CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing littlemem_st.c
Converting
Type-checking littlemem_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop littlemem_st_008_func_002.0 iteration 1 file littlemem_st.c line 220 function littlemem_st_008_func_002 thread 0
Unwinding loop littlemem_st_008_func_002.0 iteration 2 file littlemem_st.c line 220 function littlemem_st_008_func_002 thread 0
size of program expression: 93 steps
simple slicing removed 8 assignments
Generated 14 VCC(s), 2 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
266 variables, 273 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
266 variables, 0 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00156893s

** Results:
littlemem_st.c function littlemem_st_001
[littlemem_st_001.array_bounds.2] line 32 array `buf' upper bound in buf[(signed long int)i]: SUCCESS
[littlemem_st_001.array_bounds.1] line 32 array `buf' lower bound in buf[(signed long int)i]: SUCCESS
[littlemem_st_001.pointer_dereference.2] line 36 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[littlemem_st_001.pointer_dereference.1] line 36 dereference failure: dead object in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_002
[littlemem_st_002.pointer_dereference.1] line 55 dereference failure: dead object in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[littlemem_st_002.pointer_dereference.2] line 55 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_003
[littlemem_st_003.pointer_dereference.1] line 73 dereference failure: dead object in *p: SUCCESS
[littlemem_st_003.pointer_dereference.2] line 73 dereference failure: pointer outside object bounds in *p: SUCCESS

littlemem_st.c function littlemem_st_004
[littlemem_st_004.pointer_dereference.3] line 92 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.7] line 92 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.6] line 92 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.5] line 92 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.4] line 92 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.2] line 92 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_004.pointer_dereference.1] line 92 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_004_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_005
[littlemem_st_005.pointer_dereference.3] line 117 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.7] line 117 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.6] line 117 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.5] line 117 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.4] line 117 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.2] line 117 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_005.pointer_dereference.1] line 117 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_005_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_006_func_002
[littlemem_st_006_func_002.pointer_dereference.1] line 144 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.2] line 144 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.3] line 144 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.4] line 144 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.5] line 144 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.6] line 144 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_006_func_002.pointer_dereference.7] line 144 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_006_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_007_func_002
[littlemem_st_007_func_002.pointer_dereference.7] line 185 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.6] line 185 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.5] line 185 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.4] line 185 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.2] line 185 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.1] line 185 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_007_func_002.pointer_dereference.3] line 185 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_008_func_002
[littlemem_st_008_func_002.pointer_dereference.1] line 228 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_008_func_002.pointer_dereference.2] line 228 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_008_func_002.pointer_dereference.3] line 228 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_008_func_002.pointer_dereference.4] line 228 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_008_func_002.pointer_dereference.5] line 228 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_008_func_002.pointer_dereference.6] line 228 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): FAILURE
[littlemem_st_008_func_002.pointer_dereference.7] line 228 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_009_func_002
[littlemem_st_009_func_002.pointer_dereference.6] line 272 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.7] line 272 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.5] line 272 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.3] line 272 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.2] line 272 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.1] line 272 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_009_func_002.pointer_dereference.4] line 272 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_010_func_002
[littlemem_st_010_func_002.pointer_dereference.6] line 307 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.7] line 307 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.5] line 307 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.4] line 307 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.3] line 307 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.2] line 307 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_010_func_002.pointer_dereference.1] line 307 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS

littlemem_st.c function littlemem_st_011_func_002
[littlemem_st_011_func_002.pointer_dereference.1] line 343 dereference failure: pointer NULL in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.2] line 343 dereference failure: pointer invalid in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.3] line 343 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.4] line 343 dereference failure: dead object in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.5] line 343 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.6] line 343 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS
[littlemem_st_011_func_002.pointer_dereference.7] line 343 dereference failure: invalid integer address in *((signed int *)(char *)((char *)littlemem_st_007_s_001_gbl_str + (signed long int)8ul)): SUCCESS

** 1 of 64 failed (2 iterations)
VERIFICATION FAILED
