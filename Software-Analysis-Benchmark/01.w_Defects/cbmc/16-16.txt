CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing free_nondynamic_allocated_memory.c
Converting
Type-checking free_nondynamic_allocated_memory
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 73 steps
simple slicing removed 19 assignments
Generated 4 VCC(s), 1 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
8 variables, 11 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
8 variables, 0 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.000607279s

** Results:
free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_001
[free_nondynamic_allocated_memory_001.precondition_instance.2] line 22 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_001.precondition_instance.1] line 22 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_001.precondition_instance.4] line 22 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_001.precondition_instance.3] line 22 double free: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_002
[free_nondynamic_allocated_memory_002.precondition_instance.1] line 36 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_002.precondition_instance.2] line 36 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_002.precondition_instance.3] line 36 double free: SUCCESS
[free_nondynamic_allocated_memory_002.precondition_instance.4] line 36 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_003
[free_nondynamic_allocated_memory_003.precondition_instance.4] line 48 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_003.precondition_instance.3] line 48 double free: SUCCESS
[free_nondynamic_allocated_memory_003.precondition_instance.2] line 48 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_003.precondition_instance.1] line 48 free argument must be dynamic object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_004
[free_nondynamic_allocated_memory_004.precondition_instance.1] line 62 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_004.precondition_instance.2] line 62 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_004.precondition_instance.3] line 62 double free: SUCCESS
[free_nondynamic_allocated_memory_004.precondition_instance.4] line 62 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_005
[free_nondynamic_allocated_memory_005.pointer_dereference.2] line 78 dereference failure: pointer invalid in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.7] line 78 dereference failure: invalid integer address in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.6] line 78 dereference failure: pointer outside object bounds in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.5] line 78 dereference failure: pointer outside dynamic object bounds in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.4] line 78 dereference failure: dead object in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.3] line 78 dereference failure: deallocated dynamic object in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.1] line 78 dereference failure: pointer NULL in buf2[(signed long int)0]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.15] line 82 dereference failure: pointer NULL in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.21] line 82 dereference failure: invalid integer address in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.20] line 82 dereference failure: pointer outside object bounds in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.19] line 82 dereference failure: pointer outside dynamic object bounds in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.18] line 82 dereference failure: dead object in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.17] line 82 dereference failure: deallocated dynamic object in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.16] line 82 dereference failure: pointer invalid in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.14] line 82 dereference failure: invalid integer address in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.12] line 82 dereference failure: pointer outside dynamic object bounds in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.11] line 82 dereference failure: dead object in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.10] line 82 dereference failure: deallocated dynamic object in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.9] line 82 dereference failure: pointer invalid in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.8] line 82 dereference failure: pointer NULL in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.array_bounds.2] line 82 array `pbuf' upper bound in pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.array_bounds.1] line 82 array `pbuf' lower bound in pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.pointer_dereference.13] line 82 dereference failure: pointer outside object bounds in *pbuf[(signed long int)i]: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.1] line 84 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.2] line 84 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.3] line 84 double free: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.4] line 84 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.8] line 85 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.7] line 85 double free: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.6] line 85 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.5] line 85 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.9] line 86 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.10] line 86 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.11] line 86 double free: SUCCESS
[free_nondynamic_allocated_memory_005.precondition_instance.12] line 86 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_006
[free_nondynamic_allocated_memory_006.precondition_instance.1] line 103 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_006.precondition_instance.2] line 103 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_006.precondition_instance.3] line 103 double free: SUCCESS
[free_nondynamic_allocated_memory_006.precondition_instance.4] line 103 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_007
[free_nondynamic_allocated_memory_007.precondition_instance.1] line 115 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_007.precondition_instance.2] line 115 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_007.precondition_instance.3] line 115 double free: SUCCESS
[free_nondynamic_allocated_memory_007.precondition_instance.4] line 115 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_008
[free_nondynamic_allocated_memory_008.precondition_instance.4] line 128 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_008.precondition_instance.2] line 128 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_008.precondition_instance.1] line 128 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_008.precondition_instance.3] line 128 double free: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_009
[free_nondynamic_allocated_memory_009.precondition_instance.1] line 141 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_009.precondition_instance.2] line 141 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_009.precondition_instance.3] line 141 double free: SUCCESS
[free_nondynamic_allocated_memory_009.precondition_instance.4] line 141 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_010
[free_nondynamic_allocated_memory_010.precondition_instance.1] line 155 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_010.precondition_instance.2] line 155 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_010.precondition_instance.3] line 155 double free: SUCCESS
[free_nondynamic_allocated_memory_010.precondition_instance.4] line 155 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_011
[free_nondynamic_allocated_memory_011.precondition_instance.1] line 170 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_011.precondition_instance.2] line 170 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_011.precondition_instance.3] line 170 double free: SUCCESS
[free_nondynamic_allocated_memory_011.precondition_instance.4] line 170 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_012
[free_nondynamic_allocated_memory_012.precondition_instance.4] line 187 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_012.precondition_instance.3] line 187 double free: SUCCESS
[free_nondynamic_allocated_memory_012.precondition_instance.2] line 187 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_012.precondition_instance.1] line 187 free argument must be dynamic object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_013
[free_nondynamic_allocated_memory_013.pointer_dereference.1] line 208 dereference failure: pointer NULL in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.2] line 208 dereference failure: pointer invalid in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.3] line 208 dereference failure: deallocated dynamic object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.4] line 208 dereference failure: dead object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.5] line 208 dereference failure: pointer outside dynamic object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.6] line 208 dereference failure: pointer outside object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.7] line 208 dereference failure: invalid integer address in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.4] line 209 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.18] line 209 dereference failure: dead object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.19] line 209 dereference failure: pointer outside dynamic object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.20] line 209 dereference failure: pointer outside object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.21] line 209 dereference failure: invalid integer address in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.1] line 209 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.2] line 209 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.3] line 209 double free: SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.12] line 209 dereference failure: pointer outside dynamic object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.16] line 209 dereference failure: pointer invalid in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.15] line 209 dereference failure: pointer NULL in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.14] line 209 dereference failure: invalid integer address in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.13] line 209 dereference failure: pointer outside object bounds in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.11] line 209 dereference failure: dead object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.10] line 209 dereference failure: deallocated dynamic object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.9] line 209 dereference failure: pointer invalid in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.8] line 209 dereference failure: pointer NULL in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.pointer_dereference.17] line 209 dereference failure: deallocated dynamic object in *((struct node **)(char *)((char *)new_struct + (signed long int)8ul)): SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.8] line 210 free called for new[] object: SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.7] line 210 double free: SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.6] line 210 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_013.precondition_instance.5] line 210 free argument must be dynamic object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_014
[free_nondynamic_allocated_memory_014.precondition_instance.1] line 229 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_014.precondition_instance.2] line 229 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_014.precondition_instance.3] line 229 double free: SUCCESS
[free_nondynamic_allocated_memory_014.precondition_instance.4] line 229 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_015_func_001
[free_nondynamic_allocated_memory_015_func_001.precondition_instance.1] line 239 free argument must be dynamic object: SUCCESS
[free_nondynamic_allocated_memory_015_func_001.precondition_instance.2] line 239 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_015_func_001.precondition_instance.3] line 239 double free: SUCCESS
[free_nondynamic_allocated_memory_015_func_001.precondition_instance.4] line 239 free called for new[] object: SUCCESS

free_nondynamic_allocated_memory.c function free_nondynamic_allocated_memory_016_func_002
[free_nondynamic_allocated_memory_016_func_002.precondition_instance.1] line 262 free argument must be dynamic object: FAILURE
[free_nondynamic_allocated_memory_016_func_002.precondition_instance.2] line 262 free argument has offset zero: SUCCESS
[free_nondynamic_allocated_memory_016_func_002.precondition_instance.3] line 262 double free: SUCCESS
[free_nondynamic_allocated_memory_016_func_002.precondition_instance.4] line 262 free called for new[] object: SUCCESS

** 1 of 120 failed (2 iterations)
VERIFICATION FAILED
