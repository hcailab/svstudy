CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing function_return_value_unchecked.c
Converting
Type-checking function_return_value_unchecked
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 72 steps
simple slicing removed 10 assignments
Generated 14 VCC(s), 7 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
1120 variables, 2415 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
1120 variables, 1537 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
1120 variables, 1037 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.0104406s

** Results:
<builtin-library-vfprintf> function vfprintf
[vfprintf.pointer_dereference.2] line 21 dereference failure: pointer invalid in *stream: FAILURE
[vfprintf.pointer_dereference.3] line 21 dereference failure: deallocated dynamic object in *stream: FAILURE
[vfprintf.pointer_dereference.4] line 21 dereference failure: dead object in *stream: FAILURE
[vfprintf.pointer_dereference.5] line 21 dereference failure: pointer outside dynamic object bounds in *stream: FAILURE
[vfprintf.pointer_dereference.6] line 21 dereference failure: pointer outside object bounds in *stream: FAILURE
[vfprintf.pointer_dereference.7] line 21 dereference failure: invalid integer address in *stream: FAILURE
[vfprintf.pointer_dereference.1] line 21 dereference failure: pointer NULL in *stream: FAILURE
[vfprintf.pointer_dereference.13] line 25 dereference failure: pointer outside object bounds in *format: SUCCESS
[vfprintf.pointer_dereference.14] line 25 dereference failure: invalid integer address in *format: SUCCESS
[vfprintf.pointer_dereference.12] line 25 dereference failure: pointer outside dynamic object bounds in *format: SUCCESS
[vfprintf.pointer_dereference.11] line 25 dereference failure: dead object in *format: SUCCESS
[vfprintf.pointer_dereference.10] line 25 dereference failure: deallocated dynamic object in *format: SUCCESS
[vfprintf.pointer_dereference.9] line 25 dereference failure: pointer invalid in *format: SUCCESS
[vfprintf.pointer_dereference.8] line 25 dereference failure: pointer NULL in *format: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_006_func_001
[function_return_value_unchecked_006_func_001.pointer_dereference.1] line 198 dereference failure: pointer NULL in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.2] line 198 dereference failure: deallocated dynamic object in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.3] line 198 dereference failure: dead object in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.4] line 198 dereference failure: pointer outside dynamic object bounds in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.5] line 198 dereference failure: pointer outside object bounds in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.6] line 198 dereference failure: invalid integer address in p[(signed long int)i][(signed long int)j]: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_006
[function_return_value_unchecked_006.pointer_dereference.1] line 214 dereference failure: pointer NULL in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.array_bounds.1] line 214 array dynamic object upper bound in ptr[(signed long int)j][0l]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.7] line 214 dereference failure: invalid integer address in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.6] line 214 dereference failure: pointer outside object bounds in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.4] line 214 dereference failure: dead object in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.3] line 214 dereference failure: deallocated dynamic object in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.2] line 214 dereference failure: pointer invalid in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.5] line 214 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.12] line 216 dereference failure: pointer outside object bounds in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.13] line 216 dereference failure: invalid integer address in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.11] line 216 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.10] line 216 dereference failure: dead object in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.9] line 216 dereference failure: deallocated dynamic object in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.8] line 216 dereference failure: pointer NULL in ptr[(signed long int)j][(signed long int)1]: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_009_func_001
[function_return_value_unchecked_009_func_001.pointer_dereference.1] line 269 dereference failure: pointer NULL in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.2] line 269 dereference failure: pointer invalid in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.3] line 269 dereference failure: deallocated dynamic object in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.4] line 269 dereference failure: dead object in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.5] line 269 dereference failure: pointer outside dynamic object bounds in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.6] line 269 dereference failure: pointer outside object bounds in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.7] line 269 dereference failure: invalid integer address in a[(signed long int)i]: SUCCESS

** 7 of 41 failed (3 iterations)
VERIFICATION FAILED
