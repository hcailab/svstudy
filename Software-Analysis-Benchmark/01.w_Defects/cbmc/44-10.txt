CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing underrun_st.c
Converting
Type-checking underrun_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop underrun_st_010.0 iteration 1 file underrun_st.c line 138 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 2 file underrun_st.c line 138 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 3 file underrun_st.c line 138 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 4 file underrun_st.c line 138 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 5 file underrun_st.c line 138 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 6 file underrun_st.c line 138 function underrun_st_010 thread 0
size of program expression: 87 steps
simple slicing removed 8 assignments
Generated 6 VCC(s), 1 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
104 variables, 11 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
104 variables, 0 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00105794s

** Results:
underrun_st.c function underrun_st_001
[underrun_st_001.array_bounds.1] line 21 array `buf' lower bound in buf[(signed long int)-1]: SUCCESS

underrun_st.c function underrun_st_002
[underrun_st_002.array_bounds.1] line 31 array `buf' lower bound in buf[(signed long int)-1]: SUCCESS

underrun_st.c function underrun_st_003
[underrun_st_003.array_bounds.1] line 42 array `buf' lower bound in buf[(signed long int)index]: SUCCESS
[underrun_st_003.array_bounds.2] line 42 array `buf' upper bound in buf[(signed long int)index]: SUCCESS

underrun_st.c function underrun_st_004
[underrun_st_004.pointer_dereference.1] line 55 dereference failure: dead object in *(p - (signed long int)1): SUCCESS
[underrun_st_004.pointer_dereference.2] line 55 dereference failure: pointer outside object bounds in *(p - (signed long int)1): SUCCESS

underrun_st.c function underrun_st_005
[underrun_st_005.pointer_dereference.1] line 67 dereference failure: dead object in *(p - (signed long int)1): SUCCESS
[underrun_st_005.pointer_dereference.2] line 67 dereference failure: pointer outside object bounds in *(p - (signed long int)1): SUCCESS

underrun_st.c function underrun_st_006
[underrun_st_006.pointer_dereference.1] line 80 dereference failure: dead object in *(p - (signed long int)index): SUCCESS
[underrun_st_006.pointer_dereference.2] line 80 dereference failure: pointer outside object bounds in *(p - (signed long int)index): SUCCESS

underrun_st.c function underrun_st_007
[underrun_st_007.array_bounds.2] line 93 array `buf' upper bound in buf[(signed long int)i]: SUCCESS
[underrun_st_007.array_bounds.1] line 93 array `buf' lower bound in buf[(signed long int)i]: SUCCESS

underrun_st.c function underrun_st_008
[underrun_st_008.pointer_dereference.1] line 109 dereference failure: dead object in *p: SUCCESS
[underrun_st_008.pointer_dereference.2] line 109 dereference failure: pointer outside object bounds in *p: SUCCESS

underrun_st.c function underrun_st_009
[underrun_st_009.array_bounds.1] line 124 array `underrun_st_009_gbl_buf' lower bound in underrun_st_009_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_009.array_bounds.2] line 124 array `underrun_st_009_gbl_buf' upper bound in underrun_st_009_gbl_buf[(signed long int)i]: SUCCESS

underrun_st.c function underrun_st_010
[underrun_st_010.pointer_dereference.1] line 140 dereference failure: pointer outside object bounds in *p: FAILURE

underrun_st.c function underrun_st_011
[underrun_st_011.array_bounds.1] line 155 array `underrun_st_011_gbl_buf' lower bound in underrun_st_011_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_011.array_bounds.2] line 155 array `underrun_st_011_gbl_buf' upper bound in underrun_st_011_gbl_buf[(signed long int)i]: SUCCESS

underrun_st.c function underrun_st_012
[underrun_st_012.pointer_dereference.1] line 172 dereference failure: pointer outside object bounds in *p: SUCCESS

underrun_st.c function underrun_st_013
[underrun_st_013.array_bounds.1] line 190 array `underrun_st_013_gbl_buf' lower bound in underrun_st_013_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_013.array_bounds.2] line 190 array `underrun_st_013_gbl_buf' upper bound in underrun_st_013_gbl_buf[(signed long int)i]: SUCCESS

** 1 of 22 failed (2 iterations)
VERIFICATION FAILED
