CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing uninit_pointer.c
Converting
Type-checking uninit_pointer
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 107 steps
simple slicing removed 23 assignments
Generated 25 VCC(s), 3 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
374 variables, 101 clauses
SAT checker: instance is UNSATISFIABLE
Runtime decision procedure: 0.00106419s

** Results:
uninit_pointer.c function uninit_pointer_001
[uninit_pointer_001.pointer_dereference.1] line 29 dereference failure: pointer uninitialized in *p: SUCCESS

uninit_pointer.c function uninit_pointer_002
[uninit_pointer_002.pointer_dereference.1] line 40 dereference failure: pointer uninitialized in *p: SUCCESS

uninit_pointer.c function uninit_pointer_003
[uninit_pointer_003.pointer_dereference.1] line 54 dereference failure: dead object in *pp: SUCCESS
[uninit_pointer_003.pointer_dereference.2] line 54 dereference failure: pointer outside object bounds in *pp: SUCCESS
[uninit_pointer_003.pointer_dereference.3] line 54 dereference failure: pointer NULL in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.4] line 54 dereference failure: pointer invalid in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.5] line 54 dereference failure: deallocated dynamic object in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.6] line 54 dereference failure: dead object in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.7] line 54 dereference failure: pointer outside dynamic object bounds in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.8] line 54 dereference failure: pointer outside object bounds in *(*pp): SUCCESS
[uninit_pointer_003.pointer_dereference.9] line 54 dereference failure: invalid integer address in *(*pp): SUCCESS

uninit_pointer.c function uninit_pointer_005_func_001
[uninit_pointer_005_func_001.pointer_dereference.7] line 84 dereference failure: invalid integer address in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.6] line 84 dereference failure: pointer outside object bounds in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.5] line 84 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.3] line 84 dereference failure: deallocated dynamic object in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.2] line 84 dereference failure: pointer invalid in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.1] line 84 dereference failure: pointer NULL in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.4] line 84 dereference failure: dead object in pbuf[(signed long int)0]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.14] line 85 dereference failure: invalid integer address in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.13] line 85 dereference failure: pointer outside object bounds in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.12] line 85 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.11] line 85 dereference failure: dead object in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.10] line 85 dereference failure: deallocated dynamic object in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.9] line 85 dereference failure: pointer invalid in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.8] line 85 dereference failure: pointer NULL in pbuf[(signed long int)2]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.15] line 86 dereference failure: pointer NULL in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.16] line 86 dereference failure: pointer invalid in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.17] line 86 dereference failure: deallocated dynamic object in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.18] line 86 dereference failure: dead object in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.19] line 86 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.20] line 86 dereference failure: pointer outside object bounds in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.21] line 86 dereference failure: invalid integer address in pbuf[(signed long int)3]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.27] line 87 dereference failure: pointer outside object bounds in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.28] line 87 dereference failure: invalid integer address in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.26] line 87 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.24] line 87 dereference failure: deallocated dynamic object in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.22] line 87 dereference failure: pointer NULL in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.23] line 87 dereference failure: pointer invalid in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.25] line 87 dereference failure: dead object in pbuf[(signed long int)4]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.37] line 89 dereference failure: pointer invalid in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.42] line 89 dereference failure: invalid integer address in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.41] line 89 dereference failure: pointer outside object bounds in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.40] line 89 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.39] line 89 dereference failure: dead object in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.38] line 89 dereference failure: deallocated dynamic object in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.36] line 89 dereference failure: pointer NULL in pbuf[(signed long int)1][(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.35] line 89 dereference failure: invalid integer address in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.34] line 89 dereference failure: pointer outside object bounds in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.33] line 89 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.32] line 89 dereference failure: dead object in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.31] line 89 dereference failure: deallocated dynamic object in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.30] line 89 dereference failure: pointer invalid in pbuf[(signed long int)1]: SUCCESS
[uninit_pointer_005_func_001.pointer_dereference.29] line 89 dereference failure: pointer NULL in pbuf[(signed long int)1]: SUCCESS

uninit_pointer.c function uninit_pointer_006_func_001
[uninit_pointer_006_func_001.pointer_dereference.3] line 103 dereference failure: deallocated dynamic object in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.14] line 103 dereference failure: invalid integer address in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.13] line 103 dereference failure: pointer outside object bounds in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.12] line 103 dereference failure: pointer outside dynamic object bounds in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.11] line 103 dereference failure: dead object in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.10] line 103 dereference failure: deallocated dynamic object in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.9] line 103 dereference failure: pointer invalid in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.8] line 103 dereference failure: pointer NULL in *(*pp): SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.7] line 103 dereference failure: invalid integer address in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.6] line 103 dereference failure: pointer outside object bounds in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.5] line 103 dereference failure: pointer outside dynamic object bounds in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.4] line 103 dereference failure: dead object in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.2] line 103 dereference failure: pointer invalid in *pp: SUCCESS
[uninit_pointer_006_func_001.pointer_dereference.1] line 103 dereference failure: pointer NULL in *pp: SUCCESS

uninit_pointer.c function uninit_pointer_007
[uninit_pointer_007.pointer_dereference.1] line 130 dereference failure: pointer NULL in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.14] line 130 dereference failure: invalid integer address in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.13] line 130 dereference failure: pointer outside object bounds in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.12] line 130 dereference failure: pointer outside dynamic object bounds in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.11] line 130 dereference failure: dead object in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.10] line 130 dereference failure: deallocated dynamic object in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.9] line 130 dereference failure: pointer invalid in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.8] line 130 dereference failure: pointer NULL in (*pbuf[(signed long int)i])[(signed long int)j]: SUCCESS
[uninit_pointer_007.pointer_dereference.6] line 130 dereference failure: pointer outside object bounds in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.5] line 130 dereference failure: pointer outside dynamic object bounds in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.4] line 130 dereference failure: dead object in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.3] line 130 dereference failure: deallocated dynamic object in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.2] line 130 dereference failure: pointer invalid in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.array_bounds.1] line 130 array `pbuf' lower bound in pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.array_bounds.2] line 130 array `pbuf' upper bound in pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.pointer_dereference.7] line 130 dereference failure: invalid integer address in *pbuf[(signed long int)i]: SUCCESS
[uninit_pointer_007.precondition_instance.2] line 132 free argument has offset zero: SUCCESS
[uninit_pointer_007.precondition_instance.4] line 132 free called for new[] object: SUCCESS
[uninit_pointer_007.precondition_instance.3] line 132 double free: SUCCESS
[uninit_pointer_007.precondition_instance.1] line 132 free argument must be dynamic object: SUCCESS
[uninit_pointer_007.precondition_instance.5] line 133 free argument must be dynamic object: SUCCESS
[uninit_pointer_007.precondition_instance.6] line 133 free argument has offset zero: SUCCESS
[uninit_pointer_007.precondition_instance.7] line 133 double free: SUCCESS
[uninit_pointer_007.precondition_instance.8] line 133 free called for new[] object: SUCCESS
[uninit_pointer_007.precondition_instance.10] line 134 free argument has offset zero: SUCCESS
[uninit_pointer_007.precondition_instance.12] line 134 free called for new[] object: SUCCESS
[uninit_pointer_007.precondition_instance.11] line 134 double free: SUCCESS
[uninit_pointer_007.precondition_instance.9] line 134 free argument must be dynamic object: SUCCESS
[uninit_pointer_007.precondition_instance.13] line 135 free argument must be dynamic object: SUCCESS
[uninit_pointer_007.precondition_instance.14] line 135 free argument has offset zero: SUCCESS
[uninit_pointer_007.precondition_instance.15] line 135 double free: SUCCESS
[uninit_pointer_007.precondition_instance.16] line 135 free called for new[] object: SUCCESS

uninit_pointer.c function uninit_pointer_008_func_001
[uninit_pointer_008_func_001.pointer_dereference.1] line 151 dereference failure: pointer NULL in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.2] line 151 dereference failure: pointer invalid in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.3] line 151 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.4] line 151 dereference failure: dead object in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.5] line 151 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.6] line 151 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS
[uninit_pointer_008_func_001.pointer_dereference.7] line 151 dereference failure: invalid integer address in *((signed int *)(char *)((char *)p + (signed long int)8ul)): SUCCESS

uninit_pointer.c function uninit_pointer_010_func_001
[uninit_pointer_010_func_001.pointer_dereference.5] line 199 dereference failure: pointer outside dynamic object bounds in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.6] line 199 dereference failure: pointer outside object bounds in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.7] line 199 dereference failure: invalid integer address in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.1] line 199 dereference failure: pointer NULL in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.3] line 199 dereference failure: deallocated dynamic object in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.2] line 199 dereference failure: pointer invalid in *cptr: SUCCESS
[uninit_pointer_010_func_001.pointer_dereference.4] line 199 dereference failure: dead object in *cptr: SUCCESS

uninit_pointer.c function uninit_pointer_011
[uninit_pointer_011.pointer_dereference.1] line 222 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.2] line 222 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.3] line 222 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.4] line 222 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.5] line 222 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.6] line 222 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.7] line 222 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.12] line 230 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.14] line 230 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.13] line 230 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.11] line 230 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.10] line 230 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.9] line 230 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_011.pointer_dereference.8] line 230 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS

uninit_pointer.c function uninit_pointer_012
[uninit_pointer_012.pointer_dereference.1] line 249 dereference failure: dead object in fptr[(signed long int)i]: SUCCESS
[uninit_pointer_012.pointer_dereference.2] line 249 dereference failure: pointer outside object bounds in fptr[(signed long int)i]: SUCCESS
[uninit_pointer_012.pointer_dereference.3] line 253 dereference failure: dead object in fptr[(signed long int)i]: SUCCESS
[uninit_pointer_012.pointer_dereference.4] line 253 dereference failure: pointer outside object bounds in fptr[(signed long int)i]: SUCCESS
[uninit_pointer_012.array_bounds.1] line 253 array `arr' lower bound in arr[(signed long int)i]: SUCCESS
[uninit_pointer_012.array_bounds.2] line 253 array `arr' upper bound in arr[(signed long int)i]: SUCCESS
[uninit_pointer_012.pointer_dereference.5] line 253 dereference failure: dead object in fptr[(signed long int)i]: SUCCESS
[uninit_pointer_012.pointer_dereference.6] line 253 dereference failure: pointer outside object bounds in fptr[(signed long int)i]: SUCCESS

uninit_pointer.c function uninit_pointer_013
[uninit_pointer_013.pointer_dereference.7] line 267 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.6] line 267 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.5] line 267 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.3] line 267 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.2] line 267 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.1] line 267 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.4] line 267 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.19] line 276 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.array_bounds.4] line 276 array `arr'[] upper bound in arr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.array_bounds.3] line 276 array `arr'[] lower bound in arr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.array_bounds.2] line 276 array `arr' upper bound in arr[(signed long int)i]: SUCCESS
[uninit_pointer_013.array_bounds.1] line 276 array `arr' lower bound in arr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.21] line 276 dereference failure: invalid integer address in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.pointer_dereference.20] line 276 dereference failure: pointer outside object bounds in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.pointer_dereference.18] line 276 dereference failure: dead object in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.pointer_dereference.17] line 276 dereference failure: deallocated dynamic object in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.pointer_dereference.15] line 276 dereference failure: pointer NULL in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.pointer_dereference.8] line 276 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.9] line 276 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.10] line 276 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.11] line 276 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.12] line 276 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.13] line 276 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.14] line 276 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.16] line 276 dereference failure: pointer invalid in ptr[(signed long int)i][(signed long int)j]: SUCCESS
[uninit_pointer_013.precondition_instance.3] line 278 double free: SUCCESS
[uninit_pointer_013.precondition_instance.4] line 278 free called for new[] object: SUCCESS
[uninit_pointer_013.precondition_instance.2] line 278 free argument has offset zero: SUCCESS
[uninit_pointer_013.precondition_instance.1] line 278 free argument must be dynamic object: SUCCESS
[uninit_pointer_013.pointer_dereference.35] line 278 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.34] line 278 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.33] line 278 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.32] line 278 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.31] line 278 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.29] line 278 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.28] line 278 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.27] line 278 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.26] line 278 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.25] line 278 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.24] line 278 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.22] line 278 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.23] line 278 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.30] line 278 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.39] line 279 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.42] line 279 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.41] line 279 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.40] line 279 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.38] line 279 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.37] line 279 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.pointer_dereference.36] line 279 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[uninit_pointer_013.precondition_instance.5] line 281 free argument must be dynamic object: SUCCESS
[uninit_pointer_013.precondition_instance.6] line 281 free argument has offset zero: SUCCESS
[uninit_pointer_013.precondition_instance.7] line 281 double free: SUCCESS
[uninit_pointer_013.precondition_instance.8] line 281 free called for new[] object: SUCCESS

uninit_pointer.c function uninit_pointer_014_func_001
[uninit_pointer_014_func_001.pointer_dereference.3] line 305 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.7] line 305 dereference failure: invalid integer address in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.6] line 305 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.5] line 305 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.2] line 305 dereference failure: pointer invalid in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.1] line 305 dereference failure: pointer NULL in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.4] line 305 dereference failure: dead object in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.8] line 306 dereference failure: pointer NULL in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.9] line 306 dereference failure: pointer invalid in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.10] line 306 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.11] line 306 dereference failure: dead object in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.12] line 306 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.13] line 306 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.14] line 306 dereference failure: invalid integer address in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.15] line 315 dereference failure: pointer NULL in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.21] line 315 dereference failure: invalid integer address in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.20] line 315 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.19] line 315 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.18] line 315 dereference failure: dead object in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.17] line 315 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.16] line 315 dereference failure: pointer invalid in *((signed int *)(char *)((char *)s + (signed long int)0ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.22] line 316 dereference failure: pointer NULL in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.28] line 316 dereference failure: invalid integer address in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.27] line 316 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.26] line 316 dereference failure: pointer outside dynamic object bounds in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.24] line 316 dereference failure: deallocated dynamic object in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.23] line 316 dereference failure: pointer invalid in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS
[uninit_pointer_014_func_001.pointer_dereference.25] line 316 dereference failure: dead object in *((signed int *)(char *)((char *)s + (signed long int)4ul)): SUCCESS

uninit_pointer.c function uninit_pointer_014
[uninit_pointer_014.pointer_dereference.7] line 334 dereference failure: invalid integer address in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.6] line 334 dereference failure: pointer outside object bounds in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.5] line 334 dereference failure: pointer outside dynamic object bounds in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.4] line 334 dereference failure: dead object in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.3] line 334 dereference failure: deallocated dynamic object in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.2] line 334 dereference failure: pointer invalid in *s: SUCCESS
[uninit_pointer_014.pointer_dereference.1] line 334 dereference failure: pointer NULL in *s: SUCCESS
[uninit_pointer_014.precondition_instance.1] line 335 free argument must be dynamic object: SUCCESS
[uninit_pointer_014.precondition_instance.2] line 335 free argument has offset zero: SUCCESS
[uninit_pointer_014.precondition_instance.3] line 335 double free: SUCCESS
[uninit_pointer_014.precondition_instance.4] line 335 free called for new[] object: SUCCESS

uninit_pointer.c function uninit_pointer_015_func_001
[uninit_pointer_015_func_001.pointer_dereference.2] line 351 dereference failure: pointer invalid in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.7] line 351 dereference failure: invalid integer address in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.6] line 351 dereference failure: pointer outside object bounds in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.5] line 351 dereference failure: pointer outside dynamic object bounds in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.4] line 351 dereference failure: dead object in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.3] line 351 dereference failure: deallocated dynamic object in *ptr: SUCCESS
[uninit_pointer_015_func_001.pointer_dereference.1] line 351 dereference failure: pointer NULL in *ptr: SUCCESS

uninit_pointer.c function uninit_pointer_016_func_002
[uninit_pointer_016_func_002.pointer_dereference.1] line 382 dereference failure: pointer NULL in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.2] line 382 dereference failure: pointer invalid in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.3] line 382 dereference failure: deallocated dynamic object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.4] line 382 dereference failure: dead object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.5] line 382 dereference failure: pointer outside dynamic object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.6] line 382 dereference failure: pointer outside object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.7] line 382 dereference failure: invalid integer address in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.14] line 384 dereference failure: invalid integer address in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.13] line 384 dereference failure: pointer outside object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.12] line 384 dereference failure: pointer outside dynamic object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.10] line 384 dereference failure: deallocated dynamic object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.9] line 384 dereference failure: pointer invalid in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.8] line 384 dereference failure: pointer NULL in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016_func_002.pointer_dereference.11] line 384 dereference failure: dead object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS

uninit_pointer.c function uninit_pointer_016
[uninit_pointer_016.pointer_dereference.7] line 402 dereference failure: invalid integer address in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.6] line 402 dereference failure: pointer outside object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.5] line 402 dereference failure: pointer outside dynamic object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.4] line 402 dereference failure: dead object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.3] line 402 dereference failure: deallocated dynamic object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.2] line 402 dereference failure: pointer invalid in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.1] line 402 dereference failure: pointer NULL in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.15] line 406 dereference failure: pointer NULL in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.precondition_instance.4] line 406 free called for new[] object: SUCCESS
[uninit_pointer_016.precondition_instance.3] line 406 double free: SUCCESS
[uninit_pointer_016.precondition_instance.2] line 406 free argument has offset zero: SUCCESS
[uninit_pointer_016.precondition_instance.1] line 406 free argument must be dynamic object: SUCCESS
[uninit_pointer_016.pointer_dereference.21] line 406 dereference failure: invalid integer address in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.20] line 406 dereference failure: pointer outside object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.19] line 406 dereference failure: pointer outside dynamic object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.18] line 406 dereference failure: dead object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.16] line 406 dereference failure: pointer invalid in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.8] line 406 dereference failure: pointer NULL in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.9] line 406 dereference failure: pointer invalid in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.10] line 406 dereference failure: deallocated dynamic object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.11] line 406 dereference failure: dead object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.12] line 406 dereference failure: pointer outside dynamic object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.13] line 406 dereference failure: pointer outside object bounds in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.14] line 406 dereference failure: invalid integer address in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.pointer_dereference.17] line 406 dereference failure: deallocated dynamic object in uninit_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[uninit_pointer_016.precondition_instance.5] line 409 free argument must be dynamic object: SUCCESS
[uninit_pointer_016.precondition_instance.6] line 409 free argument has offset zero: SUCCESS
[uninit_pointer_016.precondition_instance.7] line 409 double free: SUCCESS
[uninit_pointer_016.precondition_instance.8] line 409 free called for new[] object: SUCCESS
[uninit_pointer_016.precondition_instance.9] line 410 free argument must be dynamic object: SUCCESS
[uninit_pointer_016.precondition_instance.10] line 410 free argument has offset zero: SUCCESS
[uninit_pointer_016.precondition_instance.11] line 410 double free: SUCCESS
[uninit_pointer_016.precondition_instance.12] line 410 free called for new[] object: SUCCESS

** 0 of 282 failed (1 iteration)
VERIFICATION SUCCESSFUL
