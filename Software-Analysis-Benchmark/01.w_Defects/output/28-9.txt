==17571== Memcheck, a memory error detector
==17571== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17571== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==17571== Command: ./01_w_Defects 28009
==17571== 
==17571== 
==17571== HEAP SUMMARY:
==17571==     in use at exit: 0 bytes in 0 blocks
==17571==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==17571== 
==17571== All heap blocks were freed -- no leaks are possible
==17571== 
==17571== For counts of detected and suppressed errors, rerun with: -v
==17571== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
