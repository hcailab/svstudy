==18393== Memcheck, a memory error detector
==18393== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18393== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18393== Command: ./01_w_Defects 29038
==18393== 
==18393== 
==18393== HEAP SUMMARY:
==18393==     in use at exit: 0 bytes in 0 blocks
==18393==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18393== 
==18393== All heap blocks were freed -- no leaks are possible
==18393== 
==18393== For counts of detected and suppressed errors, rerun with: -v
==18393== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
