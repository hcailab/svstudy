==18329== Memcheck, a memory error detector
==18329== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18329== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18329== Command: ./01_w_Defects 29024
==18329== 
==18329== 
==18329== HEAP SUMMARY:
==18329==     in use at exit: 0 bytes in 0 blocks
==18329==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18329== 
==18329== All heap blocks were freed -- no leaks are possible
==18329== 
==18329== For counts of detected and suppressed errors, rerun with: -v
==18329== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
