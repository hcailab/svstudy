==18200== Memcheck, a memory error detector
==18200== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18200== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18200== Command: ./01_w_Defects 28021
==18200== 
==18200== 
==18200== HEAP SUMMARY:
==18200==     in use at exit: 0 bytes in 0 blocks
==18200==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18200== 
==18200== All heap blocks were freed -- no leaks are possible
==18200== 
==18200== For counts of detected and suppressed errors, rerun with: -v
==18200== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
