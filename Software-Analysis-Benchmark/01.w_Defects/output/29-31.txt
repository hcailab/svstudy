==18363== Memcheck, a memory error detector
==18363== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18363== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18363== Command: ./01_w_Defects 29031
==18363== 
==18363== 
==18363== HEAP SUMMARY:
==18363==     in use at exit: 0 bytes in 0 blocks
==18363==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18363== 
==18363== All heap blocks were freed -- no leaks are possible
==18363== 
==18363== For counts of detected and suppressed errors, rerun with: -v
==18363== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
