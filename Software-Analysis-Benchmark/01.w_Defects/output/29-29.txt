==18354== Memcheck, a memory error detector
==18354== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18354== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18354== Command: ./01_w_Defects 29029
==18354== 
==18354== 
==18354== HEAP SUMMARY:
==18354==     in use at exit: 0 bytes in 0 blocks
==18354==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18354== 
==18354== All heap blocks were freed -- no leaks are possible
==18354== 
==18354== For counts of detected and suppressed errors, rerun with: -v
==18354== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
