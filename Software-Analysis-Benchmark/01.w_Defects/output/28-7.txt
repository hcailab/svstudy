==17556== Memcheck, a memory error detector
==17556== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17556== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==17556== Command: ./01_w_Defects 28007
==17556== 
==17556== Warning: set address range perms: large range [0x5bea040, 0x38f1d36b) (undefined)
==17556== Invalid read of size 1
==17556==    at 0x10B1B6: memory_allocation_failure_007_func_001 (memory_allocation_failure.c:222)
==17556==    by 0x10B1B6: memory_allocation_failure_007 (memory_allocation_failure.c:241)
==17556==    by 0x10BADE: memory_allocation_failure_main (memory_allocation_failure.c:758)
==17556==    by 0x109C00: main (main.c:188)
==17556==  Address 0x2032333269 is not stack'd, malloc'd or (recently) free'd
==17556== 
==17556== 
==17556== Process terminating with default action of signal 11 (SIGSEGV)
==17556==  Access not within mapped region at address 0x2032333269
==17556==    at 0x10B1B6: memory_allocation_failure_007_func_001 (memory_allocation_failure.c:222)
==17556==    by 0x10B1B6: memory_allocation_failure_007 (memory_allocation_failure.c:241)
==17556==    by 0x10BADE: memory_allocation_failure_main (memory_allocation_failure.c:758)
==17556==    by 0x109C00: main (main.c:188)
==17556==  If you believe this happened as a result of a stack
==17556==  overflow in your program's main thread (unlikely but
==17556==  possible), you can try to increase the size of the
==17556==  main thread stack using the --main-stacksize= flag.
==17556==  The main thread stack size used in this run was 8388608.
==17556== 
==17556== HEAP SUMMARY:
==17556==     in use at exit: 858,993,451 bytes in 1 blocks
==17556==   total heap usage: 2 allocs, 1 frees, 858,994,475 bytes allocated
==17556== 
==17556== LEAK SUMMARY:
==17556==    definitely lost: 0 bytes in 0 blocks
==17556==    indirectly lost: 0 bytes in 0 blocks
==17556==      possibly lost: 0 bytes in 0 blocks
==17556==    still reachable: 858,993,451 bytes in 1 blocks
==17556==         suppressed: 0 bytes in 0 blocks
==17556== Reachable blocks (those to which a pointer was found) are not shown.
==17556== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==17556== 
==17556== For counts of detected and suppressed errors, rerun with: -v
==17556== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
Segmentation fault (core dumped)
