==17550== Memcheck, a memory error detector
==17550== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==17550== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==17550== Command: ./01_w_Defects 28004
==17550== 
==17550== Warning: set address range perms: large range [0x59e43040, 0xc04a9694) (undefined)
==17550== Warning: set address range perms: large range [0x59e43028, 0xc04a96ac) (noaccess)
==17550== 
==17550== HEAP SUMMARY:
==17550==     in use at exit: 0 bytes in 0 blocks
==17550==   total heap usage: 2 allocs, 2 frees, 1,717,987,924 bytes allocated
==17550== 
==17550== All heap blocks were freed -- no leaks are possible
==17550== 
==17550== For counts of detected and suppressed errors, rerun with: -v
==17550== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
