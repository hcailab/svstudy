==18266== Memcheck, a memory error detector
==18266== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==18266== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==18266== Command: ./01_w_Defects 29006
==18266== 
==18266== 
==18266== HEAP SUMMARY:
==18266==     in use at exit: 0 bytes in 0 blocks
==18266==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==18266== 
==18266== All heap blocks were freed -- no leaks are possible
==18266== 
==18266== For counts of detected and suppressed errors, rerun with: -v
==18266== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
