#!/bin/sh
echo "Software Vulnerabilities Benchmark Autorun Script"
echo "Format:./template.sh compiler-and-flags checking-tool output-dir"
echo "Default compiler-and-flags: gcc"
echo "type '0' if you want program running directly."
echo "Default output-dir: ./output"
echo "File 13, 15, 21, 26, 27, 36, 40 include endless loop, Please run it manually"
if [ ! $1 ]; then
    CC=gcc
else
    CC=$1
fi

if [ "$2" = "0" ]; then
    TOOL=""
else
    TOOL=$2
fi

if [ ! $3 ]; then
    OUTPUT=./output
else
    OUTPUT=$3
fi

CC="clang -g -fmemsafety"
TOOL=""
OUTPUT=./SafeCode


echo CC=$CC
echo TOOL=$TOOL
echo OUTPUT=$OUTPUT


cd 02.wo_Defects
mkdir $OUTPUT
sed -i 's#gcc#clang -g -fmemsafety#g' Makefile
#sed -i 's#gcc#'$CC'#g' Makefile
make clean
make
#sed -i 's#'$CC'#gcc#g' Makefile
sed -i 's#clang -g -fmemsafety#gcc#g' Makefile

for i in `seq 1 51`
do   
    for j in `seq 1 54`
    do   
       #if [ "$i" != "13" ] && [ "$i" != "15" ] && [ "$i" != "21" ] && [ "$i" != "26" ] && [ "$i" != "27" ] && [ "$i" != "36" ] && [ "$i" != "40" ]; then
	    k=`expr 1000 \* $i + $j`
            $TOOL ./02_wo_Defects $k 2> $OUTPUT/$i-$j.txt
	#fi
    done
done  


echo "Software Vulnerabilities Benchmark Autorun Script"
echo "Format:./template.sh compiler-and-flags checking-tool output-dir"
echo "Default compiler-and-flags: gcc"
echo "type '0' if you want program running directly."
echo "Default output-dir: ./output"
echo "File 13, 15, 21, 26, 27, 36, 40 include endless loop, Please run it manually"


#cd $OUTPUT
#for file in `ls ./`; do size=`du $file | awk '{print \$1}'`; [ $size -lt 700 ] && rm $file; done


