CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing function_return_value_unchecked.c
Converting
Type-checking function_return_value_unchecked
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop strcpy.0 iteration 1 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 2 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 3 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 4 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 5 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 6 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcmp.0 iteration 1 file <builtin-library-strcmp> line 41 function strcmp thread 0
Unwinding loop strcmp.0 iteration 2 file <builtin-library-strcmp> line 41 function strcmp thread 0
Unwinding loop strcmp.0 iteration 3 file <builtin-library-strcmp> line 41 function strcmp thread 0
Unwinding loop strcmp.0 iteration 4 file <builtin-library-strcmp> line 41 function strcmp thread 0
Unwinding loop strcmp.0 iteration 5 file <builtin-library-strcmp> line 41 function strcmp thread 0
Unwinding loop strcmp.0 iteration 6 file <builtin-library-strcmp> line 41 function strcmp thread 0
size of program expression: 353 steps
simple slicing removed 113 assignments
Generated 197 VCC(s), 14 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
947 variables, 224 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00212525s

** Results:
<builtin-library-strcmp> function strcmp
[strcmp.pointer_dereference.2] line 28 dereference failure: pointer invalid in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.1] line 28 dereference failure: pointer NULL in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.7] line 28 dereference failure: invalid integer address in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.6] line 28 dereference failure: pointer outside object bounds in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.4] line 28 dereference failure: dead object in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.3] line 28 dereference failure: deallocated dynamic object in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.5] line 28 dereference failure: pointer outside dynamic object bounds in s1[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.8] line 29 dereference failure: pointer NULL in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.9] line 29 dereference failure: pointer invalid in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.10] line 29 dereference failure: deallocated dynamic object in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.11] line 29 dereference failure: dead object in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.12] line 29 dereference failure: pointer outside dynamic object bounds in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.13] line 29 dereference failure: pointer outside object bounds in s2[(signed long int)i]: SUCCESS
[strcmp.pointer_dereference.14] line 29 dereference failure: invalid integer address in s2[(signed long int)i]: SUCCESS

<builtin-library-strcpy> function strcpy
[strcpy.pointer_dereference.3] line 29 dereference failure: deallocated dynamic object in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.7] line 29 dereference failure: invalid integer address in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.6] line 29 dereference failure: pointer outside object bounds in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.5] line 29 dereference failure: pointer outside dynamic object bounds in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.4] line 29 dereference failure: dead object in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.2] line 29 dereference failure: pointer invalid in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.1] line 29 dereference failure: pointer NULL in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.8] line 30 dereference failure: pointer NULL in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.9] line 30 dereference failure: pointer invalid in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.10] line 30 dereference failure: deallocated dynamic object in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.11] line 30 dereference failure: dead object in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.12] line 30 dereference failure: pointer outside dynamic object bounds in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.13] line 30 dereference failure: pointer outside object bounds in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.14] line 30 dereference failure: invalid integer address in dst[(signed long int)i]: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_004_s_001_func_001
[function_return_value_unchecked_004_s_001_func_001.precondition_instance.1] line 109 strcpy src/dst overlap: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_006_func_001
[function_return_value_unchecked_006_func_001.pointer_dereference.2] line 198 dereference failure: deallocated dynamic object in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.3] line 198 dereference failure: dead object in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.4] line 198 dereference failure: pointer outside dynamic object bounds in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.5] line 198 dereference failure: pointer outside object bounds in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.6] line 198 dereference failure: invalid integer address in p[(signed long int)i][(signed long int)j]: SUCCESS
[function_return_value_unchecked_006_func_001.pointer_dereference.1] line 198 dereference failure: pointer NULL in p[(signed long int)i][(signed long int)j]: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_006
[function_return_value_unchecked_006.array_bounds.1] line 214 array dynamic object upper bound in ptr[(signed long int)j][0l]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.7] line 214 dereference failure: invalid integer address in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.6] line 214 dereference failure: pointer outside object bounds in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.5] line 214 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.3] line 214 dereference failure: deallocated dynamic object in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.2] line 214 dereference failure: pointer invalid in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.1] line 214 dereference failure: pointer NULL in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.4] line 214 dereference failure: dead object in ptr[(signed long int)j]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.12] line 216 dereference failure: pointer outside object bounds in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.13] line 216 dereference failure: invalid integer address in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.11] line 216 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.10] line 216 dereference failure: dead object in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.9] line 216 dereference failure: deallocated dynamic object in ptr[(signed long int)j][(signed long int)1]: SUCCESS
[function_return_value_unchecked_006.pointer_dereference.8] line 216 dereference failure: pointer NULL in ptr[(signed long int)j][(signed long int)1]: SUCCESS

function_return_value_unchecked.c function function_return_value_unchecked_009_func_001
[function_return_value_unchecked_009_func_001.pointer_dereference.1] line 269 dereference failure: pointer NULL in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.2] line 269 dereference failure: pointer invalid in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.3] line 269 dereference failure: deallocated dynamic object in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.4] line 269 dereference failure: dead object in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.5] line 269 dereference failure: pointer outside dynamic object bounds in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.6] line 269 dereference failure: pointer outside object bounds in a[(signed long int)i]: SUCCESS
[function_return_value_unchecked_009_func_001.pointer_dereference.7] line 269 dereference failure: invalid integer address in a[(signed long int)i]: SUCCESS

** 0 of 56 failed (1 iteration)
VERIFICATION SUCCESSFUL
