CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing ow_memcpy.c
Converting
Type-checking ow_memcpy
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop ow_memcpy_002_func_001.0 iteration 1 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 2 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 3 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 4 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 5 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 6 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 7 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
Unwinding loop ow_memcpy_002_func_001.0 iteration 8 file ow_memcpy.c line 39 function ow_memcpy_002_func_001 thread 0
size of program expression: 102 steps
simple slicing removed 0 assignments
Generated 112 VCC(s), 0 remaining after simplification
VERIFICATION SUCCESSFUL
