CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing double_free.c
Converting
Type-checking double_free
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop double_free_004.0 iteration 1 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 2 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 3 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 4 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 5 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 6 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 7 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 8 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 9 file double_free.c line 71 function double_free_004 thread 0
Unwinding loop double_free_004.0 iteration 10 file double_free.c line 71 function double_free_004 thread 0
size of program expression: 141 steps
simple slicing removed 27 assignments
Generated 74 VCC(s), 10 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
462 variables, 132 clauses
SAT checker: instance is UNSATISFIABLE
Runtime decision procedure: 0.00085838s

** Results:
double_free.c function double_free_001
[double_free_001.precondition_instance.2] line 21 free argument has offset zero: SUCCESS
[double_free_001.precondition_instance.1] line 21 free argument must be dynamic object: SUCCESS
[double_free_001.precondition_instance.4] line 21 free called for new[] object: SUCCESS
[double_free_001.precondition_instance.3] line 21 double free: SUCCESS

double_free.c function double_free_002
[double_free_002.pointer_dereference.1] line 36 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.7] line 36 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.6] line 36 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.5] line 36 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.4] line 36 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.3] line 36 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[double_free_002.pointer_dereference.2] line 36 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[double_free_002.precondition_instance.1] line 38 free argument must be dynamic object: SUCCESS
[double_free_002.precondition_instance.2] line 38 free argument has offset zero: SUCCESS
[double_free_002.precondition_instance.3] line 38 double free: SUCCESS
[double_free_002.precondition_instance.4] line 38 free called for new[] object: SUCCESS
[double_free_002.precondition_instance.5] line 40 free argument must be dynamic object: SUCCESS
[double_free_002.precondition_instance.6] line 40 free argument has offset zero: SUCCESS
[double_free_002.precondition_instance.7] line 40 double free: SUCCESS
[double_free_002.precondition_instance.8] line 40 free called for new[] object: SUCCESS

double_free.c function double_free_003
[double_free_003.pointer_dereference.7] line 55 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.6] line 55 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.5] line 55 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.3] line 55 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.2] line 55 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.1] line 55 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[double_free_003.pointer_dereference.4] line 55 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[double_free_003.precondition_instance.1] line 59 free argument must be dynamic object: SUCCESS
[double_free_003.precondition_instance.2] line 59 free argument has offset zero: SUCCESS
[double_free_003.precondition_instance.3] line 59 double free: SUCCESS
[double_free_003.precondition_instance.4] line 59 free called for new[] object: SUCCESS

double_free.c function double_free_004
[double_free_004.pointer_dereference.3] line 73 dereference failure: deallocated dynamic object in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.7] line 73 dereference failure: invalid integer address in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.6] line 73 dereference failure: pointer outside object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.5] line 73 dereference failure: pointer outside dynamic object bounds in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.4] line 73 dereference failure: dead object in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.2] line 73 dereference failure: pointer invalid in ptr[(signed long int)i]: SUCCESS
[double_free_004.pointer_dereference.1] line 73 dereference failure: pointer NULL in ptr[(signed long int)i]: SUCCESS
[double_free_004.precondition_instance.1] line 76 free argument must be dynamic object: SUCCESS
[double_free_004.precondition_instance.2] line 76 free argument has offset zero: SUCCESS
[double_free_004.precondition_instance.3] line 76 double free: SUCCESS
[double_free_004.precondition_instance.4] line 76 free called for new[] object: SUCCESS

double_free.c function double_free_005
[double_free_005.precondition_instance.4] line 89 free called for new[] object: SUCCESS
[double_free_005.precondition_instance.2] line 89 free argument has offset zero: SUCCESS
[double_free_005.precondition_instance.1] line 89 free argument must be dynamic object: SUCCESS
[double_free_005.precondition_instance.3] line 89 double free: SUCCESS

double_free.c function double_free_006
[double_free_006.precondition_instance.1] line 101 free argument must be dynamic object: SUCCESS
[double_free_006.precondition_instance.2] line 101 free argument has offset zero: SUCCESS
[double_free_006.precondition_instance.3] line 101 double free: SUCCESS
[double_free_006.precondition_instance.4] line 101 free called for new[] object: SUCCESS
[double_free_006.precondition_instance.5] line 103 free argument must be dynamic object: SUCCESS
[double_free_006.precondition_instance.6] line 103 free argument has offset zero: SUCCESS
[double_free_006.precondition_instance.7] line 103 double free: SUCCESS
[double_free_006.precondition_instance.8] line 103 free called for new[] object: SUCCESS

double_free.c function double_free_007
[double_free_007.precondition_instance.1] line 117 free argument must be dynamic object: SUCCESS
[double_free_007.precondition_instance.4] line 117 free called for new[] object: SUCCESS
[double_free_007.precondition_instance.3] line 117 double free: SUCCESS
[double_free_007.precondition_instance.2] line 117 free argument has offset zero: SUCCESS
[double_free_007.precondition_instance.5] line 119 free argument must be dynamic object: SUCCESS
[double_free_007.precondition_instance.6] line 119 free argument has offset zero: SUCCESS
[double_free_007.precondition_instance.7] line 119 double free: SUCCESS
[double_free_007.precondition_instance.8] line 119 free called for new[] object: SUCCESS

double_free.c function double_free_function_008
[double_free_function_008.precondition_instance.4] line 129 free called for new[] object: SUCCESS
[double_free_function_008.precondition_instance.3] line 129 double free: SUCCESS
[double_free_function_008.precondition_instance.1] line 129 free argument must be dynamic object: SUCCESS
[double_free_function_008.precondition_instance.2] line 129 free argument has offset zero: SUCCESS

double_free.c function double_free_009
[double_free_009.precondition_instance.1] line 152 free argument must be dynamic object: SUCCESS
[double_free_009.precondition_instance.2] line 152 free argument has offset zero: SUCCESS
[double_free_009.precondition_instance.3] line 152 double free: SUCCESS
[double_free_009.precondition_instance.4] line 152 free called for new[] object: SUCCESS
[double_free_009.precondition_instance.5] line 155 free argument must be dynamic object: SUCCESS
[double_free_009.precondition_instance.6] line 155 free argument has offset zero: SUCCESS
[double_free_009.precondition_instance.7] line 155 double free: SUCCESS
[double_free_009.precondition_instance.8] line 155 free called for new[] object: SUCCESS

double_free.c function double_free_010
[double_free_010.precondition_instance.2] line 171 free argument has offset zero: SUCCESS
[double_free_010.precondition_instance.4] line 171 free called for new[] object: SUCCESS
[double_free_010.precondition_instance.3] line 171 double free: SUCCESS
[double_free_010.precondition_instance.1] line 171 free argument must be dynamic object: SUCCESS

double_free.c function double_free_011
[double_free_011.precondition_instance.1] line 190 free argument must be dynamic object: SUCCESS
[double_free_011.precondition_instance.2] line 190 free argument has offset zero: SUCCESS
[double_free_011.precondition_instance.3] line 190 double free: SUCCESS
[double_free_011.precondition_instance.4] line 190 free called for new[] object: SUCCESS

double_free.c function double_free_012
[double_free_012.precondition_instance.1] line 208 free argument must be dynamic object: SUCCESS
[double_free_012.precondition_instance.2] line 208 free argument has offset zero: SUCCESS
[double_free_012.precondition_instance.3] line 208 double free: SUCCESS
[double_free_012.precondition_instance.4] line 208 free called for new[] object: SUCCESS

** 0 of 85 failed (1 iteration)
VERIFICATION SUCCESSFUL
