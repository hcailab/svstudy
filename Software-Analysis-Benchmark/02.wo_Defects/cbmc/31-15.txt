CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing null_pointer.c
Converting
Type-checking null_pointer
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop strlen.0 iteration 1 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 2 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 3 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 4 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 5 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 6 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 7 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 8 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 9 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 10 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 11 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 12 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 13 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 14 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 15 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strlen.0 iteration 16 file <builtin-library-strlen> line 18 function strlen thread 0
Unwinding loop strcpy.0 iteration 1 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 2 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 3 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 4 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 5 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 6 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 7 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 8 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 9 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 10 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 11 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 12 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 13 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 14 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 15 file <builtin-library-strcpy> line 32 function strcpy thread 0
Unwinding loop strcpy.0 iteration 16 file <builtin-library-strcpy> line 32 function strcpy thread 0
size of program expression: 290 steps
simple slicing removed 30 assignments
Generated 362 VCC(s), 34 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
940 variables, 460 clauses
SAT checker: instance is UNSATISFIABLE
Runtime decision procedure: 0.00240875s

** Results:
<builtin-library-strcpy> function strcpy
[strcpy.pointer_dereference.2] line 29 dereference failure: pointer invalid in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.3] line 29 dereference failure: deallocated dynamic object in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.4] line 29 dereference failure: dead object in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.5] line 29 dereference failure: pointer outside dynamic object bounds in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.6] line 29 dereference failure: pointer outside object bounds in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.7] line 29 dereference failure: invalid integer address in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.1] line 29 dereference failure: pointer NULL in src[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.10] line 30 dereference failure: deallocated dynamic object in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.14] line 30 dereference failure: invalid integer address in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.13] line 30 dereference failure: pointer outside object bounds in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.12] line 30 dereference failure: pointer outside dynamic object bounds in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.11] line 30 dereference failure: dead object in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.9] line 30 dereference failure: pointer invalid in dst[(signed long int)i]: SUCCESS
[strcpy.pointer_dereference.8] line 30 dereference failure: pointer NULL in dst[(signed long int)i]: SUCCESS

<builtin-library-strlen> function strlen
[strlen.pointer_dereference.2] line 18 dereference failure: pointer invalid in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.7] line 18 dereference failure: invalid integer address in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.6] line 18 dereference failure: pointer outside object bounds in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.5] line 18 dereference failure: pointer outside dynamic object bounds in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.4] line 18 dereference failure: dead object in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.3] line 18 dereference failure: deallocated dynamic object in s[(signed long int)len]: SUCCESS
[strlen.pointer_dereference.1] line 18 dereference failure: pointer NULL in s[(signed long int)len]: SUCCESS

null_pointer.c function null_pointer_001
[null_pointer_001.pointer_dereference.1] line 24 dereference failure: dead object in *p: SUCCESS
[null_pointer_001.pointer_dereference.2] line 24 dereference failure: pointer outside object bounds in *p: SUCCESS

null_pointer.c function null_pointer_002
[null_pointer_002.pointer_dereference.1] line 36 dereference failure: dead object in *p: SUCCESS
[null_pointer_002.pointer_dereference.2] line 36 dereference failure: pointer outside object bounds in *p: SUCCESS

null_pointer.c function null_pointer_003
[null_pointer_003.pointer_dereference.2] line 50 dereference failure: pointer outside object bounds in *pp: SUCCESS
[null_pointer_003.pointer_dereference.9] line 50 dereference failure: invalid integer address in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.8] line 50 dereference failure: pointer outside object bounds in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.7] line 50 dereference failure: pointer outside dynamic object bounds in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.6] line 50 dereference failure: dead object in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.4] line 50 dereference failure: pointer invalid in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.3] line 50 dereference failure: pointer NULL in *(*pp): SUCCESS
[null_pointer_003.pointer_dereference.1] line 50 dereference failure: dead object in *pp: SUCCESS
[null_pointer_003.pointer_dereference.5] line 50 dereference failure: deallocated dynamic object in *(*pp): SUCCESS

null_pointer.c function null_pointer_004
[null_pointer_004.pointer_dereference.1] line 67 dereference failure: dead object in *((signed int *)(char *)((char *)p + (signed long int)0ul)): SUCCESS
[null_pointer_004.pointer_dereference.2] line 67 dereference failure: pointer outside object bounds in *((signed int *)(char *)((char *)p + (signed long int)0ul)): SUCCESS

null_pointer.c function null_pointer_005
[null_pointer_005.pointer_dereference.1] line 99 dereference failure: dead object in *p: SUCCESS
[null_pointer_005.pointer_dereference.2] line 99 dereference failure: pointer outside object bounds in *p: SUCCESS

null_pointer.c function null_pointer_006
[null_pointer_006.pointer_dereference.1] line 111 dereference failure: dead object in *p: SUCCESS
[null_pointer_006.pointer_dereference.2] line 111 dereference failure: pointer outside object bounds in *p: SUCCESS

null_pointer.c function null_pointer_007
[null_pointer_007.pointer_dereference.2] line 123 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_007.pointer_dereference.1] line 123 dereference failure: dead object in *p: SUCCESS

null_pointer.c function null_pointer_008
[null_pointer_008.pointer_dereference.1] line 141 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_008.pointer_dereference.2] line 141 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_008.pointer_dereference.3] line 141 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_008.pointer_dereference.4] line 141 dereference failure: dead object in *p: SUCCESS
[null_pointer_008.pointer_dereference.5] line 141 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_008.pointer_dereference.6] line 141 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_008.pointer_dereference.7] line 141 dereference failure: invalid integer address in *p: SUCCESS

null_pointer.c function null_pointer_009_func_001
[null_pointer_009_func_001.pointer_dereference.3] line 150 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.7] line 150 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.6] line 150 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.5] line 150 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.4] line 150 dereference failure: dead object in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.1] line 150 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_009_func_001.pointer_dereference.2] line 150 dereference failure: pointer invalid in *p: SUCCESS

null_pointer.c function null_pointer_010
[null_pointer_010.pointer_dereference.1] line 169 dereference failure: dead object in *p1: SUCCESS
[null_pointer_010.pointer_dereference.2] line 169 dereference failure: pointer outside object bounds in *p1: SUCCESS

null_pointer.c function null_pointer_011
[null_pointer_011.pointer_dereference.1] line 184 dereference failure: dead object in *p2: SUCCESS
[null_pointer_011.pointer_dereference.2] line 184 dereference failure: pointer outside object bounds in *p2: SUCCESS

null_pointer.c function null_pointer_012
[null_pointer_012.pointer_dereference.1] line 195 dereference failure: dead object in p[(signed long int)3]: SUCCESS
[null_pointer_012.pointer_dereference.2] line 195 dereference failure: pointer outside object bounds in p[(signed long int)3]: SUCCESS

null_pointer.c function null_pointer_013
[null_pointer_013.pointer_dereference.6] line 215 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_013.pointer_dereference.7] line 215 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_013.pointer_dereference.5] line 215 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_013.pointer_dereference.4] line 215 dereference failure: dead object in *p: SUCCESS
[null_pointer_013.pointer_dereference.3] line 215 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_013.pointer_dereference.2] line 215 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_013.pointer_dereference.1] line 215 dereference failure: pointer NULL in *p: SUCCESS

null_pointer.c function null_pointer_014
[null_pointer_014.pointer_dereference.1] line 233 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_014.pointer_dereference.2] line 233 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_014.pointer_dereference.3] line 233 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_014.pointer_dereference.4] line 233 dereference failure: dead object in *p: SUCCESS
[null_pointer_014.pointer_dereference.5] line 233 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.6] line 233 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.7] line 233 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_014.pointer_dereference.8] line 236 dereference failure: pointer NULL in *p: SUCCESS
[null_pointer_014.pointer_dereference.14] line 236 dereference failure: invalid integer address in *p: SUCCESS
[null_pointer_014.pointer_dereference.13] line 236 dereference failure: pointer outside object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.12] line 236 dereference failure: pointer outside dynamic object bounds in *p: SUCCESS
[null_pointer_014.pointer_dereference.10] line 236 dereference failure: deallocated dynamic object in *p: SUCCESS
[null_pointer_014.pointer_dereference.9] line 236 dereference failure: pointer invalid in *p: SUCCESS
[null_pointer_014.pointer_dereference.11] line 236 dereference failure: dead object in *p: SUCCESS

null_pointer.c function null_pointer_015
[null_pointer_015.precondition_instance.1] line 258 strcpy src/dst overlap: SUCCESS
[null_pointer_015.precondition_instance.2] line 259 free argument must be dynamic object: SUCCESS
[null_pointer_015.precondition_instance.3] line 259 free argument has offset zero: SUCCESS
[null_pointer_015.precondition_instance.4] line 259 double free: SUCCESS
[null_pointer_015.precondition_instance.5] line 259 free called for new[] object: SUCCESS

null_pointer.c function null_pointer_016_func_002
[null_pointer_016_func_002.pointer_dereference.2] line 284 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.7] line 284 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.6] line 284 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.5] line 284 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.4] line 284 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.3] line 284 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.1] line 284 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.17] line 290 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.18] line 290 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.16] line 290 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.19] line 290 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.20] line 290 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.21] line 290 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.11] line 290 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.14] line 290 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.15] line 290 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.8] line 290 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.9] line 290 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.12] line 290 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.13] line 290 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016_func_002.pointer_dereference.10] line 290 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS

null_pointer.c function null_pointer_016
[null_pointer_016.pointer_dereference.11] line 308 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.14] line 308 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.13] line 308 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.12] line 308 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.10] line 308 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.9] line 308 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.8] line 308 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i][(signed long int)j]: SUCCESS
[null_pointer_016.pointer_dereference.7] line 308 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.6] line 308 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.5] line 308 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.4] line 308 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.3] line 308 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.2] line 308 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.1] line 308 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.20] line 310 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.precondition_instance.4] line 310 free called for new[] object: SUCCESS
[null_pointer_016.precondition_instance.3] line 310 double free: SUCCESS
[null_pointer_016.precondition_instance.2] line 310 free argument has offset zero: SUCCESS
[null_pointer_016.precondition_instance.1] line 310 free argument must be dynamic object: SUCCESS
[null_pointer_016.pointer_dereference.28] line 310 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.27] line 310 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.26] line 310 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.25] line 310 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.23] line 310 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.22] line 310 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.21] line 310 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.19] line 310 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.18] line 310 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.17] line 310 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.16] line 310 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.15] line 310 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.24] line 310 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.29] line 311 dereference failure: pointer NULL in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.35] line 311 dereference failure: invalid integer address in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.34] line 311 dereference failure: pointer outside object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.33] line 311 dereference failure: pointer outside dynamic object bounds in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.32] line 311 dereference failure: dead object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.31] line 311 dereference failure: deallocated dynamic object in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.pointer_dereference.30] line 311 dereference failure: pointer invalid in null_pointer_016_gbl_doubleptr[(signed long int)i]: SUCCESS
[null_pointer_016.precondition_instance.8] line 313 free called for new[] object: SUCCESS
[null_pointer_016.precondition_instance.6] line 313 free argument has offset zero: SUCCESS
[null_pointer_016.precondition_instance.5] line 313 free argument must be dynamic object: SUCCESS
[null_pointer_016.precondition_instance.7] line 313 double free: SUCCESS

null_pointer.c function null_pointer_017_func_001
[null_pointer_017_func_001.pointer_dereference.1] line 340 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.2] line 340 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.3] line 340 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.4] line 340 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.5] line 340 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.6] line 340 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017_func_001.pointer_dereference.7] line 340 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS

null_pointer.c function null_pointer_017
[null_pointer_017.pointer_dereference.5] line 353 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.precondition_instance.1] line 353 strcpy src/dst overlap: SUCCESS
[null_pointer_017.pointer_dereference.7] line 353 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.6] line 353 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.4] line 353 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.3] line 353 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.2] line 353 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.1] line 353 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.14] line 359 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.precondition_instance.5] line 359 free called for new[] object: SUCCESS
[null_pointer_017.precondition_instance.4] line 359 double free: SUCCESS
[null_pointer_017.precondition_instance.3] line 359 free argument has offset zero: SUCCESS
[null_pointer_017.precondition_instance.2] line 359 free argument must be dynamic object: SUCCESS
[null_pointer_017.pointer_dereference.21] line 359 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.20] line 359 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.19] line 359 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.18] line 359 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.16] line 359 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.15] line 359 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.13] line 359 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.12] line 359 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.11] line 359 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.10] line 359 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.9] line 359 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.8] line 359 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.17] line 359 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.23] line 360 dereference failure: pointer invalid in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.28] line 360 dereference failure: invalid integer address in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.27] line 360 dereference failure: pointer outside object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.26] line 360 dereference failure: pointer outside dynamic object bounds in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.25] line 360 dereference failure: dead object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.24] line 360 dereference failure: deallocated dynamic object in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.pointer_dereference.22] line 360 dereference failure: pointer NULL in null_pointer_017dst[(signed long int)i]: SUCCESS
[null_pointer_017.precondition_instance.6] line 362 free argument must be dynamic object: SUCCESS
[null_pointer_017.precondition_instance.7] line 362 free argument has offset zero: SUCCESS
[null_pointer_017.precondition_instance.8] line 362 double free: SUCCESS
[null_pointer_017.precondition_instance.9] line 362 free called for new[] object: SUCCESS

** 0 of 196 failed (1 iteration)
VERIFICATION SUCCESSFUL
