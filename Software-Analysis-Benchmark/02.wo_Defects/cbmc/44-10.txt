CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing underrun_st.c
Converting
Type-checking underrun_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
Unwinding loop underrun_st_010.0 iteration 1 file underrun_st.c line 143 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 2 file underrun_st.c line 143 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 3 file underrun_st.c line 143 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 4 file underrun_st.c line 143 function underrun_st_010 thread 0
Unwinding loop underrun_st_010.0 iteration 5 file underrun_st.c line 143 function underrun_st_010 thread 0
size of program expression: 72 steps
simple slicing removed 0 assignments
Generated 5 VCC(s), 0 remaining after simplification
VERIFICATION SUCCESSFUL
