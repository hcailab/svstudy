CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing underrun_st.c
Converting
Type-checking underrun_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 45 steps
simple slicing removed 4 assignments
Generated 4 VCC(s), 2 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
733 variables, 1823 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
733 variables, 899 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
733 variables, 111 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00466353s

** Results:
underrun_st.c function underrun_st_002
[underrun_st_002.array_bounds.2] line 33 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[underrun_st_002.array_bounds.1] line 33 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

underrun_st.c function underrun_st_003
[underrun_st_003.array_bounds.1] line 44 array `buf' lower bound in buf[(signed long int)index]: SUCCESS
[underrun_st_003.array_bounds.2] line 44 array `buf' upper bound in buf[(signed long int)index]: SUCCESS
[underrun_st_003.array_bounds.3] line 45 array `buf' lower bound in buf[(signed long int)idx]: FAILURE
[underrun_st_003.array_bounds.4] line 45 array `buf' upper bound in buf[(signed long int)idx]: FAILURE

underrun_st.c function underrun_st_004
[underrun_st_004.pointer_dereference.1] line 58 dereference failure: dead object in *(p - (signed long int)1): SUCCESS
[underrun_st_004.pointer_dereference.2] line 58 dereference failure: pointer outside object bounds in *(p - (signed long int)1): SUCCESS

underrun_st.c function underrun_st_005
[underrun_st_005.pointer_dereference.1] line 71 dereference failure: dead object in *(p - (signed long int)1): SUCCESS
[underrun_st_005.pointer_dereference.2] line 71 dereference failure: pointer outside object bounds in *(p - (signed long int)1): SUCCESS

underrun_st.c function underrun_st_006
[underrun_st_006.pointer_dereference.1] line 84 dereference failure: dead object in *(p - (signed long int)index): SUCCESS
[underrun_st_006.pointer_dereference.2] line 84 dereference failure: pointer outside object bounds in *(p - (signed long int)index): SUCCESS

underrun_st.c function underrun_st_007
[underrun_st_007.array_bounds.2] line 97 array `buf' upper bound in buf[(signed long int)i]: SUCCESS
[underrun_st_007.array_bounds.1] line 97 array `buf' lower bound in buf[(signed long int)i]: SUCCESS
[underrun_st_007.array_bounds.3] line 99 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[underrun_st_007.array_bounds.4] line 99 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

underrun_st.c function underrun_st_008
[underrun_st_008.pointer_dereference.1] line 114 dereference failure: dead object in *p: SUCCESS
[underrun_st_008.pointer_dereference.2] line 114 dereference failure: pointer outside object bounds in *p: SUCCESS

underrun_st.c function underrun_st_009
[underrun_st_009.array_bounds.1] line 129 array `underrun_st_009_gbl_buf' lower bound in underrun_st_009_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_009.array_bounds.2] line 129 array `underrun_st_009_gbl_buf' upper bound in underrun_st_009_gbl_buf[(signed long int)i]: SUCCESS

underrun_st.c function underrun_st_010
[underrun_st_010.pointer_dereference.1] line 145 dereference failure: pointer outside object bounds in *p: SUCCESS

underrun_st.c function underrun_st_011
[underrun_st_011.array_bounds.1] line 160 array `underrun_st_011_gbl_buf' lower bound in underrun_st_011_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_011.array_bounds.2] line 160 array `underrun_st_011_gbl_buf' upper bound in underrun_st_011_gbl_buf[(signed long int)i]: SUCCESS

underrun_st.c function underrun_st_012
[underrun_st_012.pointer_dereference.1] line 177 dereference failure: pointer outside object bounds in *p: SUCCESS

underrun_st.c function underrun_st_013
[underrun_st_013.array_bounds.1] line 195 array `underrun_st_013_gbl_buf' lower bound in underrun_st_013_gbl_buf[(signed long int)i]: SUCCESS
[underrun_st_013.array_bounds.2] line 195 array `underrun_st_013_gbl_buf' upper bound in underrun_st_013_gbl_buf[(signed long int)i]: SUCCESS

** 2 of 26 failed (3 iterations)
VERIFICATION FAILED
