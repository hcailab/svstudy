CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing overrun_st.c
Converting
Type-checking overrun_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 41 steps
simple slicing removed 4 assignments
Generated 2 VCC(s), 2 remaining after simplification
Passing problem to propositional reduction
converting SSA
Running propositional reduction
Post-processing
Solving with MiniSAT 2.2.1 with simplifier
625 variables, 1838 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
625 variables, 906 clauses
SAT checker: instance is SATISFIABLE
Solving with MiniSAT 2.2.1 with simplifier
625 variables, 93 clauses
SAT checker inconsistent: instance is UNSATISFIABLE
Runtime decision procedure: 0.00463965s

** Results:
overrun_st.c function overrun_st_001
[overrun_st_001.array_bounds.2] line 22 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_001.array_bounds.1] line 22 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_002
[overrun_st_002.array_bounds.2] line 33 array `buf' upper bound in buf[(signed long int)idx]: FAILURE
[overrun_st_002.array_bounds.1] line 33 array `buf' lower bound in buf[(signed long int)idx]: FAILURE

overrun_st.c function overrun_st_003
[overrun_st_003.array_bounds.2] line 45 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_003.array_bounds.1] line 45 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_004
[overrun_st_004.array_bounds.2] line 56 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_004.array_bounds.1] line 56 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_005
[overrun_st_005.array_bounds.1] line 67 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_005.array_bounds.2] line 67 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_006
[overrun_st_006.array_bounds.1] line 78 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_006.array_bounds.2] line 78 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_007
[overrun_st_007.array_bounds.1] line 89 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_007.array_bounds.2] line 89 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_008
[overrun_st_008.array_bounds.3] line 100 array `buf'[] lower bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_008.array_bounds.4] line 100 array `buf'[] upper bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_008.array_bounds.2] line 100 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_008.array_bounds.1] line 100 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_009
[overrun_st_009.array_bounds.1] line 111 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_009.array_bounds.2] line 111 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_009.array_bounds.3] line 111 array `buf'[] lower bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_009.array_bounds.4] line 111 array `buf'[] upper bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_009.array_bounds.5] line 111 array `buf'[][] lower bound in buf[(signed long int)idx][(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_009.array_bounds.6] line 111 array `buf'[][] upper bound in buf[(signed long int)idx][(signed long int)idx][(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_010
[overrun_st_010.pointer_dereference.7] line 126 dereference failure: invalid integer address in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.6] line 126 dereference failure: pointer outside object bounds in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.5] line 126 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.3] line 126 dereference failure: deallocated dynamic object in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.2] line 126 dereference failure: pointer invalid in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.1] line 126 dereference failure: pointer NULL in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_010.pointer_dereference.4] line 126 dereference failure: dead object in pbuf[(signed long int)4][(signed long int)5]: SUCCESS

overrun_st.c function overrun_st_011
[overrun_st_011.array_bounds.1] line 143 array `sbuf' lower bound in sbuf[(signed long int)idx]: SUCCESS
[overrun_st_011.array_bounds.2] line 143 array `sbuf' upper bound in sbuf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_013
[overrun_st_013.array_bounds.1] line 170 array `buf' lower bound in buf[(signed long int)index]: SUCCESS
[overrun_st_013.array_bounds.2] line 170 array `buf' upper bound in buf[(signed long int)index]: SUCCESS
[overrun_st_013.array_bounds.3] line 171 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_013.array_bounds.4] line 171 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_014
[overrun_st_014.array_bounds.1] line 183 array `buf' lower bound in buf[(signed long int)index]: SUCCESS
[overrun_st_014.array_bounds.2] line 183 array `buf' upper bound in buf[(signed long int)index]: SUCCESS
[overrun_st_014.array_bounds.3] line 184 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_014.array_bounds.4] line 184 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_015
[overrun_st_015.array_bounds.1] line 195 array `buf' lower bound in buf[(signed long int)(2 * index + 1)]: SUCCESS
[overrun_st_015.array_bounds.2] line 195 array `buf' upper bound in buf[(signed long int)(2 * index + 1)]: SUCCESS
[overrun_st_015.array_bounds.3] line 196 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_015.array_bounds.4] line 196 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_016
[overrun_st_016.array_bounds.1] line 207 array `buf' lower bound in buf[(signed long int)(index * index + 1)]: SUCCESS
[overrun_st_016.array_bounds.2] line 207 array `buf' upper bound in buf[(signed long int)(index * index + 1)]: SUCCESS
[overrun_st_016.array_bounds.3] line 208 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_016.array_bounds.4] line 208 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_017
[overrun_st_017.array_bounds.1] line 223 array `buf' lower bound in buf[(signed long int)return_value_overrun_st_017_func_001]: SUCCESS
[overrun_st_017.array_bounds.2] line 223 array `buf' upper bound in buf[(signed long int)return_value_overrun_st_017_func_001]: SUCCESS
[overrun_st_017.array_bounds.3] line 224 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_017.array_bounds.4] line 224 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_018_func_001
[overrun_st_018_func_001.array_bounds.1] line 234 array `overrun_st_018_buf' lower bound in overrun_st_018_buf[(signed long int)index]: SUCCESS
[overrun_st_018_func_001.array_bounds.2] line 234 array `overrun_st_018_buf' upper bound in overrun_st_018_buf[(signed long int)index]: SUCCESS

overrun_st.c function overrun_st_019
[overrun_st_019.array_bounds.1] line 251 array `indexes' lower bound in indexes[(signed long int)index]: SUCCESS
[overrun_st_019.array_bounds.2] line 251 array `indexes' upper bound in indexes[(signed long int)index]: SUCCESS
[overrun_st_019.array_bounds.3] line 251 array `buf' lower bound in buf[(signed long int)indexes[(signed long int)index]]: SUCCESS
[overrun_st_019.array_bounds.4] line 251 array `buf' upper bound in buf[(signed long int)indexes[(signed long int)index]]: SUCCESS
[overrun_st_019.array_bounds.5] line 252 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_019.array_bounds.6] line 252 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_020
[overrun_st_020.array_bounds.1] line 265 array `buf' lower bound in buf[(signed long int)index1]: SUCCESS
[overrun_st_020.array_bounds.2] line 265 array `buf' upper bound in buf[(signed long int)index1]: SUCCESS
[overrun_st_020.array_bounds.3] line 266 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_020.array_bounds.4] line 266 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_021
[overrun_st_021.array_bounds.1] line 281 array `buf' lower bound in buf[(signed long int)index2]: SUCCESS
[overrun_st_021.array_bounds.2] line 281 array `buf' upper bound in buf[(signed long int)index2]: SUCCESS
[overrun_st_021.array_bounds.3] line 282 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_021.array_bounds.4] line 282 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_022
[overrun_st_022.pointer_dereference.1] line 294 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_022.pointer_dereference.2] line 294 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_022.array_bounds.1] line 295 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_022.array_bounds.2] line 295 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_023
[overrun_st_023.pointer_dereference.1] line 307 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_023.pointer_dereference.2] line 307 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_023.array_bounds.2] line 308 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_023.array_bounds.1] line 308 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_024
[overrun_st_024.pointer_dereference.1] line 321 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_024.pointer_dereference.2] line 321 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_024.array_bounds.1] line 322 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_024.array_bounds.2] line 322 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_025
[overrun_st_025.pointer_dereference.1] line 334 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_025.pointer_dereference.2] line 334 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_025.array_bounds.1] line 335 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_025.array_bounds.2] line 335 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_026
[overrun_st_026.pointer_dereference.1] line 347 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_026.pointer_dereference.2] line 347 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_026.array_bounds.2] line 348 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_026.array_bounds.1] line 348 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_027
[overrun_st_027.pointer_dereference.1] line 360 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_027.pointer_dereference.2] line 360 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_027.array_bounds.1] line 361 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_027.array_bounds.2] line 361 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_028
[overrun_st_028.pointer_dereference.1] line 373 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_028.pointer_dereference.2] line 373 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_028.array_bounds.1] line 374 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_028.array_bounds.2] line 374 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_029
[overrun_st_029.pointer_dereference.9] line 388 dereference failure: invalid integer address in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.8] line 388 dereference failure: pointer outside object bounds in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.7] line 388 dereference failure: pointer outside dynamic object bounds in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.6] line 388 dereference failure: dead object in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.4] line 388 dereference failure: pointer invalid in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.3] line 388 dereference failure: pointer NULL in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.pointer_dereference.2] line 388 dereference failure: pointer outside object bounds in *pp: SUCCESS
[overrun_st_029.pointer_dereference.1] line 388 dereference failure: dead object in *pp: SUCCESS
[overrun_st_029.pointer_dereference.5] line 388 dereference failure: deallocated dynamic object in (*pp)[(signed long int)4]: SUCCESS
[overrun_st_029.array_bounds.1] line 389 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_029.array_bounds.2] line 389 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_030
[overrun_st_030.pointer_dereference.1] line 403 dereference failure: dead object in p2[(signed long int)4]: SUCCESS
[overrun_st_030.pointer_dereference.2] line 403 dereference failure: pointer outside object bounds in p2[(signed long int)4]: SUCCESS
[overrun_st_030.array_bounds.1] line 404 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_030.array_bounds.2] line 404 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_031
[overrun_st_031.pointer_dereference.1] line 416 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS

overrun_st.c function overrun_st_032
[overrun_st_032.pointer_dereference.2] line 429 dereference failure: pointer outside object bounds in p[(signed long int)index]: SUCCESS
[overrun_st_032.pointer_dereference.1] line 429 dereference failure: dead object in p[(signed long int)index]: SUCCESS

overrun_st.c function overrun_st_033
[overrun_st_033.pointer_dereference.1] line 443 dereference failure: dead object in p[(signed long int)index]: SUCCESS
[overrun_st_033.pointer_dereference.2] line 443 dereference failure: pointer outside object bounds in p[(signed long int)index]: SUCCESS

overrun_st.c function overrun_st_034
[overrun_st_034.pointer_dereference.1] line 456 dereference failure: dead object in p[(signed long int)(2 * index + 1)]: SUCCESS
[overrun_st_034.pointer_dereference.2] line 456 dereference failure: pointer outside object bounds in p[(signed long int)(2 * index + 1)]: SUCCESS

overrun_st.c function overrun_st_035
[overrun_st_035.pointer_dereference.1] line 469 dereference failure: dead object in p[(signed long int)(index * index + 1)]: SUCCESS
[overrun_st_035.pointer_dereference.2] line 469 dereference failure: pointer outside object bounds in p[(signed long int)(index * index + 1)]: SUCCESS

overrun_st.c function overrun_st_036
[overrun_st_036.pointer_dereference.1] line 486 dereference failure: dead object in p[(signed long int)return_value_overrun_st_036_func_001]: SUCCESS
[overrun_st_036.pointer_dereference.2] line 486 dereference failure: pointer outside object bounds in p[(signed long int)return_value_overrun_st_036_func_001]: SUCCESS

overrun_st.c function overrun_st_037_func_001
[overrun_st_037_func_001.pointer_dereference.1] line 498 dereference failure: dead object in p[(signed long int)index]: SUCCESS
[overrun_st_037_func_001.pointer_dereference.2] line 498 dereference failure: pointer outside object bounds in p[(signed long int)index]: SUCCESS

overrun_st.c function overrun_st_038
[overrun_st_038.array_bounds.1] line 517 array `indexes' lower bound in indexes[(signed long int)index]: SUCCESS
[overrun_st_038.pointer_dereference.2] line 517 dereference failure: pointer outside object bounds in p[(signed long int)indexes[(signed long int)index]]: SUCCESS
[overrun_st_038.array_bounds.2] line 517 array `indexes' upper bound in indexes[(signed long int)index]: SUCCESS
[overrun_st_038.pointer_dereference.1] line 517 dereference failure: dead object in p[(signed long int)indexes[(signed long int)index]]: SUCCESS

overrun_st.c function overrun_st_039
[overrun_st_039.pointer_dereference.1] line 532 dereference failure: dead object in p[(signed long int)index1]: SUCCESS
[overrun_st_039.pointer_dereference.2] line 532 dereference failure: pointer outside object bounds in p[(signed long int)index1]: SUCCESS

overrun_st.c function overrun_st_040
[overrun_st_040.pointer_dereference.1] line 549 dereference failure: dead object in p[(signed long int)index2]: SUCCESS
[overrun_st_040.pointer_dereference.2] line 549 dereference failure: pointer outside object bounds in p[(signed long int)index2]: SUCCESS

overrun_st.c function overrun_st_041
[overrun_st_041.array_bounds.1] line 562 array `buf' lower bound in buf[(signed long int)i]: SUCCESS
[overrun_st_041.array_bounds.2] line 562 array `buf' upper bound in buf[(signed long int)i]: SUCCESS
[overrun_st_041.array_bounds.3] line 564 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_041.array_bounds.4] line 564 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_042
[overrun_st_042.array_bounds.2] line 580 array `buf' upper bound in buf[(signed long int)i]: SUCCESS
[overrun_st_042.array_bounds.4] line 580 array `buf'[] upper bound in buf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_042.array_bounds.3] line 580 array `buf'[] lower bound in buf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_042.array_bounds.1] line 580 array `buf' lower bound in buf[(signed long int)i]: SUCCESS
[overrun_st_042.array_bounds.5] line 583 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_042.array_bounds.6] line 583 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_042.array_bounds.7] line 583 array `buf'[] lower bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_042.array_bounds.8] line 583 array `buf'[] upper bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_043
[overrun_st_043.pointer_dereference.7] line 605 dereference failure: invalid integer address in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.pointer_dereference.6] line 605 dereference failure: pointer outside object bounds in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.pointer_dereference.5] line 605 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.pointer_dereference.4] line 605 dereference failure: dead object in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.pointer_dereference.2] line 605 dereference failure: pointer invalid in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.pointer_dereference.1] line 605 dereference failure: pointer NULL in pbuf[(signed long int)i][(signed long int)j]: SUCCESS
[overrun_st_043.array_bounds.2] line 605 array `pbuf' upper bound in pbuf[(signed long int)i]: SUCCESS
[overrun_st_043.array_bounds.1] line 605 array `pbuf' lower bound in pbuf[(signed long int)i]: SUCCESS
[overrun_st_043.pointer_dereference.3] line 605 dereference failure: deallocated dynamic object in pbuf[(signed long int)i][(signed long int)j]: SUCCESS

overrun_st.c function overrun_st_044
[overrun_st_044.pointer_dereference.1] line 622 dereference failure: dead object in *p: SUCCESS
[overrun_st_044.pointer_dereference.2] line 622 dereference failure: pointer outside object bounds in *p: SUCCESS

overrun_st.c function overrun_st_045_func_001
[overrun_st_045_func_001.pointer_dereference.6] line 633 dereference failure: pointer outside object bounds in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.7] line 633 dereference failure: invalid integer address in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.5] line 633 dereference failure: pointer outside dynamic object bounds in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.4] line 633 dereference failure: dead object in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.3] line 633 dereference failure: deallocated dynamic object in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.2] line 633 dereference failure: pointer invalid in buf[(signed long int)4]: SUCCESS
[overrun_st_045_func_001.pointer_dereference.1] line 633 dereference failure: pointer NULL in buf[(signed long int)4]: SUCCESS

overrun_st.c function overrun_st_046_func_001
[overrun_st_046_func_001.pointer_dereference.1] line 648 dereference failure: pointer NULL in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.2] line 648 dereference failure: pointer invalid in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.3] line 648 dereference failure: deallocated dynamic object in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.4] line 648 dereference failure: dead object in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.5] line 648 dereference failure: pointer outside dynamic object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.6] line 648 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_046_func_001.pointer_dereference.7] line 648 dereference failure: invalid integer address in p[(signed long int)4]: SUCCESS

overrun_st.c function overrun_st_047_func_001
[overrun_st_047_func_001.pointer_dereference.7] line 663 dereference failure: invalid integer address in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.6] line 663 dereference failure: pointer outside object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.5] line 663 dereference failure: pointer outside dynamic object bounds in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.3] line 663 dereference failure: deallocated dynamic object in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.2] line 663 dereference failure: pointer invalid in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.1] line 663 dereference failure: pointer NULL in p[(signed long int)4]: SUCCESS
[overrun_st_047_func_001.pointer_dereference.4] line 663 dereference failure: dead object in p[(signed long int)4]: SUCCESS

overrun_st.c function overrun_st_048_func_001
[overrun_st_048_func_001.pointer_dereference.1] line 678 dereference failure: pointer NULL in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.2] line 678 dereference failure: pointer invalid in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.3] line 678 dereference failure: deallocated dynamic object in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.4] line 678 dereference failure: dead object in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.5] line 678 dereference failure: pointer outside dynamic object bounds in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.6] line 678 dereference failure: pointer outside object bounds in buf[(signed long int)4]: SUCCESS
[overrun_st_048_func_001.pointer_dereference.7] line 678 dereference failure: invalid integer address in buf[(signed long int)4]: SUCCESS

overrun_st.c function overrun_st_049
[overrun_st_049.array_bounds.1] line 695 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_049.array_bounds.2] line 695 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_050
[overrun_st_050.array_bounds.3] line 713 array `buf'[] lower bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_050.array_bounds.4] line 713 array `buf'[] upper bound in buf[(signed long int)idx][(signed long int)idx]: SUCCESS
[overrun_st_050.array_bounds.2] line 713 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_050.array_bounds.1] line 713 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_051
[overrun_st_051.pointer_dereference.1] line 728 dereference failure: pointer NULL in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.2] line 728 dereference failure: pointer invalid in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.3] line 728 dereference failure: deallocated dynamic object in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.4] line 728 dereference failure: dead object in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.5] line 728 dereference failure: pointer outside dynamic object bounds in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.6] line 728 dereference failure: pointer outside object bounds in pbuf[(signed long int)4][(signed long int)5]: SUCCESS
[overrun_st_051.pointer_dereference.7] line 728 dereference failure: invalid integer address in pbuf[(signed long int)4][(signed long int)5]: SUCCESS

overrun_st.c function overrun_st_052
[overrun_st_052.array_bounds.1] line 739 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_052.array_bounds.2] line 739 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_053
[overrun_st_053.pointer_dereference.1] line 751 dereference failure: dead object in p[(signed long int)1]: SUCCESS
[overrun_st_053.pointer_dereference.2] line 751 dereference failure: pointer outside object bounds in p[(signed long int)1]: SUCCESS
[overrun_st_053.array_bounds.1] line 752 array `buf' lower bound in buf[(signed long int)idx]: SUCCESS
[overrun_st_053.array_bounds.2] line 752 array `buf' upper bound in buf[(signed long int)idx]: SUCCESS

overrun_st.c function overrun_st_054
[overrun_st_054.pointer_dereference.1] line 764 dereference failure: pointer outside object bounds in p[(signed long int)3]: SUCCESS

** 2 of 204 failed (3 iterations)
VERIFICATION FAILED
