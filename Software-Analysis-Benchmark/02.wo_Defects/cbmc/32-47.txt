CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing overrun_st.c
Converting
Type-checking overrun_st
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 45 steps
simple slicing removed 0 assignments
Generated 7 VCC(s), 0 remaining after simplification
VERIFICATION SUCCESSFUL
