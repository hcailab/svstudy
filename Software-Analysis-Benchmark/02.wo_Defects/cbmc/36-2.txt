CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing race_condition.c
Converting
Type-checking race_condition
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
file <builtin-library-pthread_create> line 39 function pthread_create: replacing function pointer by 10 possible targets
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
