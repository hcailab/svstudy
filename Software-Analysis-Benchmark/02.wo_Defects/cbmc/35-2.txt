CBMC version 5.11 (cbmc-5.11) 64-bit x86_64 linux
Parsing ptr_subtraction.c
Converting
Type-checking ptr_subtraction
Generating GOTO Program
Adding CPROVER library (x86_64)
Removal of function pointers and virtual functions
Generic Property Instrumentation
Running with 8 object bits, 56 offset bits (default)
Starting Bounded Model Checking
size of program expression: 39 steps
simple slicing removed 0 assignments
Generated 2 VCC(s), 0 remaining after simplification
VERIFICATION SUCCESSFUL
