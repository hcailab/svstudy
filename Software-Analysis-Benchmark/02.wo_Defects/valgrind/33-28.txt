==44210== Memcheck, a memory error detector
==44210== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44210== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44210== Command: ./02_wo_Defects 33028
==44210== 
==44210== 
==44210== HEAP SUMMARY:
==44210==     in use at exit: 0 bytes in 0 blocks
==44210==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44210== 
==44210== All heap blocks were freed -- no leaks are possible
==44210== 
==44210== For counts of detected and suppressed errors, rerun with: -v
==44210== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
