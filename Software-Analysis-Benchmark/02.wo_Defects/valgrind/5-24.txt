==31830== Memcheck, a memory error detector
==31830== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31830== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31830== Command: ./02_wo_Defects 5024
==31830== 
==31830== 
==31830== HEAP SUMMARY:
==31830==     in use at exit: 0 bytes in 0 blocks
==31830==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31830== 
==31830== All heap blocks were freed -- no leaks are possible
==31830== 
==31830== For counts of detected and suppressed errors, rerun with: -v
==31830== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
