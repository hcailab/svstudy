==51169== Memcheck, a memory error detector
==51169== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51169== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51169== Command: ./02_wo_Defects 47045
==51169== 
==51169== 
==51169== HEAP SUMMARY:
==51169==     in use at exit: 0 bytes in 0 blocks
==51169==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51169== 
==51169== All heap blocks were freed -- no leaks are possible
==51169== 
==51169== For counts of detected and suppressed errors, rerun with: -v
==51169== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
