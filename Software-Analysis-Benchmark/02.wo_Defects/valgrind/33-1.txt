==44035== Memcheck, a memory error detector
==44035== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44035== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44035== Command: ./02_wo_Defects 33001
==44035== 
==44035== 
==44035== HEAP SUMMARY:
==44035==     in use at exit: 0 bytes in 0 blocks
==44035==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44035== 
==44035== All heap blocks were freed -- no leaks are possible
==44035== 
==44035== For counts of detected and suppressed errors, rerun with: -v
==44035== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
