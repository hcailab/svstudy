==31726== Memcheck, a memory error detector
==31726== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31726== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31726== Command: ./02_wo_Defects 5010
==31726== 
==31726== 
==31726== HEAP SUMMARY:
==31726==     in use at exit: 0 bytes in 0 blocks
==31726==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31726== 
==31726== All heap blocks were freed -- no leaks are possible
==31726== 
==31726== For counts of detected and suppressed errors, rerun with: -v
==31726== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
