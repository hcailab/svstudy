==37457== Memcheck, a memory error detector
==37457== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37457== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37457== Command: ./02_wo_Defects 19006
==37457== 
==37457== 
==37457== HEAP SUMMARY:
==37457==     in use at exit: 0 bytes in 0 blocks
==37457==   total heap usage: 11 allocs, 11 frees, 1,664 bytes allocated
==37457== 
==37457== All heap blocks were freed -- no leaks are possible
==37457== 
==37457== For counts of detected and suppressed errors, rerun with: -v
==37457== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
