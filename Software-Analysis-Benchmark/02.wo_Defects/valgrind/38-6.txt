==46335== Memcheck, a memory error detector
==46335== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46335== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46335== Command: ./02_wo_Defects 38006
==46335== 
==46335== 
==46335== HEAP SUMMARY:
==46335==     in use at exit: 0 bytes in 0 blocks
==46335==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46335== 
==46335== All heap blocks were freed -- no leaks are possible
==46335== 
==46335== For counts of detected and suppressed errors, rerun with: -v
==46335== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
