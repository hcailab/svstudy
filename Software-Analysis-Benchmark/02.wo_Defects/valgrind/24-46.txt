==39815== Memcheck, a memory error detector
==39815== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39815== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39815== Command: ./02_wo_Defects 24046
==39815== 
==39815== 
==39815== HEAP SUMMARY:
==39815==     in use at exit: 0 bytes in 0 blocks
==39815==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39815== 
==39815== All heap blocks were freed -- no leaks are possible
==39815== 
==39815== For counts of detected and suppressed errors, rerun with: -v
==39815== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
