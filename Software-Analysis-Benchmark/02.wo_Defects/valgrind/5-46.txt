==31923== Memcheck, a memory error detector
==31923== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31923== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31923== Command: ./02_wo_Defects 5046
==31923== 
==31923== 
==31923== HEAP SUMMARY:
==31923==     in use at exit: 0 bytes in 0 blocks
==31923==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31923== 
==31923== All heap blocks were freed -- no leaks are possible
==31923== 
==31923== For counts of detected and suppressed errors, rerun with: -v
==31923== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
