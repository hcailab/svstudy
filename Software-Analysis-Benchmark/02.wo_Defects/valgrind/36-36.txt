==45729== Memcheck, a memory error detector
==45729== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==45729== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==45729== Command: ./02_wo_Defects 36036
==45729== 
==45729== 
==45729== HEAP SUMMARY:
==45729==     in use at exit: 0 bytes in 0 blocks
==45729==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==45729== 
==45729== All heap blocks were freed -- no leaks are possible
==45729== 
==45729== For counts of detected and suppressed errors, rerun with: -v
==45729== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
