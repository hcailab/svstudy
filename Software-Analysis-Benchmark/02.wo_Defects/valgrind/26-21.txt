==40512== Memcheck, a memory error detector
==40512== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40512== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40512== Command: ./02_wo_Defects 26021
==40512== 
==40512== 
==40512== HEAP SUMMARY:
==40512==     in use at exit: 0 bytes in 0 blocks
==40512==   total heap usage: 3 allocs, 3 frees, 1,568 bytes allocated
==40512== 
==40512== All heap blocks were freed -- no leaks are possible
==40512== 
==40512== For counts of detected and suppressed errors, rerun with: -v
==40512== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
