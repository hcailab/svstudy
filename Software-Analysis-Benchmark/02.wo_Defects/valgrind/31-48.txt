==43629== Memcheck, a memory error detector
==43629== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43629== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43629== Command: ./02_wo_Defects 31048
==43629== 
==43629== 
==43629== HEAP SUMMARY:
==43629==     in use at exit: 0 bytes in 0 blocks
==43629==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43629== 
==43629== All heap blocks were freed -- no leaks are possible
==43629== 
==43629== For counts of detected and suppressed errors, rerun with: -v
==43629== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
