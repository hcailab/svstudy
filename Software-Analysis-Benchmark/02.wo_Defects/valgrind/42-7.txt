==73769== Memcheck, a memory error detector
==73769== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73769== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73769== Command: ./02_wo_Defects 42007
==73769== 
==73769== 
==73769== HEAP SUMMARY:
==73769==     in use at exit: 0 bytes in 0 blocks
==73769==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==73769== 
==73769== All heap blocks were freed -- no leaks are possible
==73769== 
==73769== For counts of detected and suppressed errors, rerun with: -v
==73769== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
