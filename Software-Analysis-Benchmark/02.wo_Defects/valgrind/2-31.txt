==74797== Memcheck, a memory error detector
==74797== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74797== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74797== Command: ./02_wo_Defects 2031
==74797== 
==74797== 
==74797== HEAP SUMMARY:
==74797==     in use at exit: 0 bytes in 0 blocks
==74797==   total heap usage: 2 allocs, 2 frees, 1,036 bytes allocated
==74797== 
==74797== All heap blocks were freed -- no leaks are possible
==74797== 
==74797== For counts of detected and suppressed errors, rerun with: -v
==74797== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
