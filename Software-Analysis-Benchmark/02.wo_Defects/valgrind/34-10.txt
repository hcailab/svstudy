==44472== Memcheck, a memory error detector
==44472== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44472== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44472== Command: ./02_wo_Defects 34010
==44472== 
==44472== 
==44472== HEAP SUMMARY:
==44472==     in use at exit: 0 bytes in 0 blocks
==44472==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44472== 
==44472== All heap blocks were freed -- no leaks are possible
==44472== 
==44472== For counts of detected and suppressed errors, rerun with: -v
==44472== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
