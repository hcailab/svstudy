==36756== Memcheck, a memory error detector
==36756== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36756== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36756== Command: ./02_wo_Defects 17035
==36756== 
==36756== 
==36756== HEAP SUMMARY:
==36756==     in use at exit: 0 bytes in 0 blocks
==36756==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36756== 
==36756== All heap blocks were freed -- no leaks are possible
==36756== 
==36756== For counts of detected and suppressed errors, rerun with: -v
==36756== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
