==32364== Memcheck, a memory error detector
==32364== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32364== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32364== Command: ./02_wo_Defects 7031
==32364== 
==32364== 
==32364== HEAP SUMMARY:
==32364==     in use at exit: 0 bytes in 0 blocks
==32364==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32364== 
==32364== All heap blocks were freed -- no leaks are possible
==32364== 
==32364== For counts of detected and suppressed errors, rerun with: -v
==32364== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
