==48208== Memcheck, a memory error detector
==48208== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48208== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48208== Command: ./02_wo_Defects 42011
==48208== 
==48208== 
==48208== HEAP SUMMARY:
==48208==     in use at exit: 0 bytes in 0 blocks
==48208==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48208== 
==48208== All heap blocks were freed -- no leaks are possible
==48208== 
==48208== For counts of detected and suppressed errors, rerun with: -v
==48208== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
