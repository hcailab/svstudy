==46245== Memcheck, a memory error detector
==46245== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46245== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46245== Command: ./02_wo_Defects 37044
==46245== 
==46245== 
==46245== HEAP SUMMARY:
==46245==     in use at exit: 0 bytes in 0 blocks
==46245==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46245== 
==46245== All heap blocks were freed -- no leaks are possible
==46245== 
==46245== For counts of detected and suppressed errors, rerun with: -v
==46245== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
