==74172== Memcheck, a memory error detector
==74172== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74172== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74172== Command: ./02_wo_Defects 16009
==74172== 
==74172== 
==74172== HEAP SUMMARY:
==74172==     in use at exit: 0 bytes in 0 blocks
==74172==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==74172== 
==74172== All heap blocks were freed -- no leaks are possible
==74172== 
==74172== For counts of detected and suppressed errors, rerun with: -v
==74172== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
