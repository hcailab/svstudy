==52411== Memcheck, a memory error detector
==52411== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52411== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52411== Command: ./02_wo_Defects 50035
==52411== 
==52411== 
==52411== HEAP SUMMARY:
==52411==     in use at exit: 0 bytes in 0 blocks
==52411==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52411== 
==52411== All heap blocks were freed -- no leaks are possible
==52411== 
==52411== For counts of detected and suppressed errors, rerun with: -v
==52411== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
