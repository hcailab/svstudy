==39748== Memcheck, a memory error detector
==39748== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39748== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39748== Command: ./02_wo_Defects 24036
==39748== 
==39748== 
==39748== HEAP SUMMARY:
==39748==     in use at exit: 0 bytes in 0 blocks
==39748==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39748== 
==39748== All heap blocks were freed -- no leaks are possible
==39748== 
==39748== For counts of detected and suppressed errors, rerun with: -v
==39748== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
