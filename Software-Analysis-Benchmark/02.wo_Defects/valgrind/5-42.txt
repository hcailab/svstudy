==31910== Memcheck, a memory error detector
==31910== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31910== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31910== Command: ./02_wo_Defects 5042
==31910== 
==31910== 
==31910== HEAP SUMMARY:
==31910==     in use at exit: 0 bytes in 0 blocks
==31910==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31910== 
==31910== All heap blocks were freed -- no leaks are possible
==31910== 
==31910== For counts of detected and suppressed errors, rerun with: -v
==31910== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
