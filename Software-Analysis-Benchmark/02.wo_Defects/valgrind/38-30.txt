==46500== Memcheck, a memory error detector
==46500== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46500== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46500== Command: ./02_wo_Defects 38030
==46500== 
==46500== 
==46500== HEAP SUMMARY:
==46500==     in use at exit: 0 bytes in 0 blocks
==46500==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46500== 
==46500== All heap blocks were freed -- no leaks are possible
==46500== 
==46500== For counts of detected and suppressed errors, rerun with: -v
==46500== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
