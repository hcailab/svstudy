==49694== Memcheck, a memory error detector
==49694== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49694== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49694== Command: ./02_wo_Defects 44049
==49694== 
==49694== 
==49694== HEAP SUMMARY:
==49694==     in use at exit: 0 bytes in 0 blocks
==49694==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49694== 
==49694== All heap blocks were freed -- no leaks are possible
==49694== 
==49694== For counts of detected and suppressed errors, rerun with: -v
==49694== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
