==44727== Memcheck, a memory error detector
==44727== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44727== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44727== Command: ./02_wo_Defects 34040
==44727== 
==44727== 
==44727== HEAP SUMMARY:
==44727==     in use at exit: 0 bytes in 0 blocks
==44727==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44727== 
==44727== All heap blocks were freed -- no leaks are possible
==44727== 
==44727== For counts of detected and suppressed errors, rerun with: -v
==44727== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
