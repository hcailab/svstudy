==33539== Memcheck, a memory error detector
==33539== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33539== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33539== Command: ./02_wo_Defects 11015
==33539== 
==33539== 
==33539== HEAP SUMMARY:
==33539==     in use at exit: 0 bytes in 0 blocks
==33539==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33539== 
==33539== All heap blocks were freed -- no leaks are possible
==33539== 
==33539== For counts of detected and suppressed errors, rerun with: -v
==33539== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
