==35053== Memcheck, a memory error detector
==35053== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35053== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35053== Command: ./02_wo_Defects 14026
==35053== 
==35053== 
==35053== HEAP SUMMARY:
==35053==     in use at exit: 0 bytes in 0 blocks
==35053==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35053== 
==35053== All heap blocks were freed -- no leaks are possible
==35053== 
==35053== For counts of detected and suppressed errors, rerun with: -v
==35053== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
