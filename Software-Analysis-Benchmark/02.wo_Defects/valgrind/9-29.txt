==32790== Memcheck, a memory error detector
==32790== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32790== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32790== Command: ./02_wo_Defects 9029
==32790== 
==32790== 
==32790== HEAP SUMMARY:
==32790==     in use at exit: 0 bytes in 0 blocks
==32790==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32790== 
==32790== All heap blocks were freed -- no leaks are possible
==32790== 
==32790== For counts of detected and suppressed errors, rerun with: -v
==32790== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
