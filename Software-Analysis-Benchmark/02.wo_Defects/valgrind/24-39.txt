==39770== Memcheck, a memory error detector
==39770== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39770== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39770== Command: ./02_wo_Defects 24039
==39770== 
==39770== 
==39770== HEAP SUMMARY:
==39770==     in use at exit: 0 bytes in 0 blocks
==39770==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39770== 
==39770== All heap blocks were freed -- no leaks are possible
==39770== 
==39770== For counts of detected and suppressed errors, rerun with: -v
==39770== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
