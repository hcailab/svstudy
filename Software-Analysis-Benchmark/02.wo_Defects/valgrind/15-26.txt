==35619== Memcheck, a memory error detector
==35619== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35619== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35619== Command: ./02_wo_Defects 15026
==35619== 
==35619== 
==35619== HEAP SUMMARY:
==35619==     in use at exit: 0 bytes in 0 blocks
==35619==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35619== 
==35619== All heap blocks were freed -- no leaks are possible
==35619== 
==35619== For counts of detected and suppressed errors, rerun with: -v
==35619== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
