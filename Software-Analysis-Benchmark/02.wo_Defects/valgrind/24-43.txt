==39796== Memcheck, a memory error detector
==39796== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39796== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39796== Command: ./02_wo_Defects 24043
==39796== 
==39796== 
==39796== HEAP SUMMARY:
==39796==     in use at exit: 0 bytes in 0 blocks
==39796==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39796== 
==39796== All heap blocks were freed -- no leaks are possible
==39796== 
==39796== For counts of detected and suppressed errors, rerun with: -v
==39796== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
