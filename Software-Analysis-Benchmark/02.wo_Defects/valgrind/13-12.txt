==34400== Memcheck, a memory error detector
==34400== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34400== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34400== Command: ./02_wo_Defects 13012
==34400== 
==34400== 
==34400== HEAP SUMMARY:
==34400==     in use at exit: 0 bytes in 0 blocks
==34400==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34400== 
==34400== All heap blocks were freed -- no leaks are possible
==34400== 
==34400== For counts of detected and suppressed errors, rerun with: -v
==34400== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
