==52110== Memcheck, a memory error detector
==52110== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52110== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52110== Command: ./02_wo_Defects 49048
==52110== 
==52110== 
==52110== HEAP SUMMARY:
==52110==     in use at exit: 0 bytes in 0 blocks
==52110==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52110== 
==52110== All heap blocks were freed -- no leaks are possible
==52110== 
==52110== For counts of detected and suppressed errors, rerun with: -v
==52110== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
