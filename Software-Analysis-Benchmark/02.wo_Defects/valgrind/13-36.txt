==34651== Memcheck, a memory error detector
==34651== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34651== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34651== Command: ./02_wo_Defects 13036
==34651== 
==34651== 
==34651== HEAP SUMMARY:
==34651==     in use at exit: 0 bytes in 0 blocks
==34651==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34651== 
==34651== All heap blocks were freed -- no leaks are possible
==34651== 
==34651== For counts of detected and suppressed errors, rerun with: -v
==34651== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
