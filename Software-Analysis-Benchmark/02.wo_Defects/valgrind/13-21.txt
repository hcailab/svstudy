==34488== Memcheck, a memory error detector
==34488== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34488== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34488== Command: ./02_wo_Defects 13021
==34488== 
==34488== 
==34488== HEAP SUMMARY:
==34488==     in use at exit: 0 bytes in 0 blocks
==34488==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34488== 
==34488== All heap blocks were freed -- no leaks are possible
==34488== 
==34488== For counts of detected and suppressed errors, rerun with: -v
==34488== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
