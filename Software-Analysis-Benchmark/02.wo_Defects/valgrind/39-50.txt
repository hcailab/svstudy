==47070== Memcheck, a memory error detector
==47070== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47070== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47070== Command: ./02_wo_Defects 39050
==47070== 
==47070== 
==47070== HEAP SUMMARY:
==47070==     in use at exit: 0 bytes in 0 blocks
==47070==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47070== 
==47070== All heap blocks were freed -- no leaks are possible
==47070== 
==47070== For counts of detected and suppressed errors, rerun with: -v
==47070== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
