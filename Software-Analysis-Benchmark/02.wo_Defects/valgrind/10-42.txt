==33312== Memcheck, a memory error detector
==33312== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33312== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33312== Command: ./02_wo_Defects 10042
==33312== 
==33312== 
==33312== HEAP SUMMARY:
==33312==     in use at exit: 0 bytes in 0 blocks
==33312==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33312== 
==33312== All heap blocks were freed -- no leaks are possible
==33312== 
==33312== For counts of detected and suppressed errors, rerun with: -v
==33312== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
