==51730== Memcheck, a memory error detector
==51730== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51730== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51730== Command: ./02_wo_Defects 49010
==51730== 
==51730== 
==51730== HEAP SUMMARY:
==51730==     in use at exit: 0 bytes in 0 blocks
==51730==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51730== 
==51730== All heap blocks were freed -- no leaks are possible
==51730== 
==51730== For counts of detected and suppressed errors, rerun with: -v
==51730== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
