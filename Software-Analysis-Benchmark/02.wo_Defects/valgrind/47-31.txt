==51030== Memcheck, a memory error detector
==51030== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51030== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51030== Command: ./02_wo_Defects 47031
==51030== 
==51030== 
==51030== HEAP SUMMARY:
==51030==     in use at exit: 0 bytes in 0 blocks
==51030==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51030== 
==51030== All heap blocks were freed -- no leaks are possible
==51030== 
==51030== For counts of detected and suppressed errors, rerun with: -v
==51030== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
