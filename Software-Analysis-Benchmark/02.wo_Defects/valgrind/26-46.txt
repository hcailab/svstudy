==40864== Memcheck, a memory error detector
==40864== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40864== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40864== Command: ./02_wo_Defects 26046
==40864== 
==40864== 
==40864== HEAP SUMMARY:
==40864==     in use at exit: 0 bytes in 0 blocks
==40864==   total heap usage: 3 allocs, 3 frees, 1,568 bytes allocated
==40864== 
==40864== All heap blocks were freed -- no leaks are possible
==40864== 
==40864== For counts of detected and suppressed errors, rerun with: -v
==40864== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
