==52051== Memcheck, a memory error detector
==52051== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52051== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52051== Command: ./02_wo_Defects 49040
==52051== 
==52051== 
==52051== HEAP SUMMARY:
==52051==     in use at exit: 0 bytes in 0 blocks
==52051==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52051== 
==52051== All heap blocks were freed -- no leaks are possible
==52051== 
==52051== For counts of detected and suppressed errors, rerun with: -v
==52051== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
