==35009== Memcheck, a memory error detector
==35009== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35009== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35009== Command: ./02_wo_Defects 14021
==35009== 
==35009== 
==35009== HEAP SUMMARY:
==35009==     in use at exit: 0 bytes in 0 blocks
==35009==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35009== 
==35009== All heap blocks were freed -- no leaks are possible
==35009== 
==35009== For counts of detected and suppressed errors, rerun with: -v
==35009== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
