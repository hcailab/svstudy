==31565== Memcheck, a memory error detector
==31565== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31565== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31565== Command: ./02_wo_Defects 5003
==31565== 
==31565== 
==31565== HEAP SUMMARY:
==31565==     in use at exit: 0 bytes in 0 blocks
==31565==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31565== 
==31565== All heap blocks were freed -- no leaks are possible
==31565== 
==31565== For counts of detected and suppressed errors, rerun with: -v
==31565== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
