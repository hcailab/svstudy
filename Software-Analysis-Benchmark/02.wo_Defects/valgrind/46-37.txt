==50532== Memcheck, a memory error detector
==50532== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50532== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50532== Command: ./02_wo_Defects 46037
==50532== 
==50532== 
==50532== HEAP SUMMARY:
==50532==     in use at exit: 0 bytes in 0 blocks
==50532==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50532== 
==50532== All heap blocks were freed -- no leaks are possible
==50532== 
==50532== For counts of detected and suppressed errors, rerun with: -v
==50532== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
