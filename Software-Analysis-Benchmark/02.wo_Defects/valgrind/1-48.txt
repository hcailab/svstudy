==30861== Memcheck, a memory error detector
==30861== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30861== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30861== Command: ./02_wo_Defects 1048
==30861== 
==30861== 
==30861== HEAP SUMMARY:
==30861==     in use at exit: 0 bytes in 0 blocks
==30861==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30861== 
==30861== All heap blocks were freed -- no leaks are possible
==30861== 
==30861== For counts of detected and suppressed errors, rerun with: -v
==30861== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
