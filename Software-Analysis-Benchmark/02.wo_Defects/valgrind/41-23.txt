==47829== Memcheck, a memory error detector
==47829== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47829== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47829== Command: ./02_wo_Defects 41023
==47829== 
==47829== 
==47829== HEAP SUMMARY:
==47829==     in use at exit: 0 bytes in 0 blocks
==47829==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47829== 
==47829== All heap blocks were freed -- no leaks are possible
==47829== 
==47829== For counts of detected and suppressed errors, rerun with: -v
==47829== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
