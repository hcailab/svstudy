==43640== Memcheck, a memory error detector
==43640== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43640== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43640== Command: ./02_wo_Defects 31049
==43640== 
==43640== 
==43640== HEAP SUMMARY:
==43640==     in use at exit: 0 bytes in 0 blocks
==43640==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43640== 
==43640== All heap blocks were freed -- no leaks are possible
==43640== 
==43640== For counts of detected and suppressed errors, rerun with: -v
==43640== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
