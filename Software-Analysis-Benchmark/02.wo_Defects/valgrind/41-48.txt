==48075== Memcheck, a memory error detector
==48075== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48075== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48075== Command: ./02_wo_Defects 41048
==48075== 
==48075== 
==48075== HEAP SUMMARY:
==48075==     in use at exit: 0 bytes in 0 blocks
==48075==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48075== 
==48075== All heap blocks were freed -- no leaks are possible
==48075== 
==48075== For counts of detected and suppressed errors, rerun with: -v
==48075== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
