==50950== Memcheck, a memory error detector
==50950== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50950== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50950== Command: ./02_wo_Defects 47023
==50950== 
==50950== 
==50950== HEAP SUMMARY:
==50950==     in use at exit: 0 bytes in 0 blocks
==50950==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50950== 
==50950== All heap blocks were freed -- no leaks are possible
==50950== 
==50950== For counts of detected and suppressed errors, rerun with: -v
==50950== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
