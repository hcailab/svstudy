==32086== Memcheck, a memory error detector
==32086== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32086== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32086== Command: ./02_wo_Defects 6024
==32086== 
==32086== 
==32086== HEAP SUMMARY:
==32086==     in use at exit: 0 bytes in 0 blocks
==32086==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32086== 
==32086== All heap blocks were freed -- no leaks are possible
==32086== 
==32086== For counts of detected and suppressed errors, rerun with: -v
==32086== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
