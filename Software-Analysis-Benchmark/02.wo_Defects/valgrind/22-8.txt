==38758== Memcheck, a memory error detector
==38758== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38758== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38758== Command: ./02_wo_Defects 22008
==38758== 
==38758== 
==38758== HEAP SUMMARY:
==38758==     in use at exit: 0 bytes in 0 blocks
==38758==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38758== 
==38758== All heap blocks were freed -- no leaks are possible
==38758== 
==38758== For counts of detected and suppressed errors, rerun with: -v
==38758== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
