==43625== Memcheck, a memory error detector
==43625== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43625== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43625== Command: ./02_wo_Defects 31047
==43625== 
==43625== 
==43625== HEAP SUMMARY:
==43625==     in use at exit: 0 bytes in 0 blocks
==43625==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43625== 
==43625== All heap blocks were freed -- no leaks are possible
==43625== 
==43625== For counts of detected and suppressed errors, rerun with: -v
==43625== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
