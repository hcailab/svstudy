==32768== Memcheck, a memory error detector
==32768== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32768== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32768== Command: ./02_wo_Defects 9023
==32768== 
==32768== 
==32768== HEAP SUMMARY:
==32768==     in use at exit: 0 bytes in 0 blocks
==32768==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32768== 
==32768== All heap blocks were freed -- no leaks are possible
==32768== 
==32768== For counts of detected and suppressed errors, rerun with: -v
==32768== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
