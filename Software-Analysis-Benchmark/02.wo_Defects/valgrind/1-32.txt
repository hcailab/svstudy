==30733== Memcheck, a memory error detector
==30733== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30733== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30733== Command: ./02_wo_Defects 1032
==30733== 
==30733== 
==30733== HEAP SUMMARY:
==30733==     in use at exit: 0 bytes in 0 blocks
==30733==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30733== 
==30733== All heap blocks were freed -- no leaks are possible
==30733== 
==30733== For counts of detected and suppressed errors, rerun with: -v
==30733== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
