==35344== Memcheck, a memory error detector
==35344== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35344== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35344== Command: ./02_wo_Defects 14050
==35344== 
==35344== 
==35344== HEAP SUMMARY:
==35344==     in use at exit: 0 bytes in 0 blocks
==35344==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35344== 
==35344== All heap blocks were freed -- no leaks are possible
==35344== 
==35344== For counts of detected and suppressed errors, rerun with: -v
==35344== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
