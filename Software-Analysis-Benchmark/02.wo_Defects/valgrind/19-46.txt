==37872== Memcheck, a memory error detector
==37872== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37872== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37872== Command: ./02_wo_Defects 19046
==37872== 
==37872== 
==37872== HEAP SUMMARY:
==37872==     in use at exit: 0 bytes in 0 blocks
==37872==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37872== 
==37872== All heap blocks were freed -- no leaks are possible
==37872== 
==37872== For counts of detected and suppressed errors, rerun with: -v
==37872== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
