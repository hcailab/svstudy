==50592== Memcheck, a memory error detector
==50592== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50592== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50592== Command: ./02_wo_Defects 46043
==50592== 
==50592== 
==50592== HEAP SUMMARY:
==50592==     in use at exit: 0 bytes in 0 blocks
==50592==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50592== 
==50592== All heap blocks were freed -- no leaks are possible
==50592== 
==50592== For counts of detected and suppressed errors, rerun with: -v
==50592== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
