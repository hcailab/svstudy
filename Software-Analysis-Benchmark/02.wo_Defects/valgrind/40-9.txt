==47154== Memcheck, a memory error detector
==47154== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47154== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47154== Command: ./02_wo_Defects 40009
==47154== 
==47154== 
==47154== HEAP SUMMARY:
==47154==     in use at exit: 0 bytes in 0 blocks
==47154==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47154== 
==47154== All heap blocks were freed -- no leaks are possible
==47154== 
==47154== For counts of detected and suppressed errors, rerun with: -v
==47154== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
