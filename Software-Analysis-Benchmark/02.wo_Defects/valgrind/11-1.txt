==33387== Memcheck, a memory error detector
==33387== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33387== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33387== Command: ./02_wo_Defects 11001
==33387== 
==33387== 
==33387== HEAP SUMMARY:
==33387==     in use at exit: 0 bytes in 0 blocks
==33387==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33387== 
==33387== All heap blocks were freed -- no leaks are possible
==33387== 
==33387== For counts of detected and suppressed errors, rerun with: -v
==33387== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
