==37166== Memcheck, a memory error detector
==37166== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37166== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37166== Command: ./02_wo_Defects 18027
==37166== 
==37166== 
==37166== HEAP SUMMARY:
==37166==     in use at exit: 0 bytes in 0 blocks
==37166==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37166== 
==37166== All heap blocks were freed -- no leaks are possible
==37166== 
==37166== For counts of detected and suppressed errors, rerun with: -v
==37166== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
