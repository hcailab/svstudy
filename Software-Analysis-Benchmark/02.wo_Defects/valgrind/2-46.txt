==31050== Memcheck, a memory error detector
==31050== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31050== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31050== Command: ./02_wo_Defects 2046
==31050== 
==31050== 
==31050== HEAP SUMMARY:
==31050==     in use at exit: 0 bytes in 0 blocks
==31050==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31050== 
==31050== All heap blocks were freed -- no leaks are possible
==31050== 
==31050== For counts of detected and suppressed errors, rerun with: -v
==31050== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
