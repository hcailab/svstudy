==43202== Memcheck, a memory error detector
==43202== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43202== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43202== Command: ./02_wo_Defects 31007
==43202== 
==43202== 
==43202== HEAP SUMMARY:
==43202==     in use at exit: 0 bytes in 0 blocks
==43202==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43202== 
==43202== All heap blocks were freed -- no leaks are possible
==43202== 
==43202== For counts of detected and suppressed errors, rerun with: -v
==43202== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
