==47793== Memcheck, a memory error detector
==47793== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47793== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47793== Command: ./02_wo_Defects 41020
==47793== 
==47793== 
==47793== HEAP SUMMARY:
==47793==     in use at exit: 0 bytes in 0 blocks
==47793==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47793== 
==47793== All heap blocks were freed -- no leaks are possible
==47793== 
==47793== For counts of detected and suppressed errors, rerun with: -v
==47793== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
