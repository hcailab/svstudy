==49770== Memcheck, a memory error detector
==49770== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49770== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49770== Command: ./02_wo_Defects 45006
==49770== 
==49770== 
==49770== HEAP SUMMARY:
==49770==     in use at exit: 25 bytes in 1 blocks
==49770==   total heap usage: 2 allocs, 1 frees, 1,049 bytes allocated
==49770== 
==49770== 25 bytes in 1 blocks are definitely lost in loss record 1 of 1
==49770==    at 0x4C31B25: calloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==49770==    by 0x118752: uninit_memory_access_006 (uninit_memory_access.c:126)
==49770==    by 0x118E46: uninit_memory_access_main (uninit_memory_access.c:509)
==49770==    by 0x109A20: main (main.c:293)
==49770== 
==49770== LEAK SUMMARY:
==49770==    definitely lost: 25 bytes in 1 blocks
==49770==    indirectly lost: 0 bytes in 0 blocks
==49770==      possibly lost: 0 bytes in 0 blocks
==49770==    still reachable: 0 bytes in 0 blocks
==49770==         suppressed: 0 bytes in 0 blocks
==49770== 
==49770== For counts of detected and suppressed errors, rerun with: -v
==49770== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
