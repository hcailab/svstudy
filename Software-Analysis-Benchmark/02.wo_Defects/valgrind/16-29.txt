==36175== Memcheck, a memory error detector
==36175== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36175== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36175== Command: ./02_wo_Defects 16029
==36175== 
==36175== 
==36175== HEAP SUMMARY:
==36175==     in use at exit: 0 bytes in 0 blocks
==36175==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36175== 
==36175== All heap blocks were freed -- no leaks are possible
==36175== 
==36175== For counts of detected and suppressed errors, rerun with: -v
==36175== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
