==33254== Memcheck, a memory error detector
==33254== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33254== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33254== Command: ./02_wo_Defects 10035
==33254== 
==33254== 
==33254== HEAP SUMMARY:
==33254==     in use at exit: 0 bytes in 0 blocks
==33254==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33254== 
==33254== All heap blocks were freed -- no leaks are possible
==33254== 
==33254== For counts of detected and suppressed errors, rerun with: -v
==33254== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
