==44346== Memcheck, a memory error detector
==44346== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44346== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44346== Command: ./02_wo_Defects 33047
==44346== 
==44346== 
==44346== HEAP SUMMARY:
==44346==     in use at exit: 0 bytes in 0 blocks
==44346==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44346== 
==44346== All heap blocks were freed -- no leaks are possible
==44346== 
==44346== For counts of detected and suppressed errors, rerun with: -v
==44346== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
