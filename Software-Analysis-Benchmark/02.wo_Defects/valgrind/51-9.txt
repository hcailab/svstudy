==52611== Memcheck, a memory error detector
==52611== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52611== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52611== Command: ./02_wo_Defects 51009
==52611== 
==52611== 
==52611== HEAP SUMMARY:
==52611==     in use at exit: 0 bytes in 0 blocks
==52611==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52611== 
==52611== All heap blocks were freed -- no leaks are possible
==52611== 
==52611== For counts of detected and suppressed errors, rerun with: -v
==52611== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
