==43324== Memcheck, a memory error detector
==43324== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43324== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43324== Command: ./02_wo_Defects 31017
==43324== 
==43324== 
==43324== HEAP SUMMARY:
==43324==     in use at exit: 0 bytes in 0 blocks
==43324==   total heap usage: 7 allocs, 7 frees, 1,139 bytes allocated
==43324== 
==43324== All heap blocks were freed -- no leaks are possible
==43324== 
==43324== For counts of detected and suppressed errors, rerun with: -v
==43324== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
