==39362== Memcheck, a memory error detector
==39362== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39362== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39362== Command: ./02_wo_Defects 23040
==39362== 
==39362== 
==39362== HEAP SUMMARY:
==39362==     in use at exit: 0 bytes in 0 blocks
==39362==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39362== 
==39362== All heap blocks were freed -- no leaks are possible
==39362== 
==39362== For counts of detected and suppressed errors, rerun with: -v
==39362== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
