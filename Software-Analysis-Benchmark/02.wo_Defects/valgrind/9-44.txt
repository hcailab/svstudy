==32854== Memcheck, a memory error detector
==32854== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32854== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32854== Command: ./02_wo_Defects 9044
==32854== 
==32854== 
==32854== HEAP SUMMARY:
==32854==     in use at exit: 0 bytes in 0 blocks
==32854==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32854== 
==32854== All heap blocks were freed -- no leaks are possible
==32854== 
==32854== For counts of detected and suppressed errors, rerun with: -v
==32854== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
