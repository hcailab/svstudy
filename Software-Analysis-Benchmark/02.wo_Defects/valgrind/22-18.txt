==38831== Memcheck, a memory error detector
==38831== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38831== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38831== Command: ./02_wo_Defects 22018
==38831== 
==38831== 
==38831== HEAP SUMMARY:
==38831==     in use at exit: 0 bytes in 0 blocks
==38831==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38831== 
==38831== All heap blocks were freed -- no leaks are possible
==38831== 
==38831== For counts of detected and suppressed errors, rerun with: -v
==38831== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
