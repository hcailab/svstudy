==38062== Memcheck, a memory error detector
==38062== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38062== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38062== Command: ./02_wo_Defects 20024
==38062== 
==38062== 
==38062== HEAP SUMMARY:
==38062==     in use at exit: 0 bytes in 0 blocks
==38062==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38062== 
==38062== All heap blocks were freed -- no leaks are possible
==38062== 
==38062== For counts of detected and suppressed errors, rerun with: -v
==38062== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
