==48444== Memcheck, a memory error detector
==48444== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48444== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48444== Command: ./02_wo_Defects 42030
==48444== 
==48444== 
==48444== HEAP SUMMARY:
==48444==     in use at exit: 0 bytes in 0 blocks
==48444==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48444== 
==48444== All heap blocks were freed -- no leaks are possible
==48444== 
==48444== For counts of detected and suppressed errors, rerun with: -v
==48444== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
