==36937== Memcheck, a memory error detector
==36937== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36937== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36937== Command: ./02_wo_Defects 18004
==36937== 
==36937== 
==36937== HEAP SUMMARY:
==36937==     in use at exit: 31 bytes in 2 blocks
==36937==   total heap usage: 3 allocs, 1 frees, 1,055 bytes allocated
==36937== 
==36937== 17 bytes in 1 blocks are definitely lost in loss record 2 of 2
==36937==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==36937==    by 0x116879: func_pointer_004_func_001 (func_pointer.c:104)
==36937==    by 0x116879: func_pointer_004 (func_pointer.c:132)
==36937==    by 0x1171FF: func_pointer_main (func_pointer.c:660)
==36937==    by 0x10994E: main (main.c:131)
==36937== 
==36937== LEAK SUMMARY:
==36937==    definitely lost: 17 bytes in 1 blocks
==36937==    indirectly lost: 0 bytes in 0 blocks
==36937==      possibly lost: 0 bytes in 0 blocks
==36937==    still reachable: 14 bytes in 1 blocks
==36937==         suppressed: 0 bytes in 0 blocks
==36937== Reachable blocks (those to which a pointer was found) are not shown.
==36937== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==36937== 
==36937== For counts of detected and suppressed errors, rerun with: -v
==36937== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
