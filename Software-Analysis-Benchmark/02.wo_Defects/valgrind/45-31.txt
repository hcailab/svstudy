==49978== Memcheck, a memory error detector
==49978== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49978== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49978== Command: ./02_wo_Defects 45031
==49978== 
==49978== 
==49978== HEAP SUMMARY:
==49978==     in use at exit: 0 bytes in 0 blocks
==49978==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49978== 
==49978== All heap blocks were freed -- no leaks are possible
==49978== 
==49978== For counts of detected and suppressed errors, rerun with: -v
==49978== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
