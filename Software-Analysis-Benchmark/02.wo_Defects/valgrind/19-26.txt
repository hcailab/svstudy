==37650== Memcheck, a memory error detector
==37650== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37650== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37650== Command: ./02_wo_Defects 19026
==37650== 
==37650== 
==37650== HEAP SUMMARY:
==37650==     in use at exit: 0 bytes in 0 blocks
==37650==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37650== 
==37650== All heap blocks were freed -- no leaks are possible
==37650== 
==37650== For counts of detected and suppressed errors, rerun with: -v
==37650== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
