==39419== Memcheck, a memory error detector
==39419== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39419== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39419== Command: ./02_wo_Defects 23048
==39419== 
==39419== 
==39419== HEAP SUMMARY:
==39419==     in use at exit: 0 bytes in 0 blocks
==39419==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39419== 
==39419== All heap blocks were freed -- no leaks are possible
==39419== 
==39419== For counts of detected and suppressed errors, rerun with: -v
==39419== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
