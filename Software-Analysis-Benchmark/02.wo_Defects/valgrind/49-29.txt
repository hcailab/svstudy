==51954== Memcheck, a memory error detector
==51954== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51954== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51954== Command: ./02_wo_Defects 49029
==51954== 
==51954== 
==51954== HEAP SUMMARY:
==51954==     in use at exit: 0 bytes in 0 blocks
==51954==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51954== 
==51954== All heap blocks were freed -- no leaks are possible
==51954== 
==51954== For counts of detected and suppressed errors, rerun with: -v
==51954== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
