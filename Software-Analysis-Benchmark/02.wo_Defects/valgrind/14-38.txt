==35185== Memcheck, a memory error detector
==35185== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35185== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35185== Command: ./02_wo_Defects 14038
==35185== 
==35185== 
==35185== HEAP SUMMARY:
==35185==     in use at exit: 0 bytes in 0 blocks
==35185==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35185== 
==35185== All heap blocks were freed -- no leaks are possible
==35185== 
==35185== For counts of detected and suppressed errors, rerun with: -v
==35185== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
