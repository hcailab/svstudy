==38955== Memcheck, a memory error detector
==38955== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38955== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38955== Command: ./02_wo_Defects 22033
==38955== 
==38955== 
==38955== HEAP SUMMARY:
==38955==     in use at exit: 0 bytes in 0 blocks
==38955==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38955== 
==38955== All heap blocks were freed -- no leaks are possible
==38955== 
==38955== For counts of detected and suppressed errors, rerun with: -v
==38955== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
