==31630== Memcheck, a memory error detector
==31630== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31630== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31630== Command: ./02_wo_Defects 5005
==31630== 
==31630== 
==31630== HEAP SUMMARY:
==31630==     in use at exit: 0 bytes in 0 blocks
==31630==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31630== 
==31630== All heap blocks were freed -- no leaks are possible
==31630== 
==31630== For counts of detected and suppressed errors, rerun with: -v
==31630== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
