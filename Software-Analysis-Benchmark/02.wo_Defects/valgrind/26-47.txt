==40877== Memcheck, a memory error detector
==40877== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40877== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40877== Command: ./02_wo_Defects 26047
==40877== 
==40877== 
==40877== HEAP SUMMARY:
==40877==     in use at exit: 0 bytes in 0 blocks
==40877==   total heap usage: 3 allocs, 3 frees, 1,568 bytes allocated
==40877== 
==40877== All heap blocks were freed -- no leaks are possible
==40877== 
==40877== For counts of detected and suppressed errors, rerun with: -v
==40877== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
