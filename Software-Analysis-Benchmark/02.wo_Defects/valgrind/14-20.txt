==34999== Memcheck, a memory error detector
==34999== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34999== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34999== Command: ./02_wo_Defects 14020
==34999== 
==34999== 
==34999== HEAP SUMMARY:
==34999==     in use at exit: 0 bytes in 0 blocks
==34999==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34999== 
==34999== All heap blocks were freed -- no leaks are possible
==34999== 
==34999== For counts of detected and suppressed errors, rerun with: -v
==34999== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
