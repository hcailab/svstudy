==48849== Memcheck, a memory error detector
==48849== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48849== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48849== Command: ./02_wo_Defects 43020
==48849== 
==48849== 
==48849== HEAP SUMMARY:
==48849==     in use at exit: 0 bytes in 0 blocks
==48849==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48849== 
==48849== All heap blocks were freed -- no leaks are possible
==48849== 
==48849== For counts of detected and suppressed errors, rerun with: -v
==48849== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
