==74925== Memcheck, a memory error detector
==74925== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74925== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74925== Command: ./02_wo_Defects 3022
==74925== 
==74925== 
==74925== HEAP SUMMARY:
==74925==     in use at exit: 0 bytes in 0 blocks
==74925==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==74925== 
==74925== All heap blocks were freed -- no leaks are possible
==74925== 
==74925== For counts of detected and suppressed errors, rerun with: -v
==74925== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
