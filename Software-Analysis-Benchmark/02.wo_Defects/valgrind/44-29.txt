==49496== Memcheck, a memory error detector
==49496== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49496== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49496== Command: ./02_wo_Defects 44029
==49496== 
==49496== 
==49496== HEAP SUMMARY:
==49496==     in use at exit: 0 bytes in 0 blocks
==49496==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49496== 
==49496== All heap blocks were freed -- no leaks are possible
==49496== 
==49496== For counts of detected and suppressed errors, rerun with: -v
==49496== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
