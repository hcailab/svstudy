==47941== Memcheck, a memory error detector
==47941== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47941== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47941== Command: ./02_wo_Defects 41034
==47941== 
==47941== 
==47941== HEAP SUMMARY:
==47941==     in use at exit: 0 bytes in 0 blocks
==47941==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47941== 
==47941== All heap blocks were freed -- no leaks are possible
==47941== 
==47941== For counts of detected and suppressed errors, rerun with: -v
==47941== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
