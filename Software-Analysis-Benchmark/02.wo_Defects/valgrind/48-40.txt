==51654== Memcheck, a memory error detector
==51654== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51654== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51654== Command: ./02_wo_Defects 48040
==51654== 
==51654== 
==51654== HEAP SUMMARY:
==51654==     in use at exit: 0 bytes in 0 blocks
==51654==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51654== 
==51654== All heap blocks were freed -- no leaks are possible
==51654== 
==51654== For counts of detected and suppressed errors, rerun with: -v
==51654== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
