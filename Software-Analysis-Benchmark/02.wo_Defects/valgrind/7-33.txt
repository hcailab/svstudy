==32384== Memcheck, a memory error detector
==32384== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32384== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32384== Command: ./02_wo_Defects 7033
==32384== 
==32384== 
==32384== HEAP SUMMARY:
==32384==     in use at exit: 0 bytes in 0 blocks
==32384==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32384== 
==32384== All heap blocks were freed -- no leaks are possible
==32384== 
==32384== For counts of detected and suppressed errors, rerun with: -v
==32384== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
