==34417== Memcheck, a memory error detector
==34417== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34417== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34417== Command: ./02_wo_Defects 13014
==34417== 
==34417== 
==34417== HEAP SUMMARY:
==34417==     in use at exit: 0 bytes in 0 blocks
==34417==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34417== 
==34417== All heap blocks were freed -- no leaks are possible
==34417== 
==34417== For counts of detected and suppressed errors, rerun with: -v
==34417== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
