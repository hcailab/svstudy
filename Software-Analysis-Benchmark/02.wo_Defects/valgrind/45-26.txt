==49928== Memcheck, a memory error detector
==49928== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49928== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49928== Command: ./02_wo_Defects 45026
==49928== 
==49928== 
==49928== HEAP SUMMARY:
==49928==     in use at exit: 0 bytes in 0 blocks
==49928==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49928== 
==49928== All heap blocks were freed -- no leaks are possible
==49928== 
==49928== For counts of detected and suppressed errors, rerun with: -v
==49928== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
