==44325== Memcheck, a memory error detector
==44325== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44325== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44325== Command: ./02_wo_Defects 33044
==44325== 
==44325== 
==44325== HEAP SUMMARY:
==44325==     in use at exit: 0 bytes in 0 blocks
==44325==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44325== 
==44325== All heap blocks were freed -- no leaks are possible
==44325== 
==44325== For counts of detected and suppressed errors, rerun with: -v
==44325== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
