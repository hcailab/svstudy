==50333== Memcheck, a memory error detector
==50333== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50333== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50333== Command: ./02_wo_Defects 46016
==50333== 
==50333== 
==50333== HEAP SUMMARY:
==50333==     in use at exit: 0 bytes in 0 blocks
==50333==   total heap usage: 13 allocs, 13 frees, 1,214 bytes allocated
==50333== 
==50333== All heap blocks were freed -- no leaks are possible
==50333== 
==50333== For counts of detected and suppressed errors, rerun with: -v
==50333== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
