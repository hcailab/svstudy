==45149== Memcheck, a memory error detector
==45149== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==45149== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==45149== Command: ./02_wo_Defects 35048
==45149== 
==45149== 
==45149== HEAP SUMMARY:
==45149==     in use at exit: 0 bytes in 0 blocks
==45149==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==45149== 
==45149== All heap blocks were freed -- no leaks are possible
==45149== 
==45149== For counts of detected and suppressed errors, rerun with: -v
==45149== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
