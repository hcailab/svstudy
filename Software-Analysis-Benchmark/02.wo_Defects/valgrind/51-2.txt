==52551== Memcheck, a memory error detector
==52551== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52551== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52551== Command: ./02_wo_Defects 51002
==52551== 
==52551== 
==52551== HEAP SUMMARY:
==52551==     in use at exit: 0 bytes in 0 blocks
==52551==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52551== 
==52551== All heap blocks were freed -- no leaks are possible
==52551== 
==52551== For counts of detected and suppressed errors, rerun with: -v
==52551== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
