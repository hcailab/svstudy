==46542== Memcheck, a memory error detector
==46542== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46542== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46542== Command: ./02_wo_Defects 38035
==46542== 
==46542== 
==46542== HEAP SUMMARY:
==46542==     in use at exit: 0 bytes in 0 blocks
==46542==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46542== 
==46542== All heap blocks were freed -- no leaks are possible
==46542== 
==46542== For counts of detected and suppressed errors, rerun with: -v
==46542== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
