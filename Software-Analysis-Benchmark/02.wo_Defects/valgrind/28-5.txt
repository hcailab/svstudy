==41541== Memcheck, a memory error detector
==41541== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41541== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41541== Command: ./02_wo_Defects 28005
==41541== 
==41541== 
==41541== HEAP SUMMARY:
==41541==     in use at exit: 0 bytes in 0 blocks
==41541==   total heap usage: 2 allocs, 2 frees, 7,424 bytes allocated
==41541== 
==41541== All heap blocks were freed -- no leaks are possible
==41541== 
==41541== For counts of detected and suppressed errors, rerun with: -v
==41541== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
