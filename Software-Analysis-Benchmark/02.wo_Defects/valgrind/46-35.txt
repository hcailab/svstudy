==50509== Memcheck, a memory error detector
==50509== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50509== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50509== Command: ./02_wo_Defects 46035
==50509== 
==50509== 
==50509== HEAP SUMMARY:
==50509==     in use at exit: 0 bytes in 0 blocks
==50509==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50509== 
==50509== All heap blocks were freed -- no leaks are possible
==50509== 
==50509== For counts of detected and suppressed errors, rerun with: -v
==50509== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
