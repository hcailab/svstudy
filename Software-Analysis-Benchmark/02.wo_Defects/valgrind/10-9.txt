==32977== Memcheck, a memory error detector
==32977== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32977== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32977== Command: ./02_wo_Defects 10009
==32977== 
==32977== 
==32977== HEAP SUMMARY:
==32977==     in use at exit: 0 bytes in 0 blocks
==32977==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32977== 
==32977== All heap blocks were freed -- no leaks are possible
==32977== 
==32977== For counts of detected and suppressed errors, rerun with: -v
==32977== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
