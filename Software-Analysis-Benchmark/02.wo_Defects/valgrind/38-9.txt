==46349== Memcheck, a memory error detector
==46349== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46349== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46349== Command: ./02_wo_Defects 38009
==46349== 
==46349== 
==46349== HEAP SUMMARY:
==46349==     in use at exit: 0 bytes in 0 blocks
==46349==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46349== 
==46349== All heap blocks were freed -- no leaks are possible
==46349== 
==46349== For counts of detected and suppressed errors, rerun with: -v
==46349== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
