==31352== Memcheck, a memory error detector
==31352== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31352== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31352== Command: ./02_wo_Defects 4021
==31352== 
==31352== 
==31352== HEAP SUMMARY:
==31352==     in use at exit: 0 bytes in 0 blocks
==31352==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31352== 
==31352== All heap blocks were freed -- no leaks are possible
==31352== 
==31352== For counts of detected and suppressed errors, rerun with: -v
==31352== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
