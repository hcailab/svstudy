==32809== Memcheck, a memory error detector
==32809== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32809== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32809== Command: ./02_wo_Defects 9036
==32809== 
==32809== 
==32809== HEAP SUMMARY:
==32809==     in use at exit: 0 bytes in 0 blocks
==32809==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32809== 
==32809== All heap blocks were freed -- no leaks are possible
==32809== 
==32809== For counts of detected and suppressed errors, rerun with: -v
==32809== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
