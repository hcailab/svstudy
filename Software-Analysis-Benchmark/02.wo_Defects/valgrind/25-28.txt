==40088== Memcheck, a memory error detector
==40088== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40088== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40088== Command: ./02_wo_Defects 25028
==40088== 
==40088== 
==40088== HEAP SUMMARY:
==40088==     in use at exit: 0 bytes in 0 blocks
==40088==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40088== 
==40088== All heap blocks were freed -- no leaks are possible
==40088== 
==40088== For counts of detected and suppressed errors, rerun with: -v
==40088== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
