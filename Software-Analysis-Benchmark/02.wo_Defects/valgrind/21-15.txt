==38404== Memcheck, a memory error detector
==38404== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38404== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38404== Command: ./02_wo_Defects 21015
==38404== 
==38404== 
==38404== HEAP SUMMARY:
==38404==     in use at exit: 0 bytes in 0 blocks
==38404==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38404== 
==38404== All heap blocks were freed -- no leaks are possible
==38404== 
==38404== For counts of detected and suppressed errors, rerun with: -v
==38404== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
