==49568== Memcheck, a memory error detector
==49568== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49568== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49568== Command: ./02_wo_Defects 44037
==49568== 
==49568== 
==49568== HEAP SUMMARY:
==49568==     in use at exit: 0 bytes in 0 blocks
==49568==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49568== 
==49568== All heap blocks were freed -- no leaks are possible
==49568== 
==49568== For counts of detected and suppressed errors, rerun with: -v
==49568== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
