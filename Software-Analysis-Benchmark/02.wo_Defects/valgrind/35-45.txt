==45104== Memcheck, a memory error detector
==45104== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==45104== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==45104== Command: ./02_wo_Defects 35045
==45104== 
==45104== 
==45104== HEAP SUMMARY:
==45104==     in use at exit: 0 bytes in 0 blocks
==45104==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==45104== 
==45104== All heap blocks were freed -- no leaks are possible
==45104== 
==45104== For counts of detected and suppressed errors, rerun with: -v
==45104== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
