==30586== Memcheck, a memory error detector
==30586== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30586== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30586== Command: ./02_wo_Defects 1008
==30586== 
==30586== 
==30586== HEAP SUMMARY:
==30586==     in use at exit: 0 bytes in 0 blocks
==30586==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30586== 
==30586== All heap blocks were freed -- no leaks are possible
==30586== 
==30586== For counts of detected and suppressed errors, rerun with: -v
==30586== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
