==73264== Memcheck, a memory error detector
==73264== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73264== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73264== Command: ./02_wo_Defects 32046
==73264== 
==73264== 
==73264== HEAP SUMMARY:
==73264==     in use at exit: 0 bytes in 0 blocks
==73264==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==73264== 
==73264== All heap blocks were freed -- no leaks are possible
==73264== 
==73264== For counts of detected and suppressed errors, rerun with: -v
==73264== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
