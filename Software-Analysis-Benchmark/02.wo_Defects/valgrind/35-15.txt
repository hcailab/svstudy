==44899== Memcheck, a memory error detector
==44899== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44899== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44899== Command: ./02_wo_Defects 35015
==44899== 
==44899== 
==44899== HEAP SUMMARY:
==44899==     in use at exit: 0 bytes in 0 blocks
==44899==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44899== 
==44899== All heap blocks were freed -- no leaks are possible
==44899== 
==44899== For counts of detected and suppressed errors, rerun with: -v
==44899== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
