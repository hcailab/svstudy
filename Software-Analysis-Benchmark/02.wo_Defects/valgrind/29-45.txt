==42520== Memcheck, a memory error detector
==42520== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42520== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42520== Command: ./02_wo_Defects 29045
==42520== 
==42520== 
==42520== HEAP SUMMARY:
==42520==     in use at exit: 0 bytes in 0 blocks
==42520==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42520== 
==42520== All heap blocks were freed -- no leaks are possible
==42520== 
==42520== For counts of detected and suppressed errors, rerun with: -v
==42520== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
