==44731== Memcheck, a memory error detector
==44731== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44731== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44731== Command: ./02_wo_Defects 34041
==44731== 
==44731== 
==44731== HEAP SUMMARY:
==44731==     in use at exit: 0 bytes in 0 blocks
==44731==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44731== 
==44731== All heap blocks were freed -- no leaks are possible
==44731== 
==44731== For counts of detected and suppressed errors, rerun with: -v
==44731== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
