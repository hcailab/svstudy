==34087== Memcheck, a memory error detector
==34087== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34087== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34087== Command: ./02_wo_Defects 12031
==34087== 
==34087== 
==34087== HEAP SUMMARY:
==34087==     in use at exit: 0 bytes in 0 blocks
==34087==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34087== 
==34087== All heap blocks were freed -- no leaks are possible
==34087== 
==34087== For counts of detected and suppressed errors, rerun with: -v
==34087== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
