==32200== Memcheck, a memory error detector
==32200== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32200== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32200== Command: ./02_wo_Defects 6043
==32200== 
==32200== 
==32200== HEAP SUMMARY:
==32200==     in use at exit: 0 bytes in 0 blocks
==32200==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32200== 
==32200== All heap blocks were freed -- no leaks are possible
==32200== 
==32200== For counts of detected and suppressed errors, rerun with: -v
==32200== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
