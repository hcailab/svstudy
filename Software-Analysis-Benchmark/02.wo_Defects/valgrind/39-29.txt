==46863== Memcheck, a memory error detector
==46863== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46863== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46863== Command: ./02_wo_Defects 39029
==46863== 
==46863== 
==46863== HEAP SUMMARY:
==46863==     in use at exit: 0 bytes in 0 blocks
==46863==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46863== 
==46863== All heap blocks were freed -- no leaks are possible
==46863== 
==46863== For counts of detected and suppressed errors, rerun with: -v
==46863== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
