==34207== Memcheck, a memory error detector
==34207== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34207== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34207== Command: ./02_wo_Defects 12043
==34207== 
==34207== 
==34207== HEAP SUMMARY:
==34207==     in use at exit: 0 bytes in 0 blocks
==34207==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34207== 
==34207== All heap blocks were freed -- no leaks are possible
==34207== 
==34207== For counts of detected and suppressed errors, rerun with: -v
==34207== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
