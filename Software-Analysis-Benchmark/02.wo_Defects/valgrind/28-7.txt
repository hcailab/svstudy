==41567== Memcheck, a memory error detector
==41567== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41567== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41567== Command: ./02_wo_Defects 28007
==41567== 
==41567== 
==41567== HEAP SUMMARY:
==41567==     in use at exit: 31 bytes in 2 blocks
==41567==   total heap usage: 3 allocs, 1 frees, 1,055 bytes allocated
==41567== 
==41567== 17 bytes in 1 blocks are definitely lost in loss record 2 of 2
==41567==    at 0x4C2FB0F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==41567==    by 0x10C906: memory_allocation_failure_007_func_001 (memory_allocation_failure.c:224)
==41567==    by 0x10C906: memory_allocation_failure_007 (memory_allocation_failure.c:250)
==41567==    by 0x10D16E: memory_allocation_failure_main (memory_allocation_failure.c:772)
==41567==    by 0x109ACA: main (main.c:191)
==41567== 
==41567== LEAK SUMMARY:
==41567==    definitely lost: 17 bytes in 1 blocks
==41567==    indirectly lost: 0 bytes in 0 blocks
==41567==      possibly lost: 0 bytes in 0 blocks
==41567==    still reachable: 14 bytes in 1 blocks
==41567==         suppressed: 0 bytes in 0 blocks
==41567== Reachable blocks (those to which a pointer was found) are not shown.
==41567== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==41567== 
==41567== For counts of detected and suppressed errors, rerun with: -v
==41567== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
