==34616== Memcheck, a memory error detector
==34616== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34616== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34616== Command: ./02_wo_Defects 13032
==34616== 
==34616== 
==34616== HEAP SUMMARY:
==34616==     in use at exit: 0 bytes in 0 blocks
==34616==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34616== 
==34616== All heap blocks were freed -- no leaks are possible
==34616== 
==34616== For counts of detected and suppressed errors, rerun with: -v
==34616== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
