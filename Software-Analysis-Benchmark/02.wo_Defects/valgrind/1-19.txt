==30644== Memcheck, a memory error detector
==30644== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30644== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30644== Command: ./02_wo_Defects 1019
==30644== 
==30644== 
==30644== HEAP SUMMARY:
==30644==     in use at exit: 0 bytes in 0 blocks
==30644==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30644== 
==30644== All heap blocks were freed -- no leaks are possible
==30644== 
==30644== For counts of detected and suppressed errors, rerun with: -v
==30644== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
