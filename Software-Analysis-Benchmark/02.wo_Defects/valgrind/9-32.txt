==32801== Memcheck, a memory error detector
==32801== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32801== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32801== Command: ./02_wo_Defects 9032
==32801== 
==32801== 
==32801== HEAP SUMMARY:
==32801==     in use at exit: 0 bytes in 0 blocks
==32801==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32801== 
==32801== All heap blocks were freed -- no leaks are possible
==32801== 
==32801== For counts of detected and suppressed errors, rerun with: -v
==32801== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
