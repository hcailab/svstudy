==46040== Memcheck, a memory error detector
==46040== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46040== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46040== Command: ./02_wo_Defects 37020
==46040== 
==46040== 
==46040== HEAP SUMMARY:
==46040==     in use at exit: 0 bytes in 0 blocks
==46040==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46040== 
==46040== All heap blocks were freed -- no leaks are possible
==46040== 
==46040== For counts of detected and suppressed errors, rerun with: -v
==46040== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
