==51480== Memcheck, a memory error detector
==51480== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51480== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51480== Command: ./02_wo_Defects 48020
==51480== 
==51480== 
==51480== HEAP SUMMARY:
==51480==     in use at exit: 0 bytes in 0 blocks
==51480==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51480== 
==51480== All heap blocks were freed -- no leaks are possible
==51480== 
==51480== For counts of detected and suppressed errors, rerun with: -v
==51480== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
