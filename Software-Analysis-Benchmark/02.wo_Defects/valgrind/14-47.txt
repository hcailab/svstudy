==35306== Memcheck, a memory error detector
==35306== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35306== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35306== Command: ./02_wo_Defects 14047
==35306== 
==35306== 
==35306== HEAP SUMMARY:
==35306==     in use at exit: 0 bytes in 0 blocks
==35306==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35306== 
==35306== All heap blocks were freed -- no leaks are possible
==35306== 
==35306== For counts of detected and suppressed errors, rerun with: -v
==35306== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
