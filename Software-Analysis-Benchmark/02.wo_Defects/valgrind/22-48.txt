==39087== Memcheck, a memory error detector
==39087== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39087== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39087== Command: ./02_wo_Defects 22048
==39087== 
==39087== 
==39087== HEAP SUMMARY:
==39087==     in use at exit: 0 bytes in 0 blocks
==39087==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39087== 
==39087== All heap blocks were freed -- no leaks are possible
==39087== 
==39087== For counts of detected and suppressed errors, rerun with: -v
==39087== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
