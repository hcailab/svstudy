==50400== Memcheck, a memory error detector
==50400== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50400== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50400== Command: ./02_wo_Defects 46023
==50400== 
==50400== 
==50400== HEAP SUMMARY:
==50400==     in use at exit: 0 bytes in 0 blocks
==50400==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50400== 
==50400== All heap blocks were freed -- no leaks are possible
==50400== 
==50400== For counts of detected and suppressed errors, rerun with: -v
==50400== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
