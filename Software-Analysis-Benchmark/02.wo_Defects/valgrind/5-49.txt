==31937== Memcheck, a memory error detector
==31937== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31937== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31937== Command: ./02_wo_Defects 5049
==31937== 
==31937== 
==31937== HEAP SUMMARY:
==31937==     in use at exit: 0 bytes in 0 blocks
==31937==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31937== 
==31937== All heap blocks were freed -- no leaks are possible
==31937== 
==31937== For counts of detected and suppressed errors, rerun with: -v
==31937== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
