==48587== Memcheck, a memory error detector
==48587== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48587== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48587== Command: ./02_wo_Defects 42045
==48587== 
==48587== 
==48587== HEAP SUMMARY:
==48587==     in use at exit: 0 bytes in 0 blocks
==48587==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48587== 
==48587== All heap blocks were freed -- no leaks are possible
==48587== 
==48587== For counts of detected and suppressed errors, rerun with: -v
==48587== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
