==37537== Memcheck, a memory error detector
==37537== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37537== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37537== Command: ./02_wo_Defects 19015
==37537== 
==37537== 
==37537== HEAP SUMMARY:
==37537==     in use at exit: 0 bytes in 0 blocks
==37537==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37537== 
==37537== All heap blocks were freed -- no leaks are possible
==37537== 
==37537== For counts of detected and suppressed errors, rerun with: -v
==37537== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
