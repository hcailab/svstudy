==41389== Memcheck, a memory error detector
==41389== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41389== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41389== Command: ./02_wo_Defects 27039
==41389== 
==41389== 
==41389== HEAP SUMMARY:
==41389==     in use at exit: 0 bytes in 0 blocks
==41389==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41389== 
==41389== All heap blocks were freed -- no leaks are possible
==41389== 
==41389== For counts of detected and suppressed errors, rerun with: -v
==41389== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
