==46846== Memcheck, a memory error detector
==46846== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46846== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46846== Command: ./02_wo_Defects 39027
==46846== 
==46846== 
==46846== HEAP SUMMARY:
==46846==     in use at exit: 0 bytes in 0 blocks
==46846==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46846== 
==46846== All heap blocks were freed -- no leaks are possible
==46846== 
==46846== For counts of detected and suppressed errors, rerun with: -v
==46846== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
