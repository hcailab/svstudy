==74438== Memcheck, a memory error detector
==74438== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74438== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74438== Command: ./02_wo_Defects 7009
==74438== 
==74438== 
==74438== HEAP SUMMARY:
==74438==     in use at exit: 0 bytes in 0 blocks
==74438==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==74438== 
==74438== All heap blocks were freed -- no leaks are possible
==74438== 
==74438== For counts of detected and suppressed errors, rerun with: -v
==74438== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
