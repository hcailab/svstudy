==51597== Memcheck, a memory error detector
==51597== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51597== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51597== Command: ./02_wo_Defects 48030
==51597== 
==51597== 
==51597== HEAP SUMMARY:
==51597==     in use at exit: 0 bytes in 0 blocks
==51597==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51597== 
==51597== All heap blocks were freed -- no leaks are possible
==51597== 
==51597== For counts of detected and suppressed errors, rerun with: -v
==51597== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
