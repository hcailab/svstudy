==51446== Memcheck, a memory error detector
==51446== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51446== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51446== Command: ./02_wo_Defects 48017
==51446== 
==51446== 
==51446== HEAP SUMMARY:
==51446==     in use at exit: 0 bytes in 0 blocks
==51446==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51446== 
==51446== All heap blocks were freed -- no leaks are possible
==51446== 
==51446== For counts of detected and suppressed errors, rerun with: -v
==51446== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
