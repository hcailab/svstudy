==42116== Memcheck, a memory error detector
==42116== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42116== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42116== Command: ./02_wo_Defects 29010
==42116== 
==42116== 
==42116== HEAP SUMMARY:
==42116==     in use at exit: 0 bytes in 0 blocks
==42116==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==42116== 
==42116== All heap blocks were freed -- no leaks are possible
==42116== 
==42116== For counts of detected and suppressed errors, rerun with: -v
==42116== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
