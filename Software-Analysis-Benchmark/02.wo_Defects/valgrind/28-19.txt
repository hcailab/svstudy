==41695== Memcheck, a memory error detector
==41695== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41695== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41695== Command: ./02_wo_Defects 28019
==41695== 
==41695== 
==41695== HEAP SUMMARY:
==41695==     in use at exit: 0 bytes in 0 blocks
==41695==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41695== 
==41695== All heap blocks were freed -- no leaks are possible
==41695== 
==41695== For counts of detected and suppressed errors, rerun with: -v
==41695== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
