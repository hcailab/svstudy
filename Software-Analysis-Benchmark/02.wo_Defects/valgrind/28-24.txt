==41745== Memcheck, a memory error detector
==41745== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41745== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41745== Command: ./02_wo_Defects 28024
==41745== 
==41745== 
==41745== HEAP SUMMARY:
==41745==     in use at exit: 0 bytes in 0 blocks
==41745==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41745== 
==41745== All heap blocks were freed -- no leaks are possible
==41745== 
==41745== For counts of detected and suppressed errors, rerun with: -v
==41745== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
