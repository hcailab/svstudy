==41827== Memcheck, a memory error detector
==41827== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41827== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41827== Command: ./02_wo_Defects 28032
==41827== 
==41827== 
==41827== HEAP SUMMARY:
==41827==     in use at exit: 0 bytes in 0 blocks
==41827==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41827== 
==41827== All heap blocks were freed -- no leaks are possible
==41827== 
==41827== For counts of detected and suppressed errors, rerun with: -v
==41827== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
