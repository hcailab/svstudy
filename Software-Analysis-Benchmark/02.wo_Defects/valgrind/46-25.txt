==50419== Memcheck, a memory error detector
==50419== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50419== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50419== Command: ./02_wo_Defects 46025
==50419== 
==50419== 
==50419== HEAP SUMMARY:
==50419==     in use at exit: 0 bytes in 0 blocks
==50419==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50419== 
==50419== All heap blocks were freed -- no leaks are possible
==50419== 
==50419== For counts of detected and suppressed errors, rerun with: -v
==50419== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
