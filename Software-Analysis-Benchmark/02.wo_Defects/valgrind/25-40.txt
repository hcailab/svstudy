==40174== Memcheck, a memory error detector
==40174== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40174== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40174== Command: ./02_wo_Defects 25040
==40174== 
==40174== 
==40174== HEAP SUMMARY:
==40174==     in use at exit: 0 bytes in 0 blocks
==40174==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==40174== 
==40174== All heap blocks were freed -- no leaks are possible
==40174== 
==40174== For counts of detected and suppressed errors, rerun with: -v
==40174== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
