==42635== Memcheck, a memory error detector
==42635== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42635== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42635== Command: ./02_wo_Defects 30007
==42635== 
==42635== 
==42635== HEAP SUMMARY:
==42635==     in use at exit: 0 bytes in 0 blocks
==42635==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42635== 
==42635== All heap blocks were freed -- no leaks are possible
==42635== 
==42635== For counts of detected and suppressed errors, rerun with: -v
==42635== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
