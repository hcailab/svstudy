==39698== Memcheck, a memory error detector
==39698== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39698== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39698== Command: ./02_wo_Defects 24032
==39698== 
==39698== 
==39698== HEAP SUMMARY:
==39698==     in use at exit: 0 bytes in 0 blocks
==39698==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39698== 
==39698== All heap blocks were freed -- no leaks are possible
==39698== 
==39698== For counts of detected and suppressed errors, rerun with: -v
==39698== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
