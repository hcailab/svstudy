==73751== Memcheck, a memory error detector
==73751== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73751== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73751== Command: ./02_wo_Defects 42003
==73751== 
==73751== 
==73751== HEAP SUMMARY:
==73751==     in use at exit: 0 bytes in 0 blocks
==73751==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==73751== 
==73751== All heap blocks were freed -- no leaks are possible
==73751== 
==73751== For counts of detected and suppressed errors, rerun with: -v
==73751== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
