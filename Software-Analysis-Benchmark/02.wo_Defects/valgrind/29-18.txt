==42233== Memcheck, a memory error detector
==42233== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42233== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42233== Command: ./02_wo_Defects 29018
==42233== 
==42233== 
==42233== HEAP SUMMARY:
==42233==     in use at exit: 0 bytes in 0 blocks
==42233==   total heap usage: 7 allocs, 7 frees, 1,139 bytes allocated
==42233== 
==42233== All heap blocks were freed -- no leaks are possible
==42233== 
==42233== For counts of detected and suppressed errors, rerun with: -v
==42233== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
