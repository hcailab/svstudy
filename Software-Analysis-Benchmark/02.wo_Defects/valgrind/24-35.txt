==39739== Memcheck, a memory error detector
==39739== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39739== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39739== Command: ./02_wo_Defects 24035
==39739== 
==39739== 
==39739== HEAP SUMMARY:
==39739==     in use at exit: 0 bytes in 0 blocks
==39739==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39739== 
==39739== All heap blocks were freed -- no leaks are possible
==39739== 
==39739== For counts of detected and suppressed errors, rerun with: -v
==39739== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
