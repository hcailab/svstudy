==50452== Memcheck, a memory error detector
==50452== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50452== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50452== Command: ./02_wo_Defects 46029
==50452== 
==50452== 
==50452== HEAP SUMMARY:
==50452==     in use at exit: 0 bytes in 0 blocks
==50452==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50452== 
==50452== All heap blocks were freed -- no leaks are possible
==50452== 
==50452== For counts of detected and suppressed errors, rerun with: -v
==50452== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
