==48833== Memcheck, a memory error detector
==48833== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48833== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48833== Command: ./02_wo_Defects 43018
==48833== 
==48833== 
==48833== HEAP SUMMARY:
==48833==     in use at exit: 0 bytes in 0 blocks
==48833==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48833== 
==48833== All heap blocks were freed -- no leaks are possible
==48833== 
==48833== For counts of detected and suppressed errors, rerun with: -v
==48833== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
