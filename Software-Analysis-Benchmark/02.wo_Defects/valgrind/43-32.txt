==48961== Memcheck, a memory error detector
==48961== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==48961== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==48961== Command: ./02_wo_Defects 43032
==48961== 
==48961== 
==48961== HEAP SUMMARY:
==48961==     in use at exit: 0 bytes in 0 blocks
==48961==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==48961== 
==48961== All heap blocks were freed -- no leaks are possible
==48961== 
==48961== For counts of detected and suppressed errors, rerun with: -v
==48961== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
