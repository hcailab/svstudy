==41755== Memcheck, a memory error detector
==41755== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41755== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41755== Command: ./02_wo_Defects 28025
==41755== 
==41755== 
==41755== HEAP SUMMARY:
==41755==     in use at exit: 0 bytes in 0 blocks
==41755==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41755== 
==41755== All heap blocks were freed -- no leaks are possible
==41755== 
==41755== For counts of detected and suppressed errors, rerun with: -v
==41755== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
