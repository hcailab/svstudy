==74759== Memcheck, a memory error detector
==74759== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74759== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74759== Command: ./02_wo_Defects 2022
==74759== 
==74759== 
==74759== HEAP SUMMARY:
==74759==     in use at exit: 0 bytes in 0 blocks
==74759==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==74759== 
==74759== All heap blocks were freed -- no leaks are possible
==74759== 
==74759== For counts of detected and suppressed errors, rerun with: -v
==74759== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
