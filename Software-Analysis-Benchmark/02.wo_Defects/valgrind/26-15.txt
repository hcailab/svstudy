==40440== Memcheck, a memory error detector
==40440== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40440== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40440== Command: ./02_wo_Defects 26015
==40440== 
==40440== 
==40440== HEAP SUMMARY:
==40440==     in use at exit: 0 bytes in 0 blocks
==40440==   total heap usage: 3 allocs, 3 frees, 1,568 bytes allocated
==40440== 
==40440== All heap blocks were freed -- no leaks are possible
==40440== 
==40440== For counts of detected and suppressed errors, rerun with: -v
==40440== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
