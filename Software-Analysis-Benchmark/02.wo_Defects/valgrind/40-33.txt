==47380== Memcheck, a memory error detector
==47380== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==47380== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==47380== Command: ./02_wo_Defects 40033
==47380== 
==47380== 
==47380== HEAP SUMMARY:
==47380==     in use at exit: 0 bytes in 0 blocks
==47380==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==47380== 
==47380== All heap blocks were freed -- no leaks are possible
==47380== 
==47380== For counts of detected and suppressed errors, rerun with: -v
==47380== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
