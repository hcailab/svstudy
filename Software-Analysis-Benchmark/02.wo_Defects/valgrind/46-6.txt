==50239== Memcheck, a memory error detector
==50239== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50239== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50239== Command: ./02_wo_Defects 46006
==50239== 
==50239== 
==50239== HEAP SUMMARY:
==50239==     in use at exit: 0 bytes in 0 blocks
==50239==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50239== 
==50239== All heap blocks were freed -- no leaks are possible
==50239== 
==50239== For counts of detected and suppressed errors, rerun with: -v
==50239== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
