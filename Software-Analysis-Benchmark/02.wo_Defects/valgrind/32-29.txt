==73213== Memcheck, a memory error detector
==73213== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73213== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73213== Command: ./02_wo_Defects 32029
==73213== 
==73213== 
==73213== HEAP SUMMARY:
==73213==     in use at exit: 0 bytes in 0 blocks
==73213==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==73213== 
==73213== All heap blocks were freed -- no leaks are possible
==73213== 
==73213== For counts of detected and suppressed errors, rerun with: -v
==73213== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
