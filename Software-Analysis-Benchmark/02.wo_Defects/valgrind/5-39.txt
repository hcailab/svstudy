==31898== Memcheck, a memory error detector
==31898== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31898== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31898== Command: ./02_wo_Defects 5039
==31898== 
==31898== 
==31898== HEAP SUMMARY:
==31898==     in use at exit: 0 bytes in 0 blocks
==31898==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31898== 
==31898== All heap blocks were freed -- no leaks are possible
==31898== 
==31898== For counts of detected and suppressed errors, rerun with: -v
==31898== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
