==31959== Memcheck, a memory error detector
==31959== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==31959== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==31959== Command: ./02_wo_Defects 6004
==31959== 
==31959== 
==31959== HEAP SUMMARY:
==31959==     in use at exit: 0 bytes in 0 blocks
==31959==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==31959== 
==31959== All heap blocks were freed -- no leaks are possible
==31959== 
==31959== For counts of detected and suppressed errors, rerun with: -v
==31959== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
