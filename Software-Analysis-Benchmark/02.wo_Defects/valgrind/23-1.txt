==39107== Memcheck, a memory error detector
==39107== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39107== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39107== Command: ./02_wo_Defects 23001
==39107== 
==39107== 
==39107== HEAP SUMMARY:
==39107==     in use at exit: 0 bytes in 0 blocks
==39107==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39107== 
==39107== All heap blocks were freed -- no leaks are possible
==39107== 
==39107== For counts of detected and suppressed errors, rerun with: -v
==39107== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
