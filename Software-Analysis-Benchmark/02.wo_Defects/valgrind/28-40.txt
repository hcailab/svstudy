==41907== Memcheck, a memory error detector
==41907== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41907== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41907== Command: ./02_wo_Defects 28040
==41907== 
==41907== 
==41907== HEAP SUMMARY:
==41907==     in use at exit: 0 bytes in 0 blocks
==41907==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41907== 
==41907== All heap blocks were freed -- no leaks are possible
==41907== 
==41907== For counts of detected and suppressed errors, rerun with: -v
==41907== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
