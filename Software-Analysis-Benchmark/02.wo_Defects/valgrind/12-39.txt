==34171== Memcheck, a memory error detector
==34171== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34171== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34171== Command: ./02_wo_Defects 12039
==34171== 
==34171== 
==34171== HEAP SUMMARY:
==34171==     in use at exit: 0 bytes in 0 blocks
==34171==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34171== 
==34171== All heap blocks were freed -- no leaks are possible
==34171== 
==34171== For counts of detected and suppressed errors, rerun with: -v
==34171== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
