==35487== Memcheck, a memory error detector
==35487== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35487== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35487== Command: ./02_wo_Defects 15013
==35487== 
==35487== 
==35487== HEAP SUMMARY:
==35487==     in use at exit: 0 bytes in 0 blocks
==35487==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35487== 
==35487== All heap blocks were freed -- no leaks are possible
==35487== 
==35487== For counts of detected and suppressed errors, rerun with: -v
==35487== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
