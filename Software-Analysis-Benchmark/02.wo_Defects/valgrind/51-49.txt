==52923== Memcheck, a memory error detector
==52923== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52923== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52923== Command: ./02_wo_Defects 51049
==52923== 
==52923== 
==52923== HEAP SUMMARY:
==52923==     in use at exit: 0 bytes in 0 blocks
==52923==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52923== 
==52923== All heap blocks were freed -- no leaks are possible
==52923== 
==52923== For counts of detected and suppressed errors, rerun with: -v
==52923== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
