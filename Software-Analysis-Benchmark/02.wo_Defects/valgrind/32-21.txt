==73172== Memcheck, a memory error detector
==73172== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73172== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73172== Command: ./02_wo_Defects 32021
==73172== 
==73172== 
==73172== HEAP SUMMARY:
==73172==     in use at exit: 0 bytes in 0 blocks
==73172==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==73172== 
==73172== All heap blocks were freed -- no leaks are possible
==73172== 
==73172== For counts of detected and suppressed errors, rerun with: -v
==73172== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
