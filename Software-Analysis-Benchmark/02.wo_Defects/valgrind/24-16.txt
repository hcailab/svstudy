==73980== Memcheck, a memory error detector
==73980== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73980== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73980== Command: ./02_wo_Defects 24016
==73980== 
==73980== 
==73980== HEAP SUMMARY:
==73980==     in use at exit: 0 bytes in 0 blocks
==73980==   total heap usage: 12 allocs, 12 frees, 1,204 bytes allocated
==73980== 
==73980== All heap blocks were freed -- no leaks are possible
==73980== 
==73980== For counts of detected and suppressed errors, rerun with: -v
==73980== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
