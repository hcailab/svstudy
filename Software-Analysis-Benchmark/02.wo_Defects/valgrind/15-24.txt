==35598== Memcheck, a memory error detector
==35598== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35598== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35598== Command: ./02_wo_Defects 15024
==35598== 
==35598== 
==35598== HEAP SUMMARY:
==35598==     in use at exit: 0 bytes in 0 blocks
==35598==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35598== 
==35598== All heap blocks were freed -- no leaks are possible
==35598== 
==35598== For counts of detected and suppressed errors, rerun with: -v
==35598== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
