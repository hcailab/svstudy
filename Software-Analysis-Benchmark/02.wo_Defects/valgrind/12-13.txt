==33925== Memcheck, a memory error detector
==33925== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33925== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33925== Command: ./02_wo_Defects 12013
==33925== 
==33925== 
==33925== HEAP SUMMARY:
==33925==     in use at exit: 0 bytes in 0 blocks
==33925==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33925== 
==33925== All heap blocks were freed -- no leaks are possible
==33925== 
==33925== For counts of detected and suppressed errors, rerun with: -v
==33925== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
