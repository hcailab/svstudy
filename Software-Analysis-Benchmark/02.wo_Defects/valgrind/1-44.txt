==30824== Memcheck, a memory error detector
==30824== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30824== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30824== Command: ./02_wo_Defects 1044
==30824== 
==30824== 
==30824== HEAP SUMMARY:
==30824==     in use at exit: 0 bytes in 0 blocks
==30824==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30824== 
==30824== All heap blocks were freed -- no leaks are possible
==30824== 
==30824== For counts of detected and suppressed errors, rerun with: -v
==30824== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
