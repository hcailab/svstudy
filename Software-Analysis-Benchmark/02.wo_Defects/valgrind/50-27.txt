==52349== Memcheck, a memory error detector
==52349== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52349== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52349== Command: ./02_wo_Defects 50027
==52349== 
==52349== 
==52349== HEAP SUMMARY:
==52349==     in use at exit: 0 bytes in 0 blocks
==52349==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52349== 
==52349== All heap blocks were freed -- no leaks are possible
==52349== 
==52349== For counts of detected and suppressed errors, rerun with: -v
==52349== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
