==73642== Memcheck, a memory error detector
==73642== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73642== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73642== Command: ./02_wo_Defects 41004
==73642== 
==73642== 
==73642== HEAP SUMMARY:
==73642==     in use at exit: 544 bytes in 2 blocks
==73642==   total heap usage: 3 allocs, 1 frees, 1,568 bytes allocated
==73642== 
==73642== LEAK SUMMARY:
==73642==    definitely lost: 0 bytes in 0 blocks
==73642==    indirectly lost: 0 bytes in 0 blocks
==73642==      possibly lost: 544 bytes in 2 blocks
==73642==    still reachable: 0 bytes in 0 blocks
==73642==         suppressed: 0 bytes in 0 blocks
==73642== Rerun with --leak-check=full to see details of leaked memory
==73642== 
==73642== For counts of detected and suppressed errors, rerun with: -v
==73642== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
