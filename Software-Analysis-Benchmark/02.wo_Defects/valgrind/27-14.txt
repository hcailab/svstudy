==41131== Memcheck, a memory error detector
==41131== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==41131== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==41131== Command: ./02_wo_Defects 27014
==41131== 
==41131== 
==41131== HEAP SUMMARY:
==41131==     in use at exit: 0 bytes in 0 blocks
==41131==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==41131== 
==41131== All heap blocks were freed -- no leaks are possible
==41131== 
==41131== For counts of detected and suppressed errors, rerun with: -v
==41131== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
