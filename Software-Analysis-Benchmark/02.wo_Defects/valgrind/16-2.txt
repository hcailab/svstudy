==74143== Memcheck, a memory error detector
==74143== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74143== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74143== Command: ./02_wo_Defects 16002
==74143== 
==74143== 
==74143== HEAP SUMMARY:
==74143==     in use at exit: 0 bytes in 0 blocks
==74143==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==74143== 
==74143== All heap blocks were freed -- no leaks are possible
==74143== 
==74143== For counts of detected and suppressed errors, rerun with: -v
==74143== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
