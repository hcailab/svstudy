==37939== Memcheck, a memory error detector
==37939== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37939== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37939== Command: ./02_wo_Defects 20006
==37939== 
==37939== 
==37939== HEAP SUMMARY:
==37939==     in use at exit: 0 bytes in 0 blocks
==37939==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37939== 
==37939== All heap blocks were freed -- no leaks are possible
==37939== 
==37939== For counts of detected and suppressed errors, rerun with: -v
==37939== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
