==51917== Memcheck, a memory error detector
==51917== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==51917== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==51917== Command: ./02_wo_Defects 49024
==51917== 
==51917== 
==51917== HEAP SUMMARY:
==51917==     in use at exit: 0 bytes in 0 blocks
==51917==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==51917== 
==51917== All heap blocks were freed -- no leaks are possible
==51917== 
==51917== For counts of detected and suppressed errors, rerun with: -v
==51917== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
