==33422== Memcheck, a memory error detector
==33422== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33422== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33422== Command: ./02_wo_Defects 11005
==33422== 
==33422== 
==33422== HEAP SUMMARY:
==33422==     in use at exit: 0 bytes in 0 blocks
==33422==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33422== 
==33422== All heap blocks were freed -- no leaks are possible
==33422== 
==33422== For counts of detected and suppressed errors, rerun with: -v
==33422== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
