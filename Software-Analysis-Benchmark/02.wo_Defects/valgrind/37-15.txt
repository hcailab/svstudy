==46001== Memcheck, a memory error detector
==46001== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46001== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46001== Command: ./02_wo_Defects 37015
==46001== 
==46001== 
==46001== HEAP SUMMARY:
==46001==     in use at exit: 0 bytes in 0 blocks
==46001==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46001== 
==46001== All heap blocks were freed -- no leaks are possible
==46001== 
==46001== For counts of detected and suppressed errors, rerun with: -v
==46001== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
