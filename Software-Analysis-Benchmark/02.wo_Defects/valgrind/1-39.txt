==30757== Memcheck, a memory error detector
==30757== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==30757== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==30757== Command: ./02_wo_Defects 1039
==30757== 
==30757== 
==30757== HEAP SUMMARY:
==30757==     in use at exit: 0 bytes in 0 blocks
==30757==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==30757== 
==30757== All heap blocks were freed -- no leaks are possible
==30757== 
==30757== For counts of detected and suppressed errors, rerun with: -v
==30757== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
