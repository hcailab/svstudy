==36668== Memcheck, a memory error detector
==36668== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==36668== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==36668== Command: ./02_wo_Defects 17027
==36668== 
==36668== 
==36668== HEAP SUMMARY:
==36668==     in use at exit: 0 bytes in 0 blocks
==36668==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==36668== 
==36668== All heap blocks were freed -- no leaks are possible
==36668== 
==36668== For counts of detected and suppressed errors, rerun with: -v
==36668== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
