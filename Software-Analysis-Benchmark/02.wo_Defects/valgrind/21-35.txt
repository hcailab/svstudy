==38546== Memcheck, a memory error detector
==38546== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38546== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38546== Command: ./02_wo_Defects 21035
==38546== 
==38546== 
==38546== HEAP SUMMARY:
==38546==     in use at exit: 0 bytes in 0 blocks
==38546==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38546== 
==38546== All heap blocks were freed -- no leaks are possible
==38546== 
==38546== For counts of detected and suppressed errors, rerun with: -v
==38546== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
