==38041== Memcheck, a memory error detector
==38041== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38041== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38041== Command: ./02_wo_Defects 20021
==38041== 
==38041== 
==38041== HEAP SUMMARY:
==38041==     in use at exit: 0 bytes in 0 blocks
==38041==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38041== 
==38041== All heap blocks were freed -- no leaks are possible
==38041== 
==38041== For counts of detected and suppressed errors, rerun with: -v
==38041== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
