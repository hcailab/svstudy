==52045== Memcheck, a memory error detector
==52045== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==52045== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==52045== Command: ./02_wo_Defects 49039
==52045== 
==52045== 
==52045== HEAP SUMMARY:
==52045==     in use at exit: 0 bytes in 0 blocks
==52045==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==52045== 
==52045== All heap blocks were freed -- no leaks are possible
==52045== 
==52045== For counts of detected and suppressed errors, rerun with: -v
==52045== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
