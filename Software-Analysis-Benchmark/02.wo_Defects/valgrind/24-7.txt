==73942== Memcheck, a memory error detector
==73942== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==73942== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==73942== Command: ./02_wo_Defects 24007
==73942== 
==73942== 
==73942== HEAP SUMMARY:
==73942==     in use at exit: 0 bytes in 0 blocks
==73942==   total heap usage: 7 allocs, 7 frees, 1,139 bytes allocated
==73942== 
==73942== All heap blocks were freed -- no leaks are possible
==73942== 
==73942== For counts of detected and suppressed errors, rerun with: -v
==73942== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
