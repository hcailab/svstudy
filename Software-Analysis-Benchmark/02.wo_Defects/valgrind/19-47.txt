==37878== Memcheck, a memory error detector
==37878== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37878== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37878== Command: ./02_wo_Defects 19047
==37878== 
==37878== 
==37878== HEAP SUMMARY:
==37878==     in use at exit: 0 bytes in 0 blocks
==37878==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37878== 
==37878== All heap blocks were freed -- no leaks are possible
==37878== 
==37878== For counts of detected and suppressed errors, rerun with: -v
==37878== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
