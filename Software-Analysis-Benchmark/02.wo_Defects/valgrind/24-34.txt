==39722== Memcheck, a memory error detector
==39722== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39722== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39722== Command: ./02_wo_Defects 24034
==39722== 
==39722== 
==39722== HEAP SUMMARY:
==39722==     in use at exit: 0 bytes in 0 blocks
==39722==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39722== 
==39722== All heap blocks were freed -- no leaks are possible
==39722== 
==39722== For counts of detected and suppressed errors, rerun with: -v
==39722== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
