==46034== Memcheck, a memory error detector
==46034== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46034== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46034== Command: ./02_wo_Defects 37019
==46034== 
==46034== 
==46034== HEAP SUMMARY:
==46034==     in use at exit: 0 bytes in 0 blocks
==46034==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46034== 
==46034== All heap blocks were freed -- no leaks are possible
==46034== 
==46034== For counts of detected and suppressed errors, rerun with: -v
==46034== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
