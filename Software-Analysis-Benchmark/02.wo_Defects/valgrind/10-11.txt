==32998== Memcheck, a memory error detector
==32998== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32998== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32998== Command: ./02_wo_Defects 10011
==32998== 
==32998== 
==32998== HEAP SUMMARY:
==32998==     in use at exit: 0 bytes in 0 blocks
==32998==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32998== 
==32998== All heap blocks were freed -- no leaks are possible
==32998== 
==32998== For counts of detected and suppressed errors, rerun with: -v
==32998== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
