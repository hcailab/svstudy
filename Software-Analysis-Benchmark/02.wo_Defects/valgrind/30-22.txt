==42790== Memcheck, a memory error detector
==42790== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42790== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42790== Command: ./02_wo_Defects 30022
==42790== 
==42790== 
==42790== HEAP SUMMARY:
==42790==     in use at exit: 0 bytes in 0 blocks
==42790==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42790== 
==42790== All heap blocks were freed -- no leaks are possible
==42790== 
==42790== For counts of detected and suppressed errors, rerun with: -v
==42790== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
