==49562== Memcheck, a memory error detector
==49562== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49562== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49562== Command: ./02_wo_Defects 44036
==49562== 
==49562== 
==49562== HEAP SUMMARY:
==49562==     in use at exit: 0 bytes in 0 blocks
==49562==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49562== 
==49562== All heap blocks were freed -- no leaks are possible
==49562== 
==49562== For counts of detected and suppressed errors, rerun with: -v
==49562== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
