==37700== Memcheck, a memory error detector
==37700== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37700== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37700== Command: ./02_wo_Defects 19030
==37700== 
==37700== 
==37700== HEAP SUMMARY:
==37700==     in use at exit: 0 bytes in 0 blocks
==37700==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37700== 
==37700== All heap blocks were freed -- no leaks are possible
==37700== 
==37700== For counts of detected and suppressed errors, rerun with: -v
==37700== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
