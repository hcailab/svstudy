==74176== Memcheck, a memory error detector
==74176== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74176== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74176== Command: ./02_wo_Defects 16011
==74176== 
==74176== 
==74176== HEAP SUMMARY:
==74176==     in use at exit: 0 bytes in 0 blocks
==74176==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==74176== 
==74176== All heap blocks were freed -- no leaks are possible
==74176== 
==74176== For counts of detected and suppressed errors, rerun with: -v
==74176== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
