==33785== Memcheck, a memory error detector
==33785== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==33785== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==33785== Command: ./02_wo_Defects 11045
==33785== 
==33785== 
==33785== HEAP SUMMARY:
==33785==     in use at exit: 0 bytes in 0 blocks
==33785==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==33785== 
==33785== All heap blocks were freed -- no leaks are possible
==33785== 
==33785== For counts of detected and suppressed errors, rerun with: -v
==33785== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
