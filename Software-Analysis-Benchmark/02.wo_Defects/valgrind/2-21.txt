==74757== Memcheck, a memory error detector
==74757== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74757== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74757== Command: ./02_wo_Defects 2021
==74757== 
==74757== 
==74757== HEAP SUMMARY:
==74757==     in use at exit: 0 bytes in 0 blocks
==74757==   total heap usage: 2 allocs, 2 frees, 1,044 bytes allocated
==74757== 
==74757== All heap blocks were freed -- no leaks are possible
==74757== 
==74757== For counts of detected and suppressed errors, rerun with: -v
==74757== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
