==40827== Memcheck, a memory error detector
==40827== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==40827== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==40827== Command: ./02_wo_Defects 26043
==40827== 
==40827== 
==40827== HEAP SUMMARY:
==40827==     in use at exit: 0 bytes in 0 blocks
==40827==   total heap usage: 3 allocs, 3 frees, 1,568 bytes allocated
==40827== 
==40827== All heap blocks were freed -- no leaks are possible
==40827== 
==40827== For counts of detected and suppressed errors, rerun with: -v
==40827== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
