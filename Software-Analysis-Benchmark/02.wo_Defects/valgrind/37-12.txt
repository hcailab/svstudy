==45931== Memcheck, a memory error detector
==45931== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==45931== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==45931== Command: ./02_wo_Defects 37012
==45931== 
==45931== 
==45931== HEAP SUMMARY:
==45931==     in use at exit: 0 bytes in 0 blocks
==45931==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==45931== 
==45931== All heap blocks were freed -- no leaks are possible
==45931== 
==45931== For counts of detected and suppressed errors, rerun with: -v
==45931== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
