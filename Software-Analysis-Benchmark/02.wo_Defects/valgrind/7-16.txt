==74462== Memcheck, a memory error detector
==74462== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74462== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74462== Command: ./02_wo_Defects 7016
==74462== 
==74462== 
==74462== HEAP SUMMARY:
==74462==     in use at exit: 0 bytes in 0 blocks
==74462==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==74462== 
==74462== All heap blocks were freed -- no leaks are possible
==74462== 
==74462== For counts of detected and suppressed errors, rerun with: -v
==74462== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
