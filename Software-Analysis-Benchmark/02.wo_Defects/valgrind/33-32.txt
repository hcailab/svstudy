==44235== Memcheck, a memory error detector
==44235== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44235== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44235== Command: ./02_wo_Defects 33032
==44235== 
==44235== 
==44235== HEAP SUMMARY:
==44235==     in use at exit: 0 bytes in 0 blocks
==44235==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44235== 
==44235== All heap blocks were freed -- no leaks are possible
==44235== 
==44235== For counts of detected and suppressed errors, rerun with: -v
==44235== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
