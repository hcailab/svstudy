==32114== Memcheck, a memory error detector
==32114== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32114== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32114== Command: ./02_wo_Defects 6032
==32114== 
==32114== 
==32114== HEAP SUMMARY:
==32114==     in use at exit: 0 bytes in 0 blocks
==32114==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32114== 
==32114== All heap blocks were freed -- no leaks are possible
==32114== 
==32114== For counts of detected and suppressed errors, rerun with: -v
==32114== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
