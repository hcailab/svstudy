==50715== Memcheck, a memory error detector
==50715== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50715== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50715== Command: ./02_wo_Defects 47005
==50715== 
==50715== 
==50715== HEAP SUMMARY:
==50715==     in use at exit: 0 bytes in 0 blocks
==50715==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50715== 
==50715== All heap blocks were freed -- no leaks are possible
==50715== 
==50715== For counts of detected and suppressed errors, rerun with: -v
==50715== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
