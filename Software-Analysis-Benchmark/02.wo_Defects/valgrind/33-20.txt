==44165== Memcheck, a memory error detector
==44165== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==44165== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==44165== Command: ./02_wo_Defects 33020
==44165== 
==44165== 
==44165== HEAP SUMMARY:
==44165==     in use at exit: 0 bytes in 0 blocks
==44165==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==44165== 
==44165== All heap blocks were freed -- no leaks are possible
==44165== 
==44165== For counts of detected and suppressed errors, rerun with: -v
==44165== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
