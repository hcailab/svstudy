==32380== Memcheck, a memory error detector
==32380== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==32380== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==32380== Command: ./02_wo_Defects 7032
==32380== 
==32380== 
==32380== HEAP SUMMARY:
==32380==     in use at exit: 0 bytes in 0 blocks
==32380==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==32380== 
==32380== All heap blocks were freed -- no leaks are possible
==32380== 
==32380== For counts of detected and suppressed errors, rerun with: -v
==32380== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
