==43430== Memcheck, a memory error detector
==43430== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==43430== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==43430== Command: ./02_wo_Defects 31026
==43430== 
==43430== 
==43430== HEAP SUMMARY:
==43430==     in use at exit: 0 bytes in 0 blocks
==43430==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==43430== 
==43430== All heap blocks were freed -- no leaks are possible
==43430== 
==43430== For counts of detected and suppressed errors, rerun with: -v
==43430== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
