==39100== Memcheck, a memory error detector
==39100== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39100== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39100== Command: ./02_wo_Defects 22050
==39100== 
==39100== 
==39100== HEAP SUMMARY:
==39100==     in use at exit: 0 bytes in 0 blocks
==39100==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39100== 
==39100== All heap blocks were freed -- no leaks are possible
==39100== 
==39100== For counts of detected and suppressed errors, rerun with: -v
==39100== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
