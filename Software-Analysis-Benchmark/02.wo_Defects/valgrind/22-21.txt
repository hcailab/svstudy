==38854== Memcheck, a memory error detector
==38854== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==38854== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==38854== Command: ./02_wo_Defects 22021
==38854== 
==38854== 
==38854== HEAP SUMMARY:
==38854==     in use at exit: 0 bytes in 0 blocks
==38854==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==38854== 
==38854== All heap blocks were freed -- no leaks are possible
==38854== 
==38854== For counts of detected and suppressed errors, rerun with: -v
==38854== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
