==49134== Memcheck, a memory error detector
==49134== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==49134== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==49134== Command: ./02_wo_Defects 43049
==49134== 
==49134== 
==49134== HEAP SUMMARY:
==49134==     in use at exit: 0 bytes in 0 blocks
==49134==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==49134== 
==49134== All heap blocks were freed -- no leaks are possible
==49134== 
==49134== For counts of detected and suppressed errors, rerun with: -v
==49134== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
