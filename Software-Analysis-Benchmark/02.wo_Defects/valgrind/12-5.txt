==74064== Memcheck, a memory error detector
==74064== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==74064== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==74064== Command: ./02_wo_Defects 12005
==74064== 
==74064== 
==74064== HEAP SUMMARY:
==74064==     in use at exit: 0 bytes in 0 blocks
==74064==   total heap usage: 2 allocs, 2 frees, 1,025 bytes allocated
==74064== 
==74064== All heap blocks were freed -- no leaks are possible
==74064== 
==74064== For counts of detected and suppressed errors, rerun with: -v
==74064== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
