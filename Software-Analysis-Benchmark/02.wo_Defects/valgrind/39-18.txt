==46773== Memcheck, a memory error detector
==46773== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46773== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46773== Command: ./02_wo_Defects 39018
==46773== 
==46773== 
==46773== HEAP SUMMARY:
==46773==     in use at exit: 0 bytes in 0 blocks
==46773==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46773== 
==46773== All heap blocks were freed -- no leaks are possible
==46773== 
==46773== For counts of detected and suppressed errors, rerun with: -v
==46773== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
