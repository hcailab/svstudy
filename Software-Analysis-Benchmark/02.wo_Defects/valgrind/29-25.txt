==42313== Memcheck, a memory error detector
==42313== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42313== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42313== Command: ./02_wo_Defects 29025
==42313== 
==42313== 
==42313== HEAP SUMMARY:
==42313==     in use at exit: 0 bytes in 0 blocks
==42313==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42313== 
==42313== All heap blocks were freed -- no leaks are possible
==42313== 
==42313== For counts of detected and suppressed errors, rerun with: -v
==42313== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
