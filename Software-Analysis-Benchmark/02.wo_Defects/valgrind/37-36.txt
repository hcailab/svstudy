==46180== Memcheck, a memory error detector
==46180== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==46180== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==46180== Command: ./02_wo_Defects 37036
==46180== 
==46180== 
==46180== HEAP SUMMARY:
==46180==     in use at exit: 0 bytes in 0 blocks
==46180==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==46180== 
==46180== All heap blocks were freed -- no leaks are possible
==46180== 
==46180== For counts of detected and suppressed errors, rerun with: -v
==46180== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
