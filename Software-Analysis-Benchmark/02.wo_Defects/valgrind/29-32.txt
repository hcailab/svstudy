==42382== Memcheck, a memory error detector
==42382== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==42382== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==42382== Command: ./02_wo_Defects 29032
==42382== 
==42382== 
==42382== HEAP SUMMARY:
==42382==     in use at exit: 0 bytes in 0 blocks
==42382==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==42382== 
==42382== All heap blocks were freed -- no leaks are possible
==42382== 
==42382== For counts of detected and suppressed errors, rerun with: -v
==42382== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
