==37531== Memcheck, a memory error detector
==37531== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37531== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37531== Command: ./02_wo_Defects 19014
==37531== 
==37531== 
==37531== HEAP SUMMARY:
==37531==     in use at exit: 0 bytes in 0 blocks
==37531==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37531== 
==37531== All heap blocks were freed -- no leaks are possible
==37531== 
==37531== For counts of detected and suppressed errors, rerun with: -v
==37531== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
