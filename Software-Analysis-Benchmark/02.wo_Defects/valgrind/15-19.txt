==35543== Memcheck, a memory error detector
==35543== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==35543== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==35543== Command: ./02_wo_Defects 15019
==35543== 
==35543== 
==35543== HEAP SUMMARY:
==35543==     in use at exit: 0 bytes in 0 blocks
==35543==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==35543== 
==35543== All heap blocks were freed -- no leaks are possible
==35543== 
==35543== For counts of detected and suppressed errors, rerun with: -v
==35543== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
