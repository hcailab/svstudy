==37791== Memcheck, a memory error detector
==37791== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==37791== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==37791== Command: ./02_wo_Defects 19037
==37791== 
==37791== 
==37791== HEAP SUMMARY:
==37791==     in use at exit: 0 bytes in 0 blocks
==37791==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==37791== 
==37791== All heap blocks were freed -- no leaks are possible
==37791== 
==37791== For counts of detected and suppressed errors, rerun with: -v
==37791== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
