==39351== Memcheck, a memory error detector
==39351== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==39351== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==39351== Command: ./02_wo_Defects 23038
==39351== 
==39351== 
==39351== HEAP SUMMARY:
==39351==     in use at exit: 0 bytes in 0 blocks
==39351==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==39351== 
==39351== All heap blocks were freed -- no leaks are possible
==39351== 
==39351== For counts of detected and suppressed errors, rerun with: -v
==39351== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
