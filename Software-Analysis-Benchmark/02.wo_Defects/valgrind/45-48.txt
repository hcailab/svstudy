==50159== Memcheck, a memory error detector
==50159== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==50159== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==50159== Command: ./02_wo_Defects 45048
==50159== 
==50159== 
==50159== HEAP SUMMARY:
==50159==     in use at exit: 0 bytes in 0 blocks
==50159==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==50159== 
==50159== All heap blocks were freed -- no leaks are possible
==50159== 
==50159== For counts of detected and suppressed errors, rerun with: -v
==50159== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
