==34903== Memcheck, a memory error detector
==34903== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==34903== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==34903== Command: ./02_wo_Defects 14010
==34903== 
==34903== 
==34903== HEAP SUMMARY:
==34903==     in use at exit: 0 bytes in 0 blocks
==34903==   total heap usage: 1 allocs, 1 frees, 1,024 bytes allocated
==34903== 
==34903== All heap blocks were freed -- no leaks are possible
==34903== 
==34903== For counts of detected and suppressed errors, rerun with: -v
==34903== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
