#!/bin/sh

export LARCH_PATH=/home/ynong/Download/splint-3.1.1/lib

cd 01.w_Defects

mkdir splint

for file in ./*.c
do
    /home/ynong/Download/splint-3.1.1/bin/splint $file > ./splint/$file.txt
done
