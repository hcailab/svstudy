#!/bin/sh
cd 02.wo_Defects

mkdir cbmc
<<COMMENT
#/home/ynong/Download/cbmcx/cbmc bit_shift.c --function bit_shift_001 --bounds-check --pointer-check --unwind 1000 --depth 1000
for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc bit_shift.c --function bit_shift_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/1-$j.txt
done


for j in `seq 10 17`
do
    /home/ynong/Download/cbmcx/cbmc bit_shift.c --function bit_shift_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/1-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc buffer_overrun_dynamic.c --function dynamic_buffer_overrun_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/2-$j.txt
done


for j in `seq 10 32`
do
    /home/ynong/Download/cbmcx/cbmc buffer_overrun_dynamic.c --function dynamic_buffer_overrun_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/2-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc buffer_underrun_dynamic.c --function dynamic_buffer_underrun_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/3-$j.txt
done


for j in `seq 10 39`
do
    /home/ynong/Download/cbmcx/cbmc buffer_underrun_dynamic.c --function dynamic_buffer_underrun_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/3-$j.txt
done


for j in `seq 1 2`
do
    /home/ynong/Download/cbmcx/cbmc cmp_funcadr.c --function cmp_funcadr.c_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/4-$j.txt
done

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc conflicting_cond.c --function conflicting_cond_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/5-$j.txt
done


for j in `seq 10 10`
do
    /home/ynong/Download/cbmcx/cbmc conflicting_cond.c --function conflicting_cond_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/5-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc data_lost.c --function data_lost_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/6-$j.txt
done


for j in `seq 10 19`
do
    /home/ynong/Download/cbmcx/cbmc data_lost.c --function data_lost_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/6-$j.txt
done

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc data_overflow.c --function data_overflow_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/7-$j.txt
done


for j in `seq 10 25`
do
    /home/ynong/Download/cbmcx/cbmc data_overflow.c --function data_overflow_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/7-$j.txt
done

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc data_underflow.c --function data_underflow_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/8-$j.txt
done


for j in `seq 10 12`
do
    /home/ynong/Download/cbmcx/cbmc data_underflow.c --function data_underflow_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/8-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc dead_code.c --function dead_code_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/9-$j.txt
done


for j in `seq 10 13`
do
    /home/ynong/Download/cbmcx/cbmc dead_code.c --function dead_code_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/9-$j.txt
done


for j in `seq 1 5`
do
    /home/ynong/Download/cbmcx/cbmc dead_lock.c --function dead_lock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/10-$j.txt
done


for j in `seq 1 3`
do
    /home/ynong/Download/cbmcx/cbmc deletion_of_data_structure_sentinel.c --function deletion_of_data_structure_sentinel_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/11-$j.txt
done

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc double_free.c --function double_free_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/12-$j.txt
done


for j in `seq 10 12`
do
    /home/ynong/Download/cbmcx/cbmc double_free.c --function double_free_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/12-$j.txt
done


for j in `seq 1 4`
do
    /home/ynong/Download/cbmcx/cbmc double_lock.c --function double_lock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/13-$j.txt
done


for j in `seq 1 6`
do
    /home/ynong/Download/cbmcx/cbmc double_release.c --function double_release_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/14-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc endless_loop.c --function endless_loop_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/15-$j.txt
done

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc free_nondynamic_allocated_memory.c --function free_nondynamic_allocated_memory_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/16-$j.txt
done


for j in `seq 10 16`
do
    /home/ynong/Download/cbmcx/cbmc free_nondynamic_allocated_memory.c --function free_nondynamic_allocated_memory_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/16-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc free_null_pointer.c --function free_null_pointer_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/17-$j.txt
done


for j in `seq 10 14`
do
    /home/ynong/Download/cbmcx/cbmc free_null_pointer.c --function free_null_pointer_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/17-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc func_pointer.c --function func_pointer_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/18-$j.txt
done


for j in `seq 10 15`
do
    /home/ynong/Download/cbmcx/cbmc func_pointer.c --function func_pointer_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/18-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc function_return_value_unchecked.c --function function_return_value_unchecked_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/19-$j.txt
done


for j in `seq 10 16`
do
    /home/ynong/Download/cbmcx/cbmc function_return_value_unchecked.c --function function_return_value_unchecked_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/19-$j.txt
done



for j in `seq 1 4`
do
    /home/ynong/Download/cbmcx/cbmc improper_termination_of_block.c --function improper_termination_of_block_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/21-$j.txt
done


for j in `seq 1 1`
do
    /home/ynong/Download/cbmcx/cbmc insign_code.c --function insign_code_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/22-$j.txt
done

for j in `seq 1 6`
do
    /home/ynong/Download/cbmcx/cbmc invalid_extern.c --function invalid_extern_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/23-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc invalid_memory_access.c --function invalid_memory_access_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/24-$j.txt
done


for j in `seq 10 17`
do
    /home/ynong/Download/cbmcx/cbmc invalid_memory_access.c --function invalid_memory_access_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/24-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc littlemem_st.c --function littlemem_st_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/25-$j.txt
done


for j in `seq 10 11`
do
    /home/ynong/Download/cbmcx/cbmc littlemem_st.c --function littlemem_st_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/25-$j.txt
done


for j in `seq 1 1`
do
    /home/ynong/Download/cbmcx/cbmc livelock.c --function livelock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/26-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc lock_never_unlock.c --function lock_never_unlock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/27-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc memory_allocation_failure.c --function memory_allocation_failure_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/28-$j.txt
done

for j in `seq 13 16`
do
    /home/ynong/Download/cbmcx/cbmc memory_allocation_failure.c --function memory_allocation_failure_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/28-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc memory_leak.c --function memory_leak_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/29-$j.txt
done


for j in `seq 10 18`
do
    /home/ynong/Download/cbmcx/cbmc memory_leak.c --function memory_leak_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/29-$j.txt
done



for j in `seq 1 4`
do
    /home/ynong/Download/cbmcx/cbmc not_return.c --function not_return_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/30-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc null_pointer.c --function null_pointer_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/31-$j.txt
done


for j in `seq 10 17`
do
    /home/ynong/Download/cbmcx/cbmc null_pointer.c --function null_pointer_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/31-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc overrun_st.c --function overrun_st_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/32-$j.txt
done


for j in `seq 10 54`
do
    /home/ynong/Download/cbmcx/cbmc overrun_st.c --function overrun_st_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/32-$j.txt
done


for j in `seq 1 2`
do
    /home/ynong/Download/cbmcx/cbmc ow_memcpy.c --function ow_memcpy_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/33-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc pow_related_errors.c --function pow_related_errors_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/34-$j.txt
done


for j in `seq 10 29`
do
    /home/ynong/Download/cbmcx/cbmc pow_related_errors.c --function pow_related_errors_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/34-$j.txt
done


for j in `seq 1 2`
do
    /home/ynong/Download/cbmcx/cbmc ptr_subtraction.c --function ptr_subtraction_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/35-$j.txt
done


for j in `seq 1 8`
do
    /home/ynong/Download/cbmcx/cbmc race_condition.c --function race_condition_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/36-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc redundant_cond.c --function redundant_cond_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/37-$j.txt
done


for j in `seq 10 14`
do
    /home/ynong/Download/cbmcx/cbmc redundant_cond.c --function redundant_cond_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/37-$j.txt
done


for j in `seq 1 2`
do
    /home/ynong/Download/cbmcx/cbmc return_local.c --function return_local_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/38-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc sign_conv.c --function sign_conv_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/39-$j.txt
done


for j in `seq 10 19`
do
    /home/ynong/Download/cbmcx/cbmc sign_conv.c --function sign_conv_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/39-$j.txt
done




for j in `seq 1 3`
do
    /home/ynong/Download/cbmcx/cbmc sleep_lock.c --function sleep_lock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/40-$j.txt
done



for j in `seq 1 6`
do
    /home/ynong/Download/cbmcx/cbmc st_cross_thread_access.c --function st_cross_thread_access_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/41-$j.txt
done



for j in `seq 1 7`
do
    /home/ynong/Download/cbmcx/cbmc st_overflow.c --function st_overflow_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/42-$j.txt
done


for j in `seq 1 7`
do
    /home/ynong/Download/cbmcx/cbmc st_underrun.c --function st_underrun_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/43-$j.txt
done
COMMENT

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc underrun_st.c --function underrun_st_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/44-$j.txt
done


for j in `seq 10 13`
do
    /home/ynong/Download/cbmcx/cbmc underrun_st.c --function underrun_st_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/44-$j.txt
done
<<COMMENT

for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc uninit_memory_access.c --function uninit_memory_access_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/45-$j.txt
done


for j in `seq 10 15`
do
    /home/ynong/Download/cbmcx/cbmc uninit_memory_access.c --function uninit_memory_access_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/45-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc uninit_pointer.c --function uninit_pointer_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/46-$j.txt
done


for j in `seq 10 16`
do
    /home/ynong/Download/cbmcx/cbmc uninit_pointer.c --function uninit_pointer_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/46-$j.txt
done


for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc uninit_var.c --function uninit_var_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/47-$j.txt
done


for j in `seq 10 15`
do
    /home/ynong/Download/cbmcx/cbmc uninit_var.c --function uninit_var_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/47-$j.txt
done


for j in `seq 1 8`
do
    /home/ynong/Download/cbmcx/cbmc unlock_without_lock.c --function unlock_without_lock_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/48-$j.txt
done



for j in `seq 1 7`
do
    /home/ynong/Download/cbmcx/cbmc unused_var.c --function unused_var_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/49-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc wrong_arguments_func_pointer.c --function wrong_arguments_func_pointer_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/50-$j.txt
done

for j in `seq 10 18`
do
    /home/ynong/Download/cbmcx/cbmc wrong_arguments_func_pointer.c --function wrong_arguments_func_pointer_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/50-$j.txt
done



for j in `seq 1 9`
do
    /home/ynong/Download/cbmcx/cbmc zero_division.c --function zero_division_00$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/51-$j.txt
done


for j in `seq 10 16`
do
    /home/ynong/Download/cbmcx/cbmc zero_division.c --function zero_division_0$j --bounds-check --pointer-check --unwind 1000 --depth 1000 > ./cbmc/51-$j.txt
done


COMMENT








